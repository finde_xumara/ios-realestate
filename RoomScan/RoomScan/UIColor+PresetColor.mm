//
//  UIColor+presetColor.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 22/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "UIColor+presetColor.h"

@implementation UIColor (PresetColor)

+ (UIColor *) markerSelected {
    return [UIColor greenColor];
}

+ (UIColor *) markerDefault {
    return [UIColor blueColor];
}

+ (UIColor *) pointDefaultStroke {
    return [UIColor redColor];
}

+ (UIColor *) pointDefaultFill {
    return [UIColor blackColor];
}

+ (UIColor *) textDefault {
    return [UIColor redColor];
}

@end
