//
//  Shader.fsh
//  RoomScan
//
//  Created by Finde Xumara on 08/05/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

varying highp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
