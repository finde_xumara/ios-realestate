//
//  RSScanEnvironmentViewController.m
//  RoomScan
//
//  Created by Finde Xumara on 14/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

// View Controllers
#import "RSScanViewController.h"
#import "RSScanViewController+Rotopan.h"
#import "RSScanViewController+StructureSensor.h"
#import "RSScanViewController+Camera.h"

// View Models
#import "RSScanViewModel.h"

// Models
#import "RSEnvironment.h"
#import "RSScan.h"
#import "RSAnnotation.h"

#import "ImageHelper.h"

//namespace {
//    NSString* stringWithMatrix (GLKMatrix4 pose) {
//        NSMutableString *string = [[NSMutableString alloc] init];
//        [string appendFormat: @"%f,%f,%f,%f,", pose.m00, pose.m01, pose.m02, pose.m03];
//        [string appendFormat: @"%f,%f,%f,%f,", pose.m10, pose.m11, pose.m12, pose.m13];
//        [string appendFormat: @"%f,%f,%f,%f,", pose.m20, pose.m21, pose.m22, pose.m23];
//        [string appendFormat: @"%f,%f,%f,%f",  pose.m30, pose.m31, pose.m32, pose.m33];
//        return string;
//    }
//}

@interface RSScanViewController ()

@property (atomic) unsigned int tagCounter;

@end

@implementation RSScanViewController

#pragma mark - ViewController Setup

- (void)dealloc
{
    [self.avCaptureSession stopRunning];
    
    if ([EAGLContext currentContext] == _display.context)
    {
        [EAGLContext setCurrentContext:nil];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // ReactiveCocoa Bindings
    RAC(self, title) = RACObserve(self.viewModel, scanName);
    
    _rotopan = [[Rotopan alloc] init];
    
    [self setupRotopan];
    
    [self setupUserInterface];
    
    [self setupIMU];
    
    [self setupStructureSensor];
    
    [self.sensorBatteryView setSensorController: _sensorController];
    
    [self setUIScanInit];
    
    [self enterSensorInitializationState];
    
    _RGB_view.transform = CGAffineTransformMakeRotation(M_PI);
    _Depth_view.transform = CGAffineTransformMakeRotation(M_PI);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // We will connect to the sensor when we receive appDidBecomeActive.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)enterSensorInitializationState {
    
    // TODO: show initialisation message and failure
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self
                                   selector:@selector(BLEShieldScan:)
                                   userInfo:nil repeats:NO];
    
    // Try to connect to the Structure Sensor and stream if necessary.
    if ([self currentStateNeedsSensor])
        [self connectToStructureSensorAndStartStreaming];
    
    // Abort the current scan if we were still scanning before going into background since we
    // are not likely to recover well.
    [self resetWasPressed:self];
}

- (void)enterScanningState
{
    // Switch to the Done button.
    self.scanButton.hidden = YES;
    self.doneButton.hidden = NO;
    self.resetButton.hidden = NO;
    
    // We will lock exposure during scanning to ensure better coloring.
    [self setColorCameraParametersForScanning];
    
    _appStatus.roomScanState = RSStateScanning;
    
    //    _debugTextView.text = @"scanning..";
    
    // if connected
    [self rotopanStartScan];
}

- (void)enterFinalizingState
{
    // Hide the Scan/Done/Reset button.
    self.scanButton.hidden = YES;
    self.doneButton.hidden = YES;
    self.resetButton.hidden = YES;
              
    // done...
    _debugTextView.text = @"done..";

    // Stop the sensors, we don't need them.
    [_sensorController stopStreaming];
    [self stopColorCamera];
    [self rotopanReset];
    
    // create 3d model
    [_viewModel generatePly];
    
    [NSTimer scheduledTimerWithTimeInterval:(float)5
                                     target:self
                                   selector:@selector(dismissSelf)
                                   userInfo:nil
                                    repeats:NO];
}


/// not needed
- (void)enterViewingState
{
    [self updateIdleTimer];
}

#pragma mark - User Interaction

-(IBAction)cancelWasPressed:(id)sender {
    
    [self.viewModel cancel];
    [self dismissSelf];
    
}

-(IBAction)scanWasPressed:(id)sender {
    
    [self enterScanningState];
    
}

-(IBAction)resetWasPressed:(id)sender {
    // stop all pending
    
    // Handles simultaneous press of Done & Reset.
    if(self.doneButton.hidden) return;
    
    [self enterPoseInitializationState];
    
}

-(IBAction)doneWasPressed:(id)sender {
    
    // Handles simultaneous press of Done & Reset.
    if(self.doneButton.hidden) return;
    
    [self enterFinalizingState];
    
}

#pragma mark - Private Methods

double lastTimestamp = 0;

-(void) processDepthFrame:(STDepthFrame *)depthFrame
               colorFrame:(STColorFrame *)colorFrame {
    
    BOOL okayToStore = false;
    
    _debugTextView.text = @"processing depth and color";
    
    // Upload the new color image for next rendering.
    //    if (colorFrame != nil)
    //        [self uploadGLColorTexture:colorFrame];
    
    // TODO: show current camera => depth and color
    
    // check timing / angle
    if (_appStatus.roomScanState == RSStateScanning) {
        
        if (_rotopan.deviceStatus == RotopanStatusDisconnected) {
            
            if (lastTimestamp == 0 ||CACurrentMediaTime() - lastTimestamp > 1.0/15 ) {
                
                lastTimestamp = CACurrentMediaTime();
                
                okayToStore = true;
                
            }
            
        }
        else if (self.viewModel.isWaitingForDepthSensor && _rotopan.deviceStatus == RotopanStatusScanning) {
            okayToStore = true;
        }
    }
    
    self.progressCircle.value = self.viewModel.progressValue;
    
    _debugTextView.text = self.viewModel.debugMessage;
    
    if (okayToStore) {
        
        // _debugTextView.text = @"storing..";
        UIGraphicsBeginImageContext(self.view.frame.size);
        [[ImageHelper imageFromColorFrame:colorFrame] drawInRect:self.view.bounds];
        UIImage *rgb_image = UIGraphicsGetImageFromCurrentImageContext();
        
        UIImage* flipped_rgb_image = [UIImage imageWithCGImage:rgb_image.CGImage
                                                    scale:rgb_image.scale
                                              orientation:UIImageOrientationDown];
        UIGraphicsEndImageContext();
        
        UIGraphicsBeginImageContext(self.view.frame.size);
        [[ImageHelper imageFromDepthFrame:depthFrame] drawInRect:self.view.bounds];
        UIImage *depth_image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        self.RGB_view.backgroundColor = [UIColor colorWithPatternImage:flipped_rgb_image];
        self.Depth_view.backgroundColor = [UIColor colorWithPatternImage:depth_image];
        
        // if using mapper then
        
        // save the data + rotopan + IMU
        GLKMatrix4 pose = GLKMatrix4Identity;
        
        [self.viewModel processDepthFrame:depthFrame colorFrame:colorFrame rotopanStatus:_rotopan pose:pose];
        
        self.viewModel.numberOfCapturedData++;
        self.viewModel.isWaitingForDepthSensor = NO;
        
        // update UI
        // if continuous stream then -> show number of scans
        // if step-based then -> show the percentage
        [self updateAppStatusMessage];
        
        // continue scan
        [self scanNow];
        
    }
}

-(void)dismissSelf {
    [self.viewModel willDismiss];
    //    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated: YES];
}

-(void)updateIdleTimer {
    if ([self isStructureConnectedAndCharged] && [self currentStateNeedsSensor])
    {
        // Do not let the application sleep if we are currently using the sensor data.
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    }
    else
    {
        // Let the application sleep if we are only viewing the mesh or if no sensors are connected.
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    }
}

- (void)setupIMU
{
    _lastCoreMotionGravity = GLKVector3Make (0,0,0);
    
    // 60 FPS is responsive enough for motion events.
    const float fps = 60.0;
    _motionManager = [[CMMotionManager alloc] init];
    _motionManager.accelerometerUpdateInterval = 1.0/fps;
    _motionManager.gyroUpdateInterval = 1.0/fps;
    
    RSScanViewController * __weak weakSelf = self;
    if (_motionManager.accelerometerAvailable) {
        
        if (_motionManager.deviceMotionAvailable) {
            [_motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue mainQueue]
                                                withHandler:^(CMDeviceMotion *motion, NSError *error) {
                                                    
                                                    [weakSelf processDeviceMotion:motion withError:error];
                                                    
                                                }
             ];
        }
    }
}

- (void)processDeviceMotion:(CMDeviceMotion *)motion withError:(NSError *)error
{
    _rotopan.motion = motion;
}

-(void)setupUserInterface {
    
}

-(BOOL)currentStateNeedsSensor
{
    return TRUE;
}

#pragma mark - the TO DO
-(void) updateAppStatusMessage {
    [self.sensorBatteryView setNeedsDisplay];
    
    // Skip everything if we should not show app status messages (e.g. in viewing state).
    if (_appStatus.statusMessageDisabled)
    {
        //        [self hideAppStatusMessage];
        return;
    }
    
    // First show sensor issues, if any.
    switch (_appStatus.sensorStatus)
    {
        case AppStatus::SensorStatusOk:
        {
            break;
        }
            
        case AppStatus::SensorStatusNeedsUserToConnect:
        {
            //            [self showAppStatusMessage:_appStatus.pleaseConnectSensorMessage];
            return;
        }
            
        case AppStatus::SensorStatusNeedsUserToCharge:
        {
            //            [self showAppStatusMessage:_appStatus.pleaseChargeSensorMessage];
            return;
        }
    }
    
    // Color camera without calibration (e.g. not iPad).
    if (!_appStatus.colorCameraIsCalibrated)
    {
        //        [self showAppStatusMessage:_appStatus.needCalibratedColorCameraMessage];
        return;
    }
    
    // Color camera permission issues.
    if (!_appStatus.colorCameraIsAuthorized)
    {
        //        [self showAppStatusMessage:_appStatus.needColorCameraAccessMessage];
        return;
    }
    
    // Finally background processing feedback.
    //    switch (_appStatus.backgroundProcessingStatus)
    //    {
    //        case AppStatus::BackgroundProcessingStatusIdle:
    //        {
    //            break;
    //        }
    //
    //        case AppStatus::BackgroundProcessingStatusFinalizing:
    //        {
    //            [self showAppStatusMessage:_appStatus.finalizingMeshMessage];
    //            return;
    //        }
    //    }
    
    // If we reach this point, no status to show.
    //    [self hideAppStatusMessage];
    
    
}

- (void)enterPoseInitializationState
{
    // Switch to the Scan button.
    self.scanButton.hidden = NO;
    self.doneButton.hidden = YES;
    self.resetButton.hidden = YES;
    
    // We leave exposure unlock during init.
    [self setColorCameraParametersForInit];
    
    [self updateIdleTimer];
}

- (void)setUIScanInit {
    self.scanButton.hidden = YES;
    self.doneButton.hidden = YES;
    self.resetButton.hidden = YES;
}

- (void)setUIScanReady
{
    self.scanButton.hidden = NO;
    self.doneButton.hidden = YES;
    self.resetButton.hidden = YES;
}


@end
