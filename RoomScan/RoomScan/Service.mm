//
//  Service.m
//  ServerCommunication
//
//  Created by Daniel Hallin on 21/04/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "Service.h"

@implementation Service

- (instancetype) initWithDelegate:(id<ServiceDelegate>)delegate
{
    self = [super init];
    self.delegate = delegate;
    return self;
}

- (void) dealloc
{
    self.delegate = nil;
}

@end

@implementation NetworkService

- (instancetype) init
{
    if (!(self = [super init])) return nil;
    _apiClient = [APIClient sharedClient];
    return self;
}

- (instancetype) initWithDelegate:(id<ServiceDelegate>)delegate
{
    self = [self init];
    self.delegate = delegate;
    return self;
}

@end
