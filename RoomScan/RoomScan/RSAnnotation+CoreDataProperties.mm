//
//  RSAnnotation+CoreDataProperties.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 22/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RSAnnotation+CoreDataProperties.h"

@implementation RSAnnotation (CoreDataProperties)

@dynamic annotationId;
@dynamic annotationName;
@dynamic createdAt;
@dynamic modifiedAt;
@dynamic type;
@dynamic isTopBottomSplitted;
@dynamic environmentObj;
@dynamic points;

@end
