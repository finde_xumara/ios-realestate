//
//  RSAnnotation+CoreDataProperties.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 22/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RSAnnotation.h"

NS_ASSUME_NONNULL_BEGIN

@interface RSAnnotation (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *annotationId;
@property (nullable, nonatomic, retain) NSString *annotationName;
@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSDate *modifiedAt;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSNumber *isTopBottomSplitted;
@property (nullable, nonatomic, retain) RSEnvironment *environmentObj;
@property (nullable, nonatomic, retain) NSOrderedSet *points;

@end

@interface RSAnnotation (CoreDataGeneratedAccessors)

- (void)insertObject:(NSManagedObject *)value inPointsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromPointsAtIndex:(NSUInteger)idx;
- (void)insertPoints:(NSArray<NSManagedObject *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removePointsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInPointsAtIndex:(NSUInteger)idx withObject:(NSManagedObject *)value;
- (void)replacePointsAtIndexes:(NSIndexSet *)indexes withPoints:(NSArray<NSManagedObject *> *)values;
- (void)addPointsObject:(NSManagedObject *)value;
- (void)removePointsObject:(NSManagedObject *)value;
- (void)addPoints:(NSOrderedSet<NSManagedObject *> *)values;
- (void)removePoints:(NSOrderedSet<NSManagedObject *> *)values;

@end

NS_ASSUME_NONNULL_END
