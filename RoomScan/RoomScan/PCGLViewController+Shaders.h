//
//  PCGLViewController+Shader.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 19/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

//#import "PCGLViewController.h"

@interface PCGLViewController (Shader)

- (BOOL)loadShaders;
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;

@end
