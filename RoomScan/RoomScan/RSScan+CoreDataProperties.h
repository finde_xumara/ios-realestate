//
//  RSScan+CoreDataProperties.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 01/09/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RSScan.h"

NS_ASSUME_NONNULL_BEGIN

@interface RSScan (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *createdAt;
@property (nullable, nonatomic, retain) NSDate *modifiedAt;
@property (nullable, nonatomic, retain) NSString *scanId;
@property (nullable, nonatomic, retain) NSString *scanName;
@property (nullable, nonatomic, retain) NSString *serverId;
@property (nullable, nonatomic, retain) NSNumber *m00;
@property (nullable, nonatomic, retain) NSNumber *m01;
@property (nullable, nonatomic, retain) NSNumber *m02;
@property (nullable, nonatomic, retain) NSNumber *m03;
@property (nullable, nonatomic, retain) NSNumber *m10;
@property (nullable, nonatomic, retain) NSNumber *m11;
@property (nullable, nonatomic, retain) NSNumber *m12;
@property (nullable, nonatomic, retain) NSNumber *m13;
@property (nullable, nonatomic, retain) NSNumber *m20;
@property (nullable, nonatomic, retain) NSNumber *m21;
@property (nullable, nonatomic, retain) NSNumber *m22;
@property (nullable, nonatomic, retain) NSNumber *m30;
@property (nullable, nonatomic, retain) NSNumber *m23;
@property (nullable, nonatomic, retain) NSNumber *m31;
@property (nullable, nonatomic, retain) NSNumber *m32;
@property (nullable, nonatomic, retain) NSNumber *m33;
@property (nullable, nonatomic, retain) RSEnvironment *environmentObj;

@end

NS_ASSUME_NONNULL_END
