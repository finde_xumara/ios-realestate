//
//  io_ply.h
//  RoomScan
//
//  Created by Finde Xumara on 10/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface io_ply : NSObject 

- (bool) read_ply:(NSString *)fileAtPath    withTarget:(NSArray *)arrayData;
- (bool) write_ply:(NSString *)fileAtPath   withInput:(NSArray *)arrayData;

@end

@interface ply_element : NSObject

@property (nonatomic) float x;
@property (nonatomic) float y;
@property (nonatomic) float z;
@property (nonatomic) unsigned char r;
@property (nonatomic) unsigned char g;
@property (nonatomic) unsigned char b;

-(void) setX:(float)X
           Y:(float)Y
           Z:(float)Z
           R:(unsigned char)R
           G:(unsigned char)G
           B:(unsigned char)B;
@end