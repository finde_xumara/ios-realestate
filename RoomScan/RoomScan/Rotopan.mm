//
//  NSObject+Rotopan.m
//  RoomScan
//
//  Created by Finde Xumara on 08/07/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "Rotopan.h"

@implementation Rotopan

- (id)init
{
    self = [super init];
    if (self) {
        [self reset];
    }
    return self;
}

- (void) reset {
    _stepRotopan = 0;
    _tiltRotopan = 0;
    _counter = 0;
    
    _deviceStatus = RotopanStatusDisconnected;
    _deviceDirection = RotopanDirectionUp;
    _deviceTask = RotopanTaskRotate;
    
    _isAngleDone = NO;
    _isTaskConfirmed = YES;
    
    _tiltValue = 0;
    _panValue = 0;
    _panAbsoluteValue = 0;
    _numberOfCapturedData = 0;
    _waiting_unit = 0;
}

@end
