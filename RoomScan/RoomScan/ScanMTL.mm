//
//  ScanMTL.m
//  RoomScan
//
//  Created by Finde Xumara on 22/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "ModelUtils.h"
#import "ScanMTL.h"

@implementation ScanMTL


+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"localId" : @"local_id",
             @"envId" : @"env_id",
             @"name" : @"name",
             @"serverId" : @"id",
             @"createdAt" : @"created_at",
             @"modifiedAt" : @"modified_at"
             };
}

+ (NSValueTransformer *)modifiedAtJSONTransformer {
    return [ModelUtils dateJSONTransformer];
}

+ (NSValueTransformer *)createdAtJSONTransformer {
    return [ModelUtils dateJSONTransformer];
}

@end