//
//  RotopanStatusView.m
//  RoomScan
//
//  Created by Finde Xumara on 11/07/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RotopanStatusView.h"

@implementation RotopanStatusView

@synthesize state = _state;

- (instancetype)init {
    if (self = [super init]) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

#pragma mark - Setup

- (void)setup {
    self.contentView = [UIView new];
    [self addSubview:self.contentView];
    
    [self setState:Disconnected];
}

#pragma mark - Setters

- (void)setState:(enum RotopanConnectionStatus)state {
    _state = state;
    
    switch(_state) {
        case Connected:
            [_rotoButton setImage:_connectedImage forState:UIControlStateNormal];
            break;
            
        case Connecting:
            [_rotoButton setImage:_connectingImage forState:UIControlStateNormal];
            break;
            
        case Disconnected:
        default:
            [_rotoButton setImage:_disconnectedImage forState:UIControlStateNormal];
            break;
    }
}


@end
