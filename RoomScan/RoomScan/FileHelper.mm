//
//  NSObject_FileUtils.h
//  RoomScan
//
//  Created by Finde Xumara on 14/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "FileHelper.h"


@interface FileHelper ()

@end

@implementation FileHelper {
}

// Returns the URL to the application's Documents directory.
+(NSURL *) applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

+(BOOL) createDirectoryIfNotExist:(NSString *)newDirectoryName withInDirectory:(NSURL *) rootDir {
    // create folder with name environment
    
    NSURL *storeURL = [rootDir URLByAppendingPathComponent:newDirectoryName];
    
    // If directory doesn't exist create it
    BOOL isDirectory;
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath: [storeURL path] isDirectory: &isDirectory];
    if (!fileExists | !isDirectory)
    {
        NSError *error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath: [storeURL path] withIntermediateDirectories: YES attributes:nil error: &error];
        if (error)
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    return FALSE;
}
@end

