//
//  BatteryView.h
//  Scanner
//
//  Created by Daniel Hallin on 17/11/15.
//  Copyright © 2015 Occipital. All rights reserved.
//



#import <UIKit/UIKit.h>
#import <Structure/Structure.h>

#define kHighPowerColor ([UIColor colorWithRed: 0.f green: 0.7 blue: 0.f alpha: 1.f])
#define kLowPowerColor ([UIColor colorWithRed: 1.f green: 0.f blue: 0.f alpha: 1.f])

static CGFloat const lowPowerLimit = 0.05f;

@interface BatteryLevelContainer : UIView

@property (nonatomic) CGFloat batteryLevel;

@end


@interface SensorBatteryView : UIView

@property (nonatomic, weak) IBOutlet BatteryLevelContainer *batteryLevelContainer;
@property (nonatomic, weak) IBOutlet UILabel *batteryLevelLabel;

@property (nonatomic, assign) STSensorController *sensorController;


@end


