//
//  APIClientManager.m
//  RoomScan
//
//  Created by Finde Xumara on 22/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "APIClient.h"
#import "APIClientManager.h"

@implementation APIClientManager


- (instancetype) init
{
    if (!(self = [super init])) return nil;
    
    _environmentService = [[EnvironmentService alloc] init];
    
    return self;
}

@end
