//
//  ZipService.h
//  ServerCommunication
//
//  Created by Daniel Hallin on 19/05/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "Service.h"

@interface ZipService : Service

- (void) zipDirectory: (NSString*) directory
             progress: (ServiceProgressBlock) progress
           completion: (ServiceCompletionBlock) completion;

+ (NSData*) zippedDataFromDirectory: (NSString*) directory;

@end
