//
//  EnvironmentMTL.h
//  VirtualRoom
//
//  Created by Finde Xumara on 21/06/16.
//
//

#import <Mantle/Mantle.h>

@interface EnvironmentMTL : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong, readonly) NSString *serverId;
@property (nonatomic, strong, readonly) NSString *localId;
@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSDate *createdAt;
@property (nonatomic, strong, readonly) NSDate *modifiedAt;

@end