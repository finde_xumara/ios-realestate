//
// Created by Finde Xumara on 30/12/15.
//

#ifndef PROJECT_FASTPCL_H
#define PROJECT_FASTPCL_H

#pragma once
/*
 * fastPCL.h
 *
 *  Created on: 6 Nov 2015
 *      Author: morris
 */

#include <boost/smart_ptr/shared_ptr.hpp>
#include <Eigen/src/Core/util/Memory.h>
#include <vector>

namespace pcl {
    struct PointXY;
    struct PointXYZ;
    struct PointRGB;
    struct PointXYZRGB;
    struct PointNormal;
    
    template <typename PointT> class PointCloud;
    template <typename PointT> using PointCloudPtr = boost::shared_ptr<PointCloud<PointT> >;
    template<typename PointInT> class TextureMapping;
    
    class TextureMesh;
    class PolygonMesh;
    
    namespace texture_mapping {
        struct Camera;
        typedef std::vector<Camera, Eigen::aligned_allocator<Camera> > CameraVector;
    }
}

#endif //PROJECT_FASTPCL_H
