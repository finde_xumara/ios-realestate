//
//  RotopanStatusView.h
//  RoomScan
//
//  Created by Finde Xumara on 11/07/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <UIKit/UIKit.h>

enum RotopanConnectionStatus {
    Connected,
    Connecting,
    Disconnected
};

IB_DESIGNABLE

@interface RotopanStatusView : UIView

@property (nonatomic, readwrite)    IBInspectable   enum RotopanConnectionStatus state;
@property (nonatomic, strong)       IBInspectable   UIImage *connectedImage;
@property (nonatomic, strong)       IBInspectable   UIImage *connectingImage;
@property (nonatomic, strong)       IBInspectable   UIImage *disconnectedImage;
@property (nonatomic, strong)                       UIView *contentView;
@property (nonatomic, strong)       IBOutlet        UIButton *rotoButton;

- (void)setState:(enum RotopanConnectionStatus)state;

@end