//
//  RSEditEnvironmentViewModel.h
//  RoomScan
//
//  Created by Finde Xumara on 07/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RVMViewModel.h"
#import "RVMViewModel+Edits.h"

@class RSEnvironment, RSScanViewModel, RSAnnotationViewModel, RSPointViewModel;

@interface RSEnvironmentViewModel : RVMViewModel

@property (nonatomic, readonly) RSEnvironment *model;
@property (nonatomic, assign, getter = isInserting) BOOL inserting;

@property (nonatomic, strong) NSString *environmentName;
@property (nonatomic, strong) NSString *environmentId;
@property (nonatomic, strong) NSString *serverId;

@property (nonatomic, strong) NSNumber *progress;

- (instancetype)initWithModel:(id)model;

- (void)cancel;
- (void)willDismiss;
- (NSURL *)environmentPath;
- (void) updateServerId:(NSString *) serverId;
- (RACSignal *) uploadSignal;

// scans
- (NSInteger)numberOfScans;
- (void)addScan: (NSString *) scanName;
- (void)removeScanAtIndex:(NSInteger)index;
- (RSScanViewModel *) scanAtIndex: (NSInteger) index;
- (NSString *)scanTitleAtIndexPath:(NSInteger)index;

// annotations
- (NSInteger)numberOfAnnotations;
- (void)addAnnotationWithOptions:(NSDictionary *) options;
- (void)removeAnnotationAtIndex:(NSInteger)index;
- (RSAnnotationViewModel *) annotationAtIndex: (NSInteger) index;
- (RSAnnotationViewModel *) lastCreatedAnnotation;
- (NSString *) annotationTitleAtIndexPath: (NSInteger) index;
- (NSMutableArray *) getAnnotationsMeta;
- (NSMutableArray *) getAlignmentsMeta;

@end
