//
//  ModelUtils.m
//  ServerCommunication
//
//  Created by Daniel Hallin on 07/04/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "ModelUtils.h"

@implementation ModelUtils

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
//    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    //    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    dateFormatter.dateFormat = @"yyyy-MM-dd' 'HH:mm:ss.SSSZ";
    return dateFormatter;
}

+ (NSDateFormatter *)shortDateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.dateFormat = @"yyMMdd-HHmmss";
    return dateFormatter;
}

+ (NSValueTransformer *)dateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        return [self.dateFormatter dateFromString:dateString];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
        return [self.dateFormatter stringFromDate: date];
    }];
}

@end


