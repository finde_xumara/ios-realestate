//
//  RSPoint+CoreDataProperties.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 22/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RSPoint+CoreDataProperties.h"

@implementation RSPoint (CoreDataProperties)

@dynamic isEdge;
@dynamic x;
@dynamic y;
@dynamic annotationObj;

@end
