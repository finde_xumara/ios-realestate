//
//  RVMViewModel+Edits.m
//  RoomScan
//
//  Created by Finde Xumara on 22/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RVMViewModel+Edits.h"

@implementation RVMViewModel (Edits)

APIClientManager * _apiManager;

-(instancetype)initWithAPIManager {
    
    self = [super init];
    if (self == nil) return nil;
    
    _apiManager = [[APIClientManager alloc] init];
    
    return self;
}

+(APIClientManager *) apiManager {
    return _apiManager;
}

@end
