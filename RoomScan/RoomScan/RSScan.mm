//
//  RSScan.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 01/09/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "FileHelper.h"
#import "RSScan.h"
#import "RSEnvironment.h"

@implementation RSScan

// Insert code here to add functionality to your managed object subclass
-(void) prepareSetting {
    [[NSFileManager defaultManager] createDirectoryAtPath: [[[self getPath] URLByAppendingPathComponent:kDepthDir] path] withIntermediateDirectories: YES attributes:nil error: nil];
    [[NSFileManager defaultManager] createDirectoryAtPath: [[[self getPath] URLByAppendingPathComponent:kKeyframeDir] path] withIntermediateDirectories: YES attributes:nil error: nil];
}

-(void) prepareForDeletion {
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtURL:[self getPath] error:&error];
    
    [super prepareForDeletion];
}

-(NSURL *) getPath {
    RSEnvironment *env = (RSEnvironment *) self.environmentObj;
    
    return [[[env getPath] URLByAppendingPathComponent:@"scans"] URLByAppendingPathComponent:self.scanName];
}

-(NSString *) getDir {
    RSEnvironment *env = (RSEnvironment *) self.environmentObj;
    
    return [[env getPath] path];
}

-(GLKMatrix4) getTransformationMatrix {
    if ([self.m33 floatValue] == 0) {
        [self setTransformationMatrix:GLKMatrix4Identity];
        return GLKMatrix4Identity;
    }
    
    return GLKMatrix4Make([self.m00 floatValue], [self.m01 floatValue], [self.m02 floatValue], [self.m03 floatValue],
                          [self.m10 floatValue], [self.m11 floatValue], [self.m12 floatValue], [self.m13 floatValue],
                          [self.m20 floatValue], [self.m21 floatValue], [self.m22 floatValue], [self.m23 floatValue],
                          [self.m30 floatValue], [self.m31 floatValue], [self.m32 floatValue], [self.m33 floatValue]);
}

- (NSMutableArray *) getTransformationArray {
    NSMutableArray * arr = [NSMutableArray new];
    
    [arr addObject:self.m00];
    [arr addObject:self.m01];
    [arr addObject:self.m02];
    [arr addObject:self.m03];
    
    [arr addObject:self.m10];
    [arr addObject:self.m11];
    [arr addObject:self.m12];
    [arr addObject:self.m13];
    
    [arr addObject:self.m20];
    [arr addObject:self.m21];
    [arr addObject:self.m22];
    [arr addObject:self.m23];
    
    [arr addObject:self.m30];
    [arr addObject:self.m31];
    [arr addObject:self.m32];
    [arr addObject:self.m33];
    
    return arr;
}

- (void) setTransformationMatrix: (GLKMatrix4) transformationMat {
    
    // m00 -> m33, where m30, m31, and m32 correspond to the translation values tx, ty and tz;
    self.m00 = [NSNumber numberWithFloat:transformationMat.m00];
    self.m01 = [NSNumber numberWithFloat:transformationMat.m01];
    self.m02 = [NSNumber numberWithFloat:transformationMat.m02];
    self.m03 = [NSNumber numberWithFloat:transformationMat.m03];
    
    self.m10 = [NSNumber numberWithFloat:transformationMat.m10];
    self.m11 = [NSNumber numberWithFloat:transformationMat.m11];
    self.m12 = [NSNumber numberWithFloat:transformationMat.m12];
    self.m13 = [NSNumber numberWithFloat:transformationMat.m13];
    
    self.m20 = [NSNumber numberWithFloat:transformationMat.m20];
    self.m21 = [NSNumber numberWithFloat:transformationMat.m21];
    self.m22 = [NSNumber numberWithFloat:transformationMat.m22];
    self.m23 = [NSNumber numberWithFloat:transformationMat.m23];
    
    self.m30 = [NSNumber numberWithFloat:transformationMat.m30];
    self.m31 = [NSNumber numberWithFloat:transformationMat.m31];
    self.m32 = [NSNumber numberWithFloat:transformationMat.m32];
    self.m33 = [NSNumber numberWithFloat:transformationMat.m33];
}
@end
