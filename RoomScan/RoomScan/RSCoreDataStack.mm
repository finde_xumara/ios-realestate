//
//  RSCoreDataStack.m
//  RoomScan
//
//  Created by Finde Xumara on 06/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RSCoreDataStack.h"

// Models
#import "RSEnvironment.h"
#import "RSScan.h"

@interface RSCoreDataStack ()

@property (readwrite, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readwrite, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readwrite, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation RSCoreDataStack

+(instancetype)defaultStack {
    static RSCoreDataStack *stack;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        stack = [[self alloc] init];
    });
    return stack;
}

+(instancetype)inMemoryStack {
    static RSCoreDataStack *stack;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        stack = [[self alloc] init];
        
        NSPersistentStoreCoordinator *persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[stack managedObjectModel]];
        NSError *error;
        if (![persistentStoreCoordinator addPersistentStoreWithType:NSInMemoryStoreType configuration:nil URL:nil options:nil error:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
        
        stack.persistentStoreCoordinator = persistentStoreCoordinator;
    });
    
    return stack;
}

-(void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    
    // since iOS 9
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"RoomScan" withExtension:@"momd"];
    self.managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return self.managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"RoomScan.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (BOOL) createDirectoryIfNotExist:(NSString *)newDirectoryName {
    // create folder with name environment
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:newDirectoryName];
    
    // If directory doesn't exist create it
    BOOL isDirectory;
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath: [storeURL path] isDirectory: &isDirectory];
    if (!fileExists | !isDirectory)
    {
        NSError *error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath: [storeURL path] withIntermediateDirectories: YES attributes:nil error: &error];
        if (error)
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    return FALSE;
}

#pragma mark - Public Methods

-(void)ensureInitialLoad {
    NSString *initialLoadKey = @"Initial Load v2";
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    BOOL hasInitialLoad = [userDefaults boolForKey:initialLoadKey];
    if (hasInitialLoad == NO) {
        
        [self createDirectoryIfNotExist: rootEnvironment];
        
        {
            RSEnvironment *rsEnv = [NSEntityDescription insertNewObjectForEntityForName:@"RSEnvironment" inManagedObjectContext:[RSCoreDataStack defaultStack].managedObjectContext];
            rsEnv.environmentName = @"3DU Office";
            rsEnv.environmentId = @"sampleID";
            rsEnv.createdAt = [NSDate date];
            rsEnv.modifiedAt = [NSDate date];
            
            RSScan *office_0001 = [NSEntityDescription insertNewObjectForEntityForName:@"RSScan" inManagedObjectContext:[RSCoreDataStack defaultStack].managedObjectContext];
            office_0001.environmentObj = rsEnv;
            office_0001.scanName = @"0001";
            office_0001.scanId = @"0001";
            office_0001.createdAt = [NSDate date];
            office_0001.modifiedAt = [NSDate date];
            
            RSScan *office_0002 = [NSEntityDescription insertNewObjectForEntityForName:@"RSScan" inManagedObjectContext:[RSCoreDataStack defaultStack].managedObjectContext];
            office_0002.environmentObj = rsEnv;
            office_0002.scanName = @"0002";
            office_0002.scanId = @"0002";
            office_0002.createdAt = [NSDate date];
            office_0002.modifiedAt = [NSDate date];

            RSScan *office_0003 = [NSEntityDescription insertNewObjectForEntityForName:@"RSScan" inManagedObjectContext:[RSCoreDataStack defaultStack].managedObjectContext];
            office_0003.environmentObj = rsEnv;
            office_0003.scanName = @"0003";
            office_0003.scanId = @"0003";
            office_0003.createdAt = [NSDate date];
            office_0003.modifiedAt = [NSDate date];

            RSScan *office_0004 = [NSEntityDescription insertNewObjectForEntityForName:@"RSScan" inManagedObjectContext:[RSCoreDataStack defaultStack].managedObjectContext];
            office_0004.environmentObj = rsEnv;
            office_0004.scanName = @"0004";
            office_0004.scanId = @"0004";
            office_0004.createdAt = [NSDate date];
            office_0004.modifiedAt = [NSDate date];

//            rsEnv.scans = [NSOrderedSet orderedSetWithArray:@[office_0001, office_0002]];
            rsEnv.scans = [NSOrderedSet orderedSetWithArray:@[office_0001, office_0002, office_0003, office_0004]];
            
            [rsEnv prepareSetting];
            
            // todo:: copy the dummy data to
        }
        
        [userDefaults setBool:YES forKey:initialLoadKey];
        [[RSCoreDataStack defaultStack] saveContext];
    }
}

@end

