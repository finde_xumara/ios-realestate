//
//  RSScanViewController+Camera.h
//  RoomScan
//
//  Created by Finde Xumara on 15/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#define HAS_LIBCXX
#import "RSScanViewController.h"
#import <Structure/Structure.h>

@interface RSScanViewController (Camera) <AVCaptureVideoDataOutputSampleBufferDelegate>

- (void)startColorCamera;
- (void)stopColorCamera;
- (void)setColorCameraParametersForInit;
- (void)setColorCameraParametersForScanning;

@end
