//
//  RSAnnotation.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 22/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RSEnvironment, RSPoint;

NS_ASSUME_NONNULL_BEGIN

@interface RSAnnotation : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
- (void) setupPointsWithOptions:(NSDictionary *)options;

- (NSArray *) topLine;

- (RSPoint *) topLineStart;

@end


NS_ASSUME_NONNULL_END

#import "RSAnnotation+CoreDataProperties.h"
