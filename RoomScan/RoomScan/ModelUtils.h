//
//  ModelUtils.h
//  ServerCommunication
//
//  Created by Daniel Hallin on 07/04/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/MTLValueTransformer.h>
#import <Mantle/NSValueTransformer+MTLPredefinedTransformerAdditions.h>
#import <GLKit/GLKit.h>

@interface ModelUtils : NSObject

+ (NSDateFormatter *)dateFormatter;
+ (NSValueTransformer *)dateJSONTransformer;
+ (NSDateFormatter *)shortDateFormatter;


@end
