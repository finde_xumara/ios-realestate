//
//  RSScanViewModel.h
//  RoomScan
//
//  Created by Finde Xumara on 14/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RSScanViewController.h"

#import <Structure/Structure.h>
#import "RVMViewModel.h"
#import "RVMViewModel+Edits.h"

@class RSEnvironment, RSScan; //TODO + RSEditScanViewModel

@interface RSScanViewModel : RVMViewModel<NSStreamDelegate> {
    
}

@property (nonatomic, readonly) RSScan *model;
@property (nonatomic, assign, getter = isInserting) BOOL inserting;

@property (nonatomic, strong) NSString *scanId;
@property (nonatomic, strong) NSString *scanName;
@property (nonatomic, strong) NSString *serverId;
@property (nonatomic, strong) NSString *environmentId;
@property (nonatomic, strong) NSString *environmentServerId;
@property (nonatomic, strong) NSString *environmentDir;

@property (nonatomic) float progressValue;
@property (nonatomic, strong) NSString *debugMessage;

@property (nonatomic) BOOL isWaitingForDepthSensor;
@property (nonatomic) int numberOfCapturedData;

@property (nonatomic) double lastTimestamp;

@property (nonatomic) double totalStep;

- (instancetype)initWithModel:(id)model;

- (void)cancel;
- (void)willDismiss;

-(void) processDepthFrame:(STDepthFrame *)depthFrame
               colorFrame:(STColorFrame *)colorFrame
            rotopanStatus:(Rotopan *) rotopan
                     pose:(GLKMatrix4) pose;

- (void)prepareScanning;
- (void)wipeLastObjectCache;
- (void)processCache;
- (void) updateServerId:(NSString *)serverId;

- (GLKVector2) getTranslation;
- (void) setTranslation:(GLKVector2) translation;
- (GLKMatrix4) getTransformation;
- (void) setTransformation:(GLKMatrix4) transformationMat;
- (NSDictionary *) getAlignmentMeta;
- (void) generatePly;

@end