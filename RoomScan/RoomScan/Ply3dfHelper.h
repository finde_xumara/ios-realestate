//
//  3dfPly.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 04/11/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#ifndef _dfPly_h
#define _dfPly_h

#import <Foundation/Foundation.h>
@interface Ply3dfHelper : NSObject

+ (void) createPLYFileFrom3DFDir:(NSString *)dir scanName:(NSString *)scan_name;

@end

#endif /* _dfPly_h */
