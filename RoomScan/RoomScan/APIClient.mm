//
//  APIClient.m
//  ServerCommunication
//
//  Created by Daniel Hallin on 04/04/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "APIClient.h"
#import <AFNetworking/AFNetworking.h> 
//#import <AFNetworking/AFURLRequestSerialization.h>
#import "APIClient+Edits.h"

@implementation APIClient

- (instancetype) init
{
//    NSURL *baseURL = [NSURL URLWithString:
//                      [NSString stringWithFormat: @"http://%@:%ld%@", kServerHost, (long)kServerPort, kServerPath]];
    
    NSURL *baseURL = [NSURL URLWithString:
                      [NSString stringWithFormat: @"https://%@%@", kServerHost, kServerPath]];
    
    
    NSLog(@"BaseURL: %@", baseURL);
    self = [super initWithBaseURL: baseURL];
    
    
    // Acceptable Content Types
    NSSet *acceptableContentTypes = [NSSet setWithArray: @[@"text/json",
                                                           @"application/json",
                                                           @"text/plain"]];
    
    
    self.responseSerializer.acceptableContentTypes = acceptableContentTypes;
    
    // Acceptable Status Codes
    NSMutableIndexSet *acceptableStatusCodes = [NSMutableIndexSet new];
    
    [acceptableStatusCodes addIndexesInRange: NSMakeRange(200, 100)];
//    [acceptableStatusCodes addIndexesInRange: NSMakeRange(500, 100)];

    self.responseSerializer.acceptableStatusCodes = acceptableStatusCodes;
    
    // HTTP Methods Encoding Parameters In URI
    self.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithArray:@[@"POST",
                                                                                      @"HEAD",
                                                                                      @"PUT",
                                                                                      @"GET",
                                                                                      @"DELETE"]];
    self.requestSerializer.timeoutInterval = 120.f;

    return self;
}

static APIClient *_sharedClient = nil;

+ (instancetype) sharedClient
{
    if (!_sharedClient)
    {
        _sharedClient = [[APIClient alloc] init];
    }
    return _sharedClient;
}


#pragma mark - Override

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
     constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                      progress:(nullable void (^)(NSProgress * _Nonnull))uploadProgress
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    return [self POST: URLString URIparameters: parameters constructingBodyWithBlock: block progress: uploadProgress success:success failure: failure];
}




@end
