//
//  Sweep.m
//  VirtualRoom
//
//  Created by Finde Xumara on 21/06/16.
//
//

#import "ModelUtils.h"
#import "EnvironmentMTL.h"

@implementation EnvironmentMTL

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"localId" : @"local_id",
             @"name" : @"name",
             @"serverId" : @"id",
             @"createdAt" : @"created_at",
             @"modifiedAt" : @"modified_at"
             };
}


+ (NSValueTransformer *)modifiedAtJSONTransformer {
    return [ModelUtils dateJSONTransformer];
}

+ (NSValueTransformer *)createdAtJSONTransformer {
    return [ModelUtils dateJSONTransformer];
}

@end
