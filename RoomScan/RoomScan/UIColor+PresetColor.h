//
//  UIColor+presetColor.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 22/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PresetColor)

+ (UIColor *) markerSelected;
+ (UIColor *) markerDefault;
+ (UIColor *) pointDefaultStroke;
+ (UIColor *) pointDefaultFill;
+ (UIColor *) textDefault;

@end
