
//  PointCloudRenderer.m
//  RoomScan
//
//  Created by Finde Xumara on 10/05/16.
//  Copyright © 2016 Occipital. All rights reserved.
//

#import "PCGLViewController.h"
#import <OpenGLES/ES2/glext.h>
#import "CommonMath.h"
#import <vector>
#import "Frame3D.h"
#import <opencv2/imgproc.hpp>

#import "RSEnvironmentViewModel.h"
#import "RSScanViewModel.h"
#import "PCLHelper.h"

#import "Ply3dfHelper.h"

// test pcl
#import "PCGLViewController+Annotation.h"
#import "PCGLViewController+Shaders.h"

#import <pcl/point_cloud.h>
#import <pcl/io/ply_io.h>
#import <pcl/filters/voxel_grid.h>
#import <pcl/filters/passthrough.h>

using namespace pcl;
using namespace cv;

@implementation PCGLViewController {
    GLKVector4 _bgColor;
    GLKVector4 _bgColorDefault;
    GLKVector4 _bgColorEditMode;
    GLKVector4 _bgColorAddMarkerMode;
    int base_factor;
    int ortho_factor;
    float aspect;
//    MarkerType _markerType;
    
    CGPoint _centerCamera;
}

#pragma mark - button selectors
-(IBAction)dismissView:(id)sender
{
    UIViewController *dismissingVC = self.presentingViewController ?: self;
    [dismissingVC dismissViewControllerAnimated: YES completion:^{
        
    }];
}

float lastRotationY = 0;
GLKMatrix4 lastTransformation = GLKMatrix4Identity;
GLKVector2 lastTranslation = GLKVector2Make(0, 0);
-(void)rotationGestureRecognized:(UIRotationGestureRecognizer *) sender {
    
    if (_selectedPointcloud == nil) {
        if (sender.state == UIGestureRecognizerStateBegan) {
            lastRotationY = _rotation;
        } else {
            _rotation = lastRotationY + sender.rotation;
        }
    } else {
        if (sender.state == UIGestureRecognizerStateBegan) {
            lastTransformation = [_selectedPointcloud.scanModel getTransformation];
            lastTranslation.x = lastTransformation.m30;
            lastTranslation.y = lastTransformation.m32;
            lastTransformation.m30 = 0;
            lastTransformation.m32 = 0;
        }
        else if ((sender.state == UIGestureRecognizerStateChanged) ||
                 (sender.state == UIGestureRecognizerStateEnded)) {
            
            GLKMatrix4 finalTransformation = GLKMatrix4Rotate(lastTransformation, sender.rotation, 0, -1, 0);
            finalTransformation.m30 = lastTranslation.x;
            finalTransformation.m32 = lastTranslation.y;
            //        finalTransformation = GLKMatrix4Translate(finalTransformation, lastTranslation.x, 0, lastTranslation.y);
            
            [_selectedPointcloud.scanModel setTransformation:finalTransformation];
        }
    }
    
}

-(void)pinchGestureRecognized:(UIPinchGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan) {
        [_annotationView prepareToScale];
        
    } else if (sender.state == UIGestureRecognizerStateChanged){
        if (sender.scale > 1) // scale up
        {
            _pinch_zoom_scale = (1 - sender.scale);
        } else {
            _pinch_zoom_scale = ((1 / sender.scale) - 1);
        }
        
        // check the limit
        float _temp_total = _zoom_scale + _pinch_zoom_scale;
        if (_temp_total <= -1.4) {
            _pinch_zoom_scale = -1.4 -_zoom_scale;
        } else if (_temp_total >= 1.5 ) {
            _pinch_zoom_scale = 1.5-_zoom_scale;
        }
        
        ortho_factor = base_factor * ( 1 + 0.6 * (_zoom_scale + _pinch_zoom_scale) );
        
        [_annotationView updateScaleTo: ortho_factor * 2];
        [_annotationView setNeedsDisplay];
        
    } else if (sender.state == UIGestureRecognizerStateEnded) {
        _zoom_scale += _pinch_zoom_scale;
        _pinch_zoom_scale = 0;
        
        [_annotationView finalizeScaling];
    }
    
}


CGPoint preLoc = CGPointMake(0, 0);
CGPoint lastCenter = CGPointMake(0, 0);
-(void)panGestureRecognized:(UIPanGestureRecognizer *)sender
{
    switch (_viewMode) {
            
        case DEFAULT_MODE:
            
            if (sender.state == UIGestureRecognizerStateBegan) {
                preLoc = [sender locationInView:self.view];
                if (_selectedPointcloud == nil) {
                    lastCenter = _centerCamera;
                } else {
                    lastCenter = VTP([_selectedPointcloud.scanModel getTranslation]);
                }
                
            }
            
            else if ((sender.state == UIGestureRecognizerStateChanged) ||
                     (sender.state == UIGestureRecognizerStateEnded))
            {
                CGPoint postLoc = [sender locationInView:self.view];
                
                GLKVector2 newCenter;
                newCenter.x = lastCenter.x + (postLoc.x - preLoc.x) / self.view.bounds.size.height * 2 * ortho_factor;
                newCenter.y = lastCenter.y + (postLoc.y - preLoc.y) / self.view.bounds.size.height * 2 * ortho_factor;
                
                if (_selectedPointcloud == nil) {
                    _centerCamera = VTP(newCenter);
                    [_annotationView updateCenterTo:_centerCamera];
                } else {
                    [_selectedPointcloud.scanModel setTranslation:newCenter];
                }
            }
            break;
            
        case EDIT_MARKER_MODE: {
            CGPoint location = [sender locationInView:self.view];
            [self handleEditMarkerMode: location sender:sender];
            
        } break;
            
        case ADD_MARKER_MODE: {
            CGPoint location = [sender locationInView:self.view];
            [self handleAddMarkerMode: location state:sender.state];
            
        } break;
            
        default:
            break;
    }
    
}

-(void)addNewByLongPress:(UILongPressGestureRecognizer * )sender {
    
    if (_viewMode != ADD_MARKER_MODE) {
        [self setViewMode:ADD_MARKER_MODE];
    }
    
//    CGPoint location = [sender locationInView:self.view];
    
    [_annotationView setNeedsDisplay];
}

#pragma mark - OES delegates
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    // bind the GLKView
    _glkView = (GLKView *)self.view;
    _glkView.context = self.context;
    _glkView.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    UIPanGestureRecognizer *Annotation_PanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)];
    Annotation_PanRecognizer.minimumNumberOfTouches = 1;
    Annotation_PanRecognizer.maximumNumberOfTouches = 1;
    
//    UIPinchGestureRecognizer *PC_PinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestureRecognized:)];
    UIRotationGestureRecognizer *Annotation_RotationRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotationGestureRecognized:)];
    
    //    PC_PinchRecognizer.delegate=self;
    //    PC_RotationRecognizer.delegate=self;
    
    UILongPressGestureRecognizer *Annotation_addNewByLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(addNewByLongPress:)];
    Annotation_addNewByLongPress.minimumPressDuration = 1.0;
    
    [self.view addGestureRecognizer:Annotation_PanRecognizer];
    [self.view addGestureRecognizer:Annotation_addNewByLongPress];
    [self.view addGestureRecognizer:Annotation_RotationRecognizer];
    
    [self setupAnnotationView];
    
    // Setup the init variable
    [self initView];
    
    [self setupGL];
    
    self.paused=NO;
}

- (void)dealloc
{
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Functions

- (void) initView
{
    aspect = fabs(self.view.bounds.size.width / self.view.bounds.size.height);
    
    _viewMode = DEFAULT_MODE;
    //    _viewMode = ADD_MARKER_MODE;
    _bgColorDefault = GLKVector4Make(.30, .10, .10, 1.); // GLKVector4Make(.65, .65, .65, 1.);
    _bgColorEditMode = GLKVector4Make(.0, .0, .0, 1.);
    _bgColorAddMarkerMode = GLKVector4Make(.30, .10, .10, 1.);
    
    _isOrtho = YES;
    base_factor = 7;
    ortho_factor = base_factor;
    
    _annotationView.envViewModel = self.viewModel;
    _annotationView.base_scale = base_factor * 2;
    _annotationView.scale = base_factor * 2;
    _annotationView.width = self.view.bounds.size.width;
    _annotationView.height = self.view.bounds.size.height;
    [_annotationView setHidden:YES];
    [_annotationView setNeedsDisplay];
    
    _pointclouds = [[NSMutableArray alloc] init];
    _selectedPointcloud = nil;
    
    _centerCamera = CGPointMake(0, 0);
    
    [self updateUIComponent];
}

- (void) setViewMode:(ViewMode) newViewMode {
    _viewMode = newViewMode;
    [self updateUIComponent];
}

- (void) updateUIComponent
{
    switch (_viewMode) {
        case EDIT_ANCHOR_MODE:
        case EDIT_MARKER_MODE:
        case ADD_ANCHOR_MODE:
        case ADD_MARKER_MODE:
            _bgColor = _bgColorEditMode;
            //            _markerView.hidden = NO;
            //            _projectionBox.hidden = YES;
            break;
            
        case DEFAULT_MODE:
        default:
            _bgColor = _bgColorDefault;
            //            _markerView.hidden = YES;
            //            _projectionBox.hidden = NO;
            break;
    }
    
//    [_annotationView setHidden:!_isOrtho];
}

- (void) setupGL
{
    [EAGLContext setCurrentContext:self.context];
    
    [self loadShaders];
    
    glEnable(GL_DEPTH_TEST);
    
    [self loadPointCloud];
}

- (void) tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
    
    glDeleteBuffers(1, &_vertexBuffer);
    glDeleteVertexArraysOES(1, &_vertexArray);
    
    if (_program) {
        glDeleteProgram(_program);
        _program = 0;
    }
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update
{
    GLKMatrix4 projectionMatrix;
    GLKMatrix4 modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, 0.0f);
//    
//    if(_isOrtho == YES) {
//        
        projectionMatrix = GLKMatrix4MakeOrtho(-ortho_factor*aspect,
                                               ortho_factor*aspect,
                                               -ortho_factor,
                                               ortho_factor, 0.001f, 1000.0f);
        
        modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, GLKMathDegreesToRadians(90.f), 1.0f, 0.0f, 0.0f);
        modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, _rotation, 0.0f, 1.0f, 0.0f);
        
        modelViewMatrix = GLKMatrix4Translate(modelViewMatrix, _centerCamera.x, -10.0f, _centerCamera.y);
//    } else {
//        float scale = 65.0f * ( 1 + (_zoom_scale + _pinch_zoom_scale) * 0.6);
//        projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(scale), aspect, 0.001f, 100.0f);
//        
//        modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, _tilt, -1.0f, 0.0f, 0.0f);
//        modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, _rotation, 0.0f, -1.0f, 0.0f);
//    }
    
    _normalMatrix = GLKMatrix3InvertAndTranspose(GLKMatrix4GetMatrix3(modelViewMatrix), NULL);
    _modelViewProjectionMatrix = GLKMatrix4Multiply(projectionMatrix, modelViewMatrix);
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    glClearColor(_bgColor.r, _bgColor.g, _bgColor.b, _bgColor.a);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glBindVertexArrayOES(_vertexArray);
    
    glUseProgram(_program);
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    for (Pointcloud *pc in _pointclouds) {
        
        if (_selectedPointcloud != nil && pc.first == _selectedPointcloud.first) {
            continue;
        }
        
        GLKMatrix4 finalMVP = GLKMatrix4Multiply(_modelViewProjectionMatrix, [pc.scanModel getTransformation]);
        
        glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX], 1, 0, finalMVP.m);
        glUniformMatrix3fv(uniforms[UNIFORM_NORMAL_MATRIX], 1, 0, _normalMatrix.m);
        
        if (_selectedPointcloud == nil) {
            glUniform1f(uniforms[UNIFORM_OPACITY], 1.0f);
        } else if (_selectedPointcloud != nil && pc.first != _selectedPointcloud.first) {
            glUniform1f(uniforms[UNIFORM_OPACITY], 0.2f);
        }
        
        glDrawArrays(GL_POINTS, pc.first, pc.count);
    }
    
    Pointcloud * pc = _selectedPointcloud;
    GLKMatrix4 finalMVP = GLKMatrix4Multiply(_modelViewProjectionMatrix, [pc.scanModel getTransformation]);
    finalMVP = GLKMatrix4Translate(finalMVP, 0, 5.0f, 0);
    
    glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX], 1, 0, finalMVP.m);
    glUniformMatrix3fv(uniforms[UNIFORM_NORMAL_MATRIX], 1, 0, _normalMatrix.m);
    glUniform1f(uniforms[UNIFORM_OPACITY], 1.0f);
    glDrawArrays(GL_POINTS, pc.first, pc.count);
    
}

#pragma mark - PointCloud Interaction

- (NSURL *) applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

//- (std::vector<Frame3D>) loadFrames:(NSString *)dir scanName:(NSString *)scan_name {
//    
//    NSArray * frame3DFiles = [self getFrame3DFiles:dir scanName:scan_name];
//    
//    std::vector<Frame3D> frames;
//    
//    for (NSURL *framePath in frame3DFiles) {
//        std::string strPath = std::string([[framePath path] UTF8String]);
//        // do something with object
//        Frame3D frame(strPath);
//        frames.push_back(frame);
//    }
//    
//    return frames;
//}

- (BOOL) loadPointCloud
{
    //TODO:    [self loadAnnotation];
    NSString * dir = [[self.viewModel environmentPath] path];
    std::vector<pcl::PointCloudPtr<pcl::PointXYZRGBNormal>> scans;
    int modelSize = 0;
    
    for (int i=0, limit = self.viewModel.numberOfScans; i<limit; i++ ) {
        
        NSString * scan_name = [self.viewModel scanTitleAtIndexPath:i];
        
        // create pointcloud
        if (![[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/scans/%@.ply", dir, scan_name]]){
            [Ply3dfHelper createPLYFileFrom3DFDir:dir scanName:scan_name];
        }
        
        // read
        std::string filepath = std::string([[NSString stringWithFormat:@"%@/scans/%@-viz.ply", dir, scan_name] UTF8String]);
        pcl::PointCloudPtr<pcl::PointXYZRGBNormal> model (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
        pcl::io::loadPLYFile(filepath, *model);
        
        modelSize += model->points.size();
        scans.push_back(model);
    }
    
    // allocate memory buffer
    GLfloat * _gPointCloudVertexData = (GLfloat *) malloc(modelSize * 6 * 24 * sizeof(GLfloat));
    int last_index = 0;
    int idx = 0;
    for (int i=0, limit = self.viewModel.numberOfScans; i<limit; i++ ) {
        
        //        last_index = idx;
        pcl::PointCloudPtr<pcl::PointXYZRGBNormal> scan = scans[i];
        NSString * scan_name = [self.viewModel scanTitleAtIndexPath:i];
        
        // add to array
        for (pcl::PointXYZRGBNormal &p:scan->points) {
            //            NSLog(@"%f %f %f", p.x, p.y, p.z);
            _gPointCloudVertexData[idx++] = (GLfloat) p.x;
            _gPointCloudVertexData[idx++] = (GLfloat) p.y;
            _gPointCloudVertexData[idx++] = (GLfloat) p.z;
            
            
            // rgb => bgr
            _gPointCloudVertexData[idx++] = (GLfloat) p.r / 255.0;
            _gPointCloudVertexData[idx++] = (GLfloat) p.g / 255.0;
            _gPointCloudVertexData[idx++] = (GLfloat) p.b / 255.0;
        }
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"_scanID == %@", scan_name];
        NSArray *filteredArray = [_pointclouds filteredArrayUsingPredicate:predicate];
        
        Pointcloud *pc;
        
        if (filteredArray.count == 0 ) {
            pc = [[Pointcloud alloc] init];
            pc.scanID = scan_name;
        } else {
            pc = [filteredArray objectAtIndex:0];
        }
        
        pc.first = last_index / 6;
        pc.count = (idx - last_index) / 6;
        last_index = idx;
        
        if (filteredArray.count == 0 ) {
            RSScanViewModel * scanModel = [self.viewModel scanAtIndex:i];
            pc.scanModel = scanModel;
            
            //reset
//            [pc.scanModel setTransformation:GLKMatrix4Identity];
            [_pointclouds addObject:pc];
        }
    }
    
    // create VAO
    glGenVertexArraysOES(1, &_vertexArray);
    glBindVertexArrayOES(_vertexArray);
    
    // create VBO + copy to GPU
    glGenBuffers(1, &_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * idx, _gPointCloudVertexData, GL_STATIC_DRAW);
    free(_gPointCloudVertexData);
    
    // map to shader attribute
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 24, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(GLKVertexAttribColor);
    glVertexAttribPointer(GLKVertexAttribColor, 3, GL_FLOAT, GL_FALSE, 24, BUFFER_OFFSET(12));
    
    glBindVertexArrayOES(0);
    
    _vertexArraySize = idx / 6;
    
    return YES;
}

- (void) selectScanAtIndex:(int) index {
    if (index == -1) {
        _selectedPointcloud = nil;
    } else {
        _selectedPointcloud = [_pointclouds objectAtIndex:index];
    }
    [self.view setNeedsDisplay];
}

- (void) automaticAlignment {
    
    //TODO:    [self loadAnnotation];
    NSString * dir = [[self.viewModel environmentPath] path];
    std::vector<pcl::PointCloudPtr<pcl::PointXYZRGBNormal>> scans;

    GLKMatrix4 prevTransformation = GLKMatrix4Identity;
    pcl::PointCloudPtr<pcl::PointXYZRGBNormal> prev_scan (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    for (int i=0, limit = self.viewModel.numberOfScans; i<limit; i++) {
        Pointcloud * p = _pointclouds[i];
        
        NSString *scan_name = [self.viewModel scanTitleAtIndexPath:i];
        NSString *filepath_NSString = [NSString stringWithFormat:@"%@/scans/%@.ply", dir, scan_name];
        std::string filepath = std::string([filepath_NSString UTF8String]);
        if (![[NSFileManager defaultManager] fileExistsAtPath:filepath_NSString]){
            [Ply3dfHelper createPLYFileFrom3DFDir:dir scanName:scan_name];
        }
        
        pcl::PointCloudPtr<pcl::PointXYZRGBNormal> cur_scan (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
        pcl::io::loadPLYFile(filepath, *cur_scan);
        
        if (i == 0) {
            [p.scanModel setTransformation: GLKMatrix4Identity];
        } else {
            Eigen::Affine3f poseEstimation;
            
            estimateCameraPose(prev_scan, cur_scan, poseEstimation, 0.01, 3.0);
            
            GLKMatrix4 newTransformation = GLKMatrix4Make(poseEstimation(0,0), poseEstimation(1,0), poseEstimation(2,0), poseEstimation(3,0),
                                                          poseEstimation(0,1), poseEstimation(1,1), poseEstimation(2,1), poseEstimation(3,1),
                                                          poseEstimation(0,2), poseEstimation(1,2), poseEstimation(2,2), poseEstimation(3,2),
                                                          poseEstimation(0,3), poseEstimation(1,3), poseEstimation(2,3), poseEstimation(3,3));
            newTransformation = GLKMatrix4Multiply(prevTransformation, newTransformation);
            [p.scanModel setTransformation: newTransformation];
            prevTransformation = newTransformation;
        }
        
        pcl::copyPointCloud(*cur_scan, *prev_scan);
        
        [self.view setNeedsDisplay];
    }
}

@end
