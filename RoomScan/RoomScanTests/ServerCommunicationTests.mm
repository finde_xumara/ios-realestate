//
//  RoomScanTests.m
//  RoomScanTests
//
//  Created by Finde Xumara on 06/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CoreData/CoreData.h>
//
#import "APIClient.h"
#import "EnvironmentService.h"
#import "RSCoreDataStack.h"
#import "RSGalleryViewModel.h"


@interface RoomScanTests : XCTestCase {
    RSGalleryViewModel *_viewModel;
    EnvironmentService *_environmentService;
} @end

@implementation RoomScanTests
- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    _environmentService = [[EnvironmentService alloc] init];
    _viewModel = [[RSGalleryViewModel alloc] initWithModel:[RSCoreDataStack defaultStack].managedObjectContext];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testUploadEnvironment {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    NSLog(@"started");
    
    RSEnvironmentViewModel *model_0 = [_viewModel environmentModelForIndexPath:0];
    
}

//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

@end
