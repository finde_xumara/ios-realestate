//
//  BatteryView.m
//  Scanner
//
//  Created by Daniel Hallin on 17/11/15.
//  Copyright © 2015 Occipital. All rights reserved.
//

#import "SensorBatteryView.h"

@implementation SensorBatteryView

- (void) setNeedsDisplay
{
    int batteryLevelPercentage = _sensorController.getBatteryChargePercentage;
    CGFloat batteryLevel = batteryLevelPercentage / 100.f;

    _batteryLevelContainer.batteryLevel = batteryLevel;
    
    _batteryLevelLabel.text = [NSString stringWithFormat: @"%d%%", batteryLevelPercentage];
    
    // Update BatterLevelLabelFrame
    CGFloat padding = 3.f;
    CGRect frame = _batteryLevelLabel.frame;
    if (batteryLevelPercentage < 50)
    {
        frame.origin.x = padding + _batteryLevelContainer.frame.origin.x +
                         _batteryLevelContainer.frame.size.width * batteryLevel;
    } else {
        frame.origin.x = padding + _batteryLevelContainer.frame.origin.x;
    }
    _batteryLevelLabel.frame = frame;
    
    [super setNeedsDisplay];
    
    [_batteryLevelContainer setNeedsDisplay];
}

@end


@implementation BatteryLevelContainer

- (void) drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextClearRect(context, rect);
    
    UIColor * color = _batteryLevel <= lowPowerLimit ?
                      kLowPowerColor : kHighPowerColor;
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    
    CGRect batteryLevelRect = CGRectMake(0, 0,
                                 self.bounds.size.width * self.batteryLevel,
                                 self.bounds.size.height);
    
    CGContextFillRect(context, batteryLevelRect);
}

@end