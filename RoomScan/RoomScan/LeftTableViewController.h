//
//  LeftTableViewController.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 17/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RSEnvironmentViewModel;

@interface LeftTableViewController : UIViewController

@property (nonatomic, strong) NSString* selectedCategory;
@property (nonatomic, strong) IBOutlet UITableView * tableView;
@property (nonatomic, strong) IBOutlet UISegmentedControl * segmentControl;

@property (nonatomic, strong) RSEnvironmentViewModel *viewModel;

- (void) reloadData:(NSInteger) index;

@end
