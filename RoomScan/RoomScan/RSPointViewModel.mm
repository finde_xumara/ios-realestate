//
//  RSAnnotationViewModel.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 23/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RSPointViewModel.h"

#import "RSPoint.h"

@interface RSPointViewModel ()
    @property (nonatomic, strong) RSPoint *model;
@end

@implementation RSPointViewModel

- (instancetype)initWithModel: (id)model {
    self = [super initWithAPIManager];
    
    self.model = model;
    
    if (self == nil) return nil;
    
    RAC(self, x) = [RACObserve(self.model, x) map:^id(NSNumber *value) {
        return value;
    }];
    
    RAC(self, y) = [RACObserve(self.model, y) map:^id(NSNumber *value) {
        return value;
    }];
    
    RAC(self, isEdge) = [RACObserve(self.model, isEdge) map:^id(NSNumber *value) {
        return value;
    }];
    
    return self;
}

- (void)setPositionWithCGPoint: (CGPoint)location {
    [self setPositionWithVector:PTV(location)];
}

- (void)setPositionWithVector: (GLKVector2)location {
    self.model.x = [NSNumber numberWithDouble:location.x];
    self.model.y = [NSNumber numberWithDouble:location.y];
}

- (float)hitSquareDistance: (GLKVector2)point {
    GLKVector2 diff = GLKVector2Subtract([self getVector], point);
    return GLKVector2DotProduct(diff, diff);
}

- (GLKVector2) getVector {
    return GLKVector2Make([self.model.x doubleValue], [self.model.y doubleValue]);
}

@end
