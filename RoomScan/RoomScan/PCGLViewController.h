//
//  PointCloudRenderer.h
//  RoomScan
//
//  Created by Finde Xumara on 10/05/16.
//  Copyright © 2016 Occipital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

#import "AnnotationView.h"
#import "PointCloud.h"


@class RSEnvironmentViewModel, RSScanViewModel;

@interface PCGLViewController : GLKViewController {
    // Uniform index.
    enum
    {
        UNIFORM_MODELVIEWPROJECTION_MATRIX,
        UNIFORM_NORMAL_MATRIX,
        UNIFORM_OPACITY,
        NUM_UNIFORMS
    };
    GLint uniforms[NUM_UNIFORMS];
    
    // Attribute index.
    enum
    {
        ATTRIB_VERTEX,
        ATTRIB_NORMAL,
        ATTRIB_COLOR,
        NUM_ATTRIBUTES
    };
    
    GLuint _program;
    
    GLKMatrix4 _modelViewProjectionMatrix;
    GLKMatrix3 _normalMatrix;
    float _rotation;
    float _tilt;
    float _zoom_scale;
    float _pinch_zoom_scale;
    BOOL _isOrtho;
    
    GLuint _vertexArray;
    GLuint _vertexBuffer;
    
    int _vertexArraySize;
    
    Pointcloud *_selectedPointcloud;
    ViewMode _viewMode;
}

@property (strong, nonatomic) GLKView *glkView;
@property (strong, nonatomic) IBOutlet AnnotationView *annotationView;
//@property (nonatomic, strong) Scan *scan;

@property (nonatomic, strong) RSEnvironmentViewModel *viewModel;

@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) GLKBaseEffect *effect;

//@property (nonatomic, strong) IBOutlet UIView *projectionBox;

@property(strong, nonatomic) NSMutableArray *pointclouds;

//@property (nonatomic, strong) IBOutlet UITableView *scanView;
//@property (nonatomic, strong) IBOutlet UITableView *markerView;

//@property (nonatomic, strong) IBOutlet UIButton *saveButton;

- (void)setupGL;
- (void)tearDownGL;

//- (BOOL)loadShaders;
//- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file;
//- (BOOL)linkProgram:(GLuint)prog;
//- (BOOL)validateProgram:(GLuint)prog;

- (void)selectScanAtIndex:(int) index;
- (void)setViewMode:(ViewMode) newViewMode;

- (void)updateUIComponent;
- (void)automaticAlignment;
//- (IBAction)saveAnnotation:(id)sender;
//- (void)loadAnnotation;

@end
