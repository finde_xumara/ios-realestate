//
//  RSScanEnvironmentViewController.h
//  RoomScan
//
//  Created by Finde Xumara on 14/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//
#import <CoreMotion/CoreMotion.h>
#import <vector>
#import <UIKit/UIKit.h>
#import <Structure/Structure.h>
#import "BLE.h"
#import "CalibrationOverlay.h"

// UI connection icon
#import "SensorBatteryView.h"
#import "RotopanStatusView.h"
//#import "ServiceView.h"
//#import "RotopanView.h"

// Add categories
#import "Rotopan.h"

@class RSEnvironmentViewModel, RSScanViewModel;

struct Options
{
    // The initial scanning volume size will be 6.0 x 4.0 x 6.0 meters
    GLKVector3 initialVolumeSizeInMeters = GLKVector3Make (10.f, 8.f, 10.f);
    
    // Resolution for the initialVolumeSizeInMeters. Will get scaled if we change the volume size.
    const float initialVolumeResolutionInMeters = 0.05; // coarse, but gives good performance
    
    // The minimal vertical volume size is fixed, since the ceiling is not likely to be very low.
    float minVerticalVolumeSize = 3.f;
    
    // The maximum of keyframes for keyFrameManager. More won't fit in a single OpenGL texture.
    int maxNumKeyframes = 48;
    
    // Take a new keyframe in the rotation difference is higher than 20 degrees.
    float maxKeyFrameRotation = 20.0f * (M_PI / 180.f);
    
    // Take a new keyframe if the translation difference is higher than 30 cm.
    float maxKeyFrameTranslation = 0.3;
    
    // Threshold to consider that the rotation motion was small enough for a frame to be accepted
    // as a keyframe. This avoids capturing keyframes with strong motion blur / rolling shutter.
    float maxKeyframeRotationSpeedInDegreesPerSecond = 1.f;
    
    // Threshold to pop a warning to the user if he's exploring too far away since this demo is optimized
    // for a rotation around oneself.
    float maxDistanceFromInitialPositionInMeters = 1.f;
    
    // Fixed focus position of the color camera.
    float colorCameraLensPosition = 0.75f; // 0.75 gives pretty good focus for a room scale.
    
    // Whether we should use depth aligned to the color viewpoint when Structure Sensor was calibrated.
    // This setting may get overwritten to false if no color camera can be used.
    bool useHardwareRegisteredDepth = true;
    
    // Whether to enable an expensive per-frame depth accuracy refinement.
    // Note: this option requires useHardwareRegisteredDepth to be set to false.
    const bool applyExpensiveCorrectionToDepth = false;
};

enum RSState
{
    // Defining the options
    RSStateNil = 0,
    
    RSStateInitialized,
    
    // Scanning
    RSStateScanning,
    
    // Finalizing the scanning
    RSStateFinalizing
};

struct AppStatus
{
    NSString* const pleaseConnectSensorMessage = @"Please connect Structure Sensor.";
    NSString* const pleaseChargeSensorMessage = @"Please charge Structure Sensor.";
    
    NSString* const needColorCameraAccessMessage = @"This app requires camera access to capture rooms.\nAllow access by going to Settings → Privacy → Camera.";
    NSString* const needCalibratedColorCameraMessage = @"This app requires an iOS device with a supported bracket.";
    
    enum SensorStatus
    {
        SensorStatusOk,
        SensorStatusNeedsUserToConnect,
        SensorStatusNeedsUserToCharge,
    };
    
    SensorStatus sensorStatus = SensorStatusOk;
    RSState roomScanState = RSStateNil;
    
    // Whether iOS camera access was granted by the user.
    bool colorCameraIsAuthorized = true;
    
    // Whether the current iOS device has a supported bracket and thus a calibrated color camera.
    bool colorCameraIsCalibrated = true;
    
    // Whether there is currently a message to show.
    bool needsDisplayOfStatusMessage = false;
    
    // Flag to disable entirely status message display.
    bool statusMessageDisabled = false;
};

// Display related members.
struct DisplayData
{
    ~DisplayData ()
    {
        if (lumaTexture)
        {
            CFRelease (lumaTexture);
            lumaTexture = NULL;
        }
        
        if (chromaTexture)
        {
            CFRelease(chromaTexture);
            chromaTexture = NULL;
        }
        
        if (videoTextureCache)
        {
            CFRelease(videoTextureCache);
            videoTextureCache = NULL;
        }
    }
    
    // OpenGL context.
    EAGLContext *context = nil;
    
    // OpenGL Texture reference for y images.
    CVOpenGLESTextureRef lumaTexture = NULL;
    
    // OpenGL Texture reference for color images.
    CVOpenGLESTextureRef chromaTexture = NULL;
    
    // OpenGL Texture cache for the color camera.
    CVOpenGLESTextureCacheRef videoTextureCache = NULL;
    
    // Shader to render a GL texture as a simple quad. YCbCr version.
    STGLTextureShaderYCbCr *yCbCrTextureShader = nil;
    
    // Shader to render a GL texture as a simple quad. RGBA version.
    STGLTextureShaderRGBA *rgbaTextureShader = nil;
    
    // Used during initialization to show which depth pixels lies in the scanning volume boundaries.
    std::vector<uint8_t> scanningVolumeFeedbackBuffer;
    GLuint scanningVolumeFeedbackTexture = -1;
    
    // OpenGL viewport.
    GLfloat viewport[4] = {0,0,0,0};
    
    // OpenGL projection matrix for the color camera.
    GLKMatrix4 colorCameraGLProjectionMatrix = GLKMatrix4Identity;
};

@interface RSScanViewController : UIViewController<UIPopoverControllerDelegate, BLEDelegate, STBackgroundTaskDelegate> {
    
    CalibrationOverlay * _calibrationOverlay;
    STSensorController *_sensorController;
    BLE *_bleShield;
    dispatch_group_t _scanTask;
    
    Rotopan * _rotopan;
    Options _options;
    
    DisplayData _display;
    AppStatus _appStatus;
    
    // Most recent gravity vector from IMU.
    GLKVector3 _lastCoreMotionGravity;
    
    // IMU handling.
    CMMotionManager *_motionManager;
    NSOperationQueue *_imuQueue;
}

@property (nonatomic, retain) AVCaptureSession *avCaptureSession;
@property (nonatomic, retain) AVCaptureDevice *videoDevice;

@property (nonatomic, strong) RSScanViewModel *viewModel;
@property (weak, nonatomic) IBOutlet UIButton *connectRotopanButton;

@property (weak, nonatomic) IBOutlet SensorBatteryView *sensorBatteryView;
@property (weak, nonatomic) IBOutlet RotopanStatusView *rotopanStatusView;
@property (weak, nonatomic) IBOutlet UIImageView *RGB_view;
@property (weak, nonatomic) IBOutlet UIImageView *Depth_view;

@property (nonatomic, strong) IBOutlet MBCircularProgressBarView * progressCircle;

@property (nonatomic, strong) IBOutlet UILabel * progressLabel;
@property (nonatomic, strong) IBOutlet UITextView * debugTextView;

@property (nonatomic, strong) IBOutlet UIButton * scanButton;
@property (nonatomic, strong) IBOutlet UIButton * resetButton;
@property (nonatomic, strong) IBOutlet UIButton * doneButton;

@property (nonatomic, strong) IBOutlet UISwitch * highGainButton;

//////// Scan
//-(void) startScanning; //
//-(void) processScanData; //
//-(void) stopScanning; //
-(void) processDepthFrame:(STDepthFrame *)depthFrame
               colorFrame:(STColorFrame *)colorFrame;
////////

// UI interaction
-(IBAction) cancelWasPressed:(id)sender;
-(IBAction) scanWasPressed:(id)sender;
-(IBAction) resetWasPressed:(id)sender;

// UI
-(void) setupUserInterface;
-(void) updateAppStatusMessage;
//-(void) appendDebugMessage:(NSString *) message;

// state
- (void)enterPoseInitializationState;
- (void)enterScanningState;
- (void)enterViewingState;
- (void)enterFinalizingState;

//- (void)adjustVolumeSize:(GLKVector3)volumeSize;
- (BOOL)currentStateNeedsSensor;
- (void)updateIdleTimer;
//- (void)showTrackingMessage:(NSString*)message;
//- (void)hideTrackingErrorMessage;

- (void)setUIScanInit;
- (void)setUIScanReady;



@end