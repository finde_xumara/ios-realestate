//
//  ZipService.m
//  ServerCommunication
//
//  Created by Daniel Hallin on 19/05/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "ZipService.h"

#import <SSZipArchive/SSZipArchive.h>
//#import "NSFilemanager+Extensions.h"

@implementation ZipService

- (void) zipDirectory: (NSString*) directory
             progress: (ServiceProgressBlock) progress
           completion: (ServiceCompletionBlock) serviceCompletion
{
    //    NSString *sourcePath = directory;
    //    NSString *destinationPath =  [NSFileManager tempDirectoryWithName: directory.lastPathComponent];
    //    destinationPath = [destinationPath stringByAppendingPathExtension: @"tar"];
    //
    //    [[NVHTarGzip sharedInstance] tarFileAtPath:sourcePath toPath:destinationPath completion:^(NSError* tarError) {
    //        if (tarError != nil) {
    //            NSLog(@"Error tarring %@", tarError);
    //        }
    //        NSString *retval = (tarError) ? nil : destinationPath;
    //        if (serviceCompletion) serviceCompletion(retval, tarError);
    //    }];
}

+ (NSData*) zippedDataFromDirectory: (NSString*) directory
{
    //    NSString *destinationPath =  [NSFileManager tempDirectoryWithName: directory.lastPathComponent];
    NSString *destinationPath =  [NSTemporaryDirectory() stringByAppendingString: directory.lastPathComponent];
    destinationPath = [destinationPath stringByAppendingPathExtension: @"zip"];
    
    [SSZipArchive createZipFileAtPath: destinationPath withContentsOfDirectory: directory];
    NSData *returnData = nil;
    
    returnData = [NSData dataWithContentsOfFile: destinationPath];
    
    return returnData;
}


@end
