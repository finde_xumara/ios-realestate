//
//  PCLHelper.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 17/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <vector>
#import "Eigen/StdVector"
#import "Eigen/Geometry"
#import <pcl/point_types.h>
#import <pcl/common/transforms.h>
#import "FastPCL.h"
#import <opencv2/imgproc.hpp>

@interface PCLHelper : NSObject

//void rotationMatrixFromAngles(Eigen::Matrix3f& pose, float angle_x, float angle_y, float angle_z);
//void rotationMatrixFromDegAngles(Eigen::Matrix3f& pose, float angle_x, float angle_y, float angle_z);
//void rotationMatrixFromAngles(Eigen::Affine3f& pose, float angle_x, float angle_y, float angle_z, float trans_x, float trans_y, float trans_z);
//void rotationMatrixFromDegAngles(Eigen::Affine3f& pose, float angle_x, float angle_y, float angle_z, float trans_x, float trans_y, float trans_z);
//void zeroRemoveWithColor(pcl::PointCloudPtr<pcl::PointXYZRGBNormal>& points1, pcl::PointCloudPtr<pcl::PointXYZRGBNormal>& points2);
//void addOnePointCloudWithNormalColor(cv::Mat& depth, cv::Mat& color, pcl::PointCloudPtr<pcl::PointXYZRGBNormal>& model, float focal_length, float cx, float cy, float threah);
void addPointCloudsWithNormalColor(std::vector<cv::Mat>& images,
                                   std::vector<cv::Mat>& depths,
                                   std::vector<cv::Mat>& poses,
                                   std::vector<cv::Vec3f>& rots,
                                   std::vector<pcl::PointCloudPtr<pcl::PointXYZRGBNormal>>& global_points,
                                   std::vector<Eigen::Affine3f>& global_poses,
                                   float xrot, float yrot, float zrot, float xlength, float ylength, float zlength, float focal_length, float cx, float cy, float dist_thresh);
//pcl::PointCloudPtr<pcl::PointNormal> statistical_removal(pcl::PointCloudPtr<pcl::PointNormal> cloud, int meanK, float stdThreshold);
//void voxtelGridDownsampling(pcl::PointCloudPtr<pcl::PointNormal> &cloud, float leafSize);
//void rotate_90n(cv::Mat &src, cv::Mat &dst, int angle);
void depth2Pointcloud(cv::Mat& depth, cv::Mat& color,
                      pcl::PointCloudPtr<pcl::PointXYZRGBNormal>& pointcloud,
                      float focal_length, float thresh);

////
void findTranslation(float tilt, float pan, float& xoffset, float& yoffset, float& zoffset);
void composeRotation(float x, float y, float z, Eigen::Affine3f& R);
void median(std::vector<float>& values, float& val_med);

void estimateCameraPose(
                        pcl::PointCloudPtr<pcl::PointXYZRGBNormal>& cloud0,
                        pcl::PointCloudPtr<pcl::PointXYZRGBNormal>& cloud1,
                        Eigen::Affine3f& relative_pose,
                        float leaf_size/*=0.05*/, float dist_threshold/*=5.0*/
);

void mergeSingleScan(
                     std::vector<cv::Mat> images,
                     std::vector<cv::Mat> depths,
                     std::vector<std::vector<float>> poses_imu,
                     std::vector<std::vector<float>> poses_device,
                     
                     pcl::PointCloudPtr<pcl::PointXYZRGBNormal>& cloud,
                     std::vector<pcl::PointCloudPtr<pcl::PointXYZRGBNormal>>& clouds,
                     std::vector<Eigen::Affine3f>& poses,
                     
                     float leaf_size, float thresh
                     );

void filterTopView (pcl::PointCloudPtr<pcl::PointXYZRGBNormal>& pointcloud,
                    float leafSize/*=0.01f*/);

- (NSArray *) getFrame3DFiles:(NSString *)dir scanName:(NSString *)scan_name;

@end
