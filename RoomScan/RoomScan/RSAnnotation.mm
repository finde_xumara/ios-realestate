//
//  RSAnnotation.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 22/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RSAnnotation.h"
#import "RSPoint.h"
#import "RSEnvironment.h"

#import "RSCoreDataStack.h"

@implementation RSAnnotation

// Insert code here to add functionality to your managed object subclass

- (void) setupPointsWithOptions:(NSDictionary *)options {
    
    CGPoint from = [options[@"from"] CGPointValue];
    CGPoint to = [options[@"to"] CGPointValue];
    CGPoint mid = MID(from, to);
    
    NSMutableArray *points = [[NSMutableArray alloc] init];
    for (int i=0; i<8; i++) {
        RSPoint *point = [NSEntityDescription insertNewObjectForEntityForName:@"RSPoint" inManagedObjectContext:[RSCoreDataStack defaultStack].managedObjectContext];
        
        GLKVector2 pointLocation = VEC0;
        
        if (i==0) {
            pointLocation = PTV(from);
        } else if (i==1) {
            pointLocation = PTV(to);
        } else if (i==2) {
            pointLocation = PTV(mid);
        }
        
        point.x = [NSNumber numberWithDouble:pointLocation.x];
        point.y = [NSNumber numberWithDouble:pointLocation.y];
        
        point.isEdge = [NSNumber numberWithBool:NO];
        
        if (i==0 || i==1 || i==3 || i==4) {
            point.isEdge = [NSNumber numberWithBool:YES];
        }
        
        point.annotationObj = self;
        [points addObject:point];
    }
    
    self.points = [NSOrderedSet orderedSetWithArray:points];
}

- (NSArray *) topLine {
    return @[self.points[0], self.points[1]];
}

- (RSPoint *) topLineStart {
    return [self.points objectAtIndex:0];
}

@end
