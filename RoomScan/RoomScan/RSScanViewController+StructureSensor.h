//
//  RSScanViewController+StructureSensor.h
//  RoomScan
//
//  Created by Finde Xumara on 14/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#define HAS_LIBCXX

#import "RSScanViewController.h"
#import <Structure/Structure.h>

@interface RSScanViewController (StructureSensor) <STSensorControllerDelegate>

- (STSensorControllerInitStatus)connectToStructureSensorAndStartStreaming;
- (void)setupStructureSensor;
- (BOOL)isStructureConnectedAndCharged;

@end
