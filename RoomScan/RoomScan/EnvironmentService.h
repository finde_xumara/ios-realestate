//
//  ScanService.h
//  ServerCommunication
//
//  Created by Daniel Hallin on 21/04/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "Service.h"
//#import "Scan.h"

@class RSEnvironmentViewModel, RSScanViewModel, RSAnnotationViewModel, RSPointViewModel;

@interface EnvironmentService : NetworkService

- (RACSignal *) login:(RSEnvironmentViewModel *) viewModel;
- (RACSignal *) fetchEnvironments;
- (RACSignal *) fetchEnvironment:(RSEnvironmentViewModel *) viewModel;
- (RACSignal *) createNewEnvironmentWithViewModel:(RSEnvironmentViewModel *) viewModel;
- (RACSignal *) createNewScanWithViewModel:(RSScanViewModel *) viewModel;
- (RACSignal *) uploadScanDataWithViewModel:(RSScanViewModel *) viewModel;
- (RACSignal *) uploadContinousDepth:(NSData *) depthData
                               Color:(NSData *) colorData
                                Pose:(NSString *) pose
                                 Tag:(int) tag
                             ModelID:(NSString *) modelID;
- (RACSignal *) upload3DFrame:(NSData *) frame3d
                          Tag:(int) tag
//                         Pose:(NSString *) pose
                      ModelID:(NSString *) modelID;
@end