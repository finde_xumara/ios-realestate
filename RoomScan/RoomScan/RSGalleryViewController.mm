//
//  ViewController.m
//  RoomScan
//
//  Created by Finde Xumara on 06/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

// UI Component
#import "RSGalleryViewCell.h"

// view Controllers
#import "RSGalleryViewController.h"
#import "RSEnvironmentViewController.h"

// view Model
#import "RSGalleryViewModel.h"
#import "RSEnvironmentViewModel.h"

// Services
#import "EnvironmentMTL.h"

@interface RSGalleryViewController ()
@property bool isEditMode;
@property NSIndexPath * selectedIndexPath;
@end

@implementation RSGalleryViewController

#pragma mark - UIViewController Overrides

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    _isEditMode = FALSE;
    _selectedIndexPath = nil;
    
    @weakify(self);
    [self.viewModel.updatedContentSignal subscribeNext:^(id x) {
        @strongify(self);
        [self.collectionView reloadData];
    }];
    
    [self configureNavigationButtons];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    // flip all card to the default
    for (NSInteger cardID = 0; cardID < [self.collectionView numberOfItemsInSection:0]; cardID++) {
        
        NSIndexPath *path = [NSIndexPath indexPathForRow:cardID inSection:0];

        RSGalleryViewCell *cell = (RSGalleryViewCell *)[_galleryView cellForItemAtIndexPath: path];

        if (!cell.isFront){
            [self flipCard:cell];
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collection View

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.viewModel numberOfSectionsInCollectionView:collectionView];
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.viewModel collectionView:collectionView numberOfItemsInSection:section];
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    RSGalleryViewCell * cell =(RSGalleryViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier: @"Cell"
                                                                                             forIndexPath: indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
    
}

- (void) flipCard:(RSGalleryViewCell *)cell {
    CGFloat flipDuration = 0.7;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, flipDuration * 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [cell flip];
    });
    
    NSUInteger options;
    if (cell.isFront) {
        options = UIViewAnimationOptionTransitionFlipFromRight|UIViewAnimationOptionAllowAnimatedContent;
    } else {
        options = UIViewAnimationOptionTransitionFlipFromLeft|UIViewAnimationOptionAllowAnimatedContent;
    }
    
    [UIView transitionWithView:cell.frontCard
                      duration:flipDuration
                       options:options
                    animations:^{}
                    completion:^(BOOL finished) {}
     ];
    
    [UIView transitionWithView:cell.backCard
                      duration:flipDuration
                       options:options
                    animations:^{}
                    completion:^(BOOL finished) {}
     ];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    RSGalleryViewCell *cell = (RSGalleryViewCell *)[collectionView cellForItemAtIndexPath: indexPath];
    
    if (!_isEditMode) {
        [self flipCard:cell];
    } else {
        cell.isSelected = !cell.isSelected;
        [self configureCell:cell atIndexPath:indexPath];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    _selectedIndexPath = indexPath;
}

-(BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    //    if ( action.type? name? == "willMoveToIndexPath"  && sender == cellToNotMove?? )  return NO;
    
    return YES;
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    [super setEditing:editing animated:animated];
    
    if(editing) {
        //Do something for edit mode
        self.isEditMode = TRUE;
        
        NSArray *allCell = [self.collectionView visibleCells];
        for (RSGalleryViewCell *cell in allCell) {
            cell.isSelected = FALSE;
        }
    }
    
    else {
        //Do something for non-edit mode
        self.isEditMode = FALSE;
    }
    
    [self.collectionView reloadData];
    [self configureNavigationButtons];
}

#pragma mark - IBActions
- (IBAction)addButtonPressed:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Create environment"
                                                                             message:@"add new environment"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *CreateEnvironmentTextField)
     {
         CreateEnvironmentTextField.placeholder = @"Please enter the environment name";
     }];
    
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // nothing...
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Create" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self.viewModel addObject:alertController.textFields.firstObject.text];
        
    }]];
    
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

- (IBAction)deleteButtonPressed:(id)sender {
    
    NSArray *allCell = [self.collectionView visibleCells];
    NSMutableArray *toBeDeletedIndex = [[NSMutableArray alloc] init];
    
    for (RSGalleryViewCell *cell in allCell) {
        if (cell.isSelected) {
            
            NSIndexPath * indexPath = [self.collectionView indexPathForCell:cell];
            [toBeDeletedIndex addObject:[NSNumber numberWithInteger:indexPath.item]];
        }
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirmation"
                                                                             message:[NSString stringWithFormat:@"Delete %d environment(s)?", (int) toBeDeletedIndex.count]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // nothing...
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // re-arrange because the index will be shifted after deletion
        NSSortDescriptor *lowestToHighest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
        [toBeDeletedIndex sortUsingDescriptors:[NSArray arrayWithObject:lowestToHighest]];
        
        // delete
        int decay = 0;
        for (NSNumber * idx in toBeDeletedIndex) {
            NSIndexPath * correctedIP = [NSIndexPath indexPathForItem:0 inSection:[idx integerValue] - decay];
            [self.viewModel deleteObjectAtIndexPath:correctedIP];
            decay++;
        }
        
        [self setEditing:FALSE];
        
    }]];
    
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

- (IBAction)uploadButtonPressed:(id)sender {
    
    UIButton *button = (UIButton *) sender;
    RSGalleryViewCell *cell = (RSGalleryViewCell *)[self superviewWithClassName:@"RSGalleryViewCell"
                                                                       fromView:button];
    
    if (cell)
    {
        [cell.uploadButton setEnabled:FALSE];
        [cell.uploadButton setTitle:@"Preparing to upload" forState:UIControlStateNormal];
        
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        RSEnvironmentViewModel * localEnvObj = [self.viewModel environmentModelForIndexPath:indexPath];
        
        RACSignal *syncEnv = [RACSignal return:localEnvObj];
        
        // 0. Login
        syncEnv = [syncEnv flattenMap:^RACStream *(RSEnvironmentViewModel * _envObj) {
            return [RVMViewModel.apiManager.environmentService login: _envObj];
        }];
        
        // 1. Fetch Environment obj from server if exist
        syncEnv = [syncEnv flattenMap:^RACStream *(RSEnvironmentViewModel * _envObj) {
            return [RVMViewModel.apiManager.environmentService fetchEnvironment:_envObj];
        }];
        
        // 2. request server to create new environment if not exists
        syncEnv = [syncEnv flattenMap:^RACStream *(RSEnvironmentViewModel * _envObj) {
            return [RVMViewModel.apiManager.environmentService createNewEnvironmentWithViewModel:_envObj];
        }];
        
        // 3. for each scan in environments, create -> send file
        for (int idx=0, count=(int) localEnvObj.numberOfScans; idx<count; idx++){
            
            // 3.1. create
            syncEnv = [syncEnv flattenMap:^RACStream *(RSScanViewModel * _scanObj) {
                NSLog(@"%d", idx);
                RSScanViewModel * _localscanObj =[localEnvObj scanAtIndex: idx];
                return [RVMViewModel.apiManager.environmentService createNewScanWithViewModel:_localscanObj];
            }];
            
            syncEnv = [syncEnv flattenMap:^RACStream *(RSScanViewModel * _scanObj) {
                NSLog(@"ss-%d", idx);
                NSString *message = [NSString stringWithFormat:@"Uploading %d/%d", idx+1, count];
                [cell.uploadButton setTitle:message forState:UIControlStateNormal];
                return [RVMViewModel.apiManager.environmentService uploadScanDataWithViewModel:[localEnvObj scanAtIndex: idx]];
            }];
        }
        
        syncEnv = [syncEnv flattenMap:^RACStream *(RSScanViewModel * _scanObj) {
            return [RACSignal return:@YES];
        }];
        
        [syncEnv
         subscribeNext:^(id val){
             NSLog(@"-subscribeNext: %@", val);
             [cell.uploadButton setTitle:@"Upload success" forState:UIControlStateNormal];
             [cell.uploadButton setEnabled:TRUE];
        }
         error:^(NSError *err){
             NSLog(@"error");
         }
         completed:^{
             
         }];
        
    }
    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"editEnvironment"]) {
        
        RSEnvironmentViewController *viewController = (RSEnvironmentViewController *)segue.destinationViewController;
        RSEnvironmentViewModel * comm = [self.viewModel environmentModelForIndexPath:_selectedIndexPath];
        viewController.viewModel = comm;
    }
    
    else if ([[segue identifier] isEqualToString:@"editEnvironmentFromButton"]) {
        
        UIButton *button = (UIButton *) sender;
        UICollectionViewCell *cell = (UICollectionViewCell *)[self superviewWithClassName:@"RSGalleryViewCell"
                                                                                 fromView:button];
        if (cell)
        {
            NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
            RSEnvironmentViewController *viewController = (RSEnvironmentViewController *)segue.destinationViewController;
            RSEnvironmentViewModel * comm = [self.viewModel environmentModelForIndexPath:indexPath];
            viewController.viewModel = comm;
        }
        
    }
    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return (self.editing == NO);
}

#pragma mark - Private Methods

- (UIView *)superviewWithClassName:(NSString *)className fromView:(UIView *)view
{
    while (view)
    {
        if ([NSStringFromClass([view class]) isEqualToString:className])
        {
            return view;
        }
        view = view.superview;
    }
    return nil;
}

- (void)configureCell:(RSGalleryViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.titleLabel.text = [self.viewModel titleAtIndexPath:indexPath];
    cell.detailLabel.text = [self.viewModel subtitleAtIndexPath:indexPath];
    
    cell.frontCard.alpha = _isEditMode ? 0.5 : 1.0;
    cell.highlightLayer.hidden = !_isEditMode;
    cell.highlightLayer.backgroundColor = (_isEditMode && cell.isSelected) ? [UIColor colorWithHexString:@"9E4B10"] : nil;
}

- (void) configureNavigationButtons {
    if (!_isEditMode) {
        [[self.navigationItem.rightBarButtonItems objectAtIndex:0] setEnabled:FALSE];
        [[self.navigationItem.rightBarButtonItems objectAtIndex:1] setEnabled:TRUE];
    } else {
        [[self.navigationItem.rightBarButtonItems objectAtIndex:0] setEnabled:TRUE];
        [[self.navigationItem.rightBarButtonItems objectAtIndex:1] setEnabled:FALSE];
    }
    
}


@end
