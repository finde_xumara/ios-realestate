//
//  PCGLViewController_Annotation.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 19/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

//#import "PCGLViewController.h"

@class RSEnvironmentViewModel, RSEnvironmentViewController, RSAnnotationViewModel, RSPointViewModel;

@interface PCGLViewController (Annotation)

-(void)setupAnnotationView;
-(void)selectAnnotationGestureRecognizer:(UITapGestureRecognizer *) sender;

-(void) handleEditMarkerMode:(CGPoint) location sender:(id) sender;
-(void) handleAddMarkerMode:(CGPoint) location state:(UIGestureRecognizerState) state;

@end
