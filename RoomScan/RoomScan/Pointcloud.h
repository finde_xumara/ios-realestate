//
//  Pointcloud.h
//  RoomScan
//
//  Created by Finde Xumara on 17/05/16.
//  Copyright © 2016 Occipital. All rights reserved.
//

#ifndef Pointcloud_h
#define Pointcloud_h

#import <GLKit/GLKit.h>

@interface Refinement : NSObject {
    
}
-(GLKMatrix4) transformMatrix;
@property (nonatomic) float rotation;
@property (nonatomic) GLKVector2 translation;
@property (nonatomic) GLKMatrix4 transformMat;

@end;

@class RSScanViewModel;
@interface Pointcloud: NSObject
@property (nonatomic) NSString* scanID;
@property (nonatomic) int first;
@property (nonatomic) int count;
@property (nonatomic, strong) Refinement* refinement;
@property (nonatomic, strong) RSScanViewModel *scanModel;

@end

#endif /* Pointcloud_h */
