//
//  RSEditEnvironmentViewController.h
//  RoomScan
//
//  Created by Finde Xumara on 07/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "PCGLViewController.h"

@class RSEnvironmentViewModel, PCGLViewController, LeftTableViewController;

@interface RSEnvironmentViewController : UIViewController

@property (nonatomic, strong) RSEnvironmentViewModel *viewModel;

@property (nonatomic, strong) PCGLViewController *pointcloudViewController;
@property (nonatomic, strong) LeftTableViewController *selectionTableViewController;

- (void) selectScanAtIndex:(int) index;
- (void) reloadData:(NSInteger) index;
- (void) refreshDrawing;
- (void) setAnnotationView:(BOOL) isShouldShow;

@end
