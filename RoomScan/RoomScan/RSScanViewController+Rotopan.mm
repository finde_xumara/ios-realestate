//
//  RSScanViewController+Rotopan.m
//  RoomScan
//
//  Created by Finde Xumara on 14/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//
#import "RSScanViewController.h"
#import "RSScanViewController+Rotopan.h"
#import "RSScanViewController+StructureSensor.h"

#import "RSScanViewModel.h"

@implementation RSScanViewController (BLE)

NSTimer *rssiTimer;

- (void) readRSSITimer:(NSTimer *)timer {
    [_bleShield readRSSI];
}

- (void) connectionTimer:(NSTimer *)timer {
    if(_bleShield.peripherals.count > 0)
    {
        [_bleShield connectPeripheral:[_bleShield.peripherals objectAtIndex:0]];
    }
    else
    {
        [self.rotopanStatusView setState:Disconnected];
    }
}

- (void) BLEDisconnect {
    if (_bleShield.activePeripheral)
        if(_bleShield.activePeripheral.state == CBPeripheralStateConnected)
        {
            [[_bleShield CM] cancelPeripheralConnection:[_bleShield activePeripheral]];
        }
    
}

- (IBAction) BLEShieldScan:(id)sender {
    
    if (self.rotopanStatusView.state == Connecting) {
        return;
    }
    
    // if it connected, then disconnect
    else if (self.rotopanStatusView.state == Connected) {
        [self BLEDisconnect];
        return;
    }
    
    else if (self.rotopanStatusView.state == Disconnected) {
        if (_bleShield.peripherals)
            _bleShield.peripherals = nil;
        
        [_bleShield findBLEPeripherals:3];
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(connectionTimer:) userInfo:nil repeats:NO];
        [self.rotopanStatusView setState:Connecting];
    }
}
@end

@implementation RSScanViewController (Rotopan)

- (void) setupRotopan {
    _bleShield = [[BLE alloc] init];
    [_bleShield controlSetup];
    _bleShield.delegate = self;
    
    [self.rotopanStatusView setState:Disconnected];
    self->_scanTask = dispatch_group_create();
}

- (void) rotopanStartScan {
    
    if (_rotopan.deviceStatus == RotopanStatusReady){
        
        self.viewModel.numberOfCapturedData = 0;
        _rotopan.waiting_unit = 0;
        _rotopan.counter = 0;
        
        _rotopan.stepRotopan = MIN_ROTATION;
        _rotopan.tiltRotopan = MIN_TILT;
        _rotopan.deviceStatus = RotopanStatusScanning;
        _rotopan.deviceTask = RotopanTaskTilt;
        _rotopan.deviceDirection = RotopanDirectionUp;
        
        // trigger
        // start with tilt down
        [self bleReport];
        
    }
}

- (void) rotopanStopScan {
    // TODO::set flag to stop scan
    
    // disconnect the rotopan
    [self BLEDisconnect];
}

- (void) rotopanReport {
    [self bleReport];
}

- (void) rotopanReset {
    // set flags
    _rotopan.deviceStatus = RotopanStatusCalibrating;
    _rotopan.deviceTask = RotopanTaskNone;
    
    // trigger the rotopan
    [self bleReport];
}

- (void)reset_pan {
    
    NSData *d;
    NSString *combined = [NSString stringWithFormat:@"cs%@x", @"0"];
    d = [combined dataUsingEncoding:NSUTF8StringEncoding];
    
    if (_bleShield.activePeripheral.state == CBPeripheralStateConnected) {
        [_bleShield write:d];
    }
    
}

- (void) bleRotateTo: (int) degrees {
    //    36000
    [self bleRotateToStep:(int) (MAX_STEP*degrees*100/36000)];
}

- (void) bleRotateToStep: (int) steps {
    NSData *d;
    NSString *combined = [NSString stringWithFormat:@"cp%dx", (int) steps];
    d = [combined dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",combined);
    
    _rotopan.isTaskConfirmed = NO;
    if (_bleShield.activePeripheral.state == CBPeripheralStateConnected) {
        [_bleShield write:d];
    }
}

- (void) bleTiltTo: (int) degrees {
    degrees = -degrees;
    
    int _zero_position = ZERO_TILT;
    int _degrees = _zero_position; // 0 location
    
    _degrees = ((int) floor(2000*degrees/45)) + _zero_position;
    
    NSData *d;
    NSString *combined = [NSString stringWithFormat:@"ct%dx", _degrees];
    d = [combined dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",combined);
    
    _rotopan.isTaskConfirmed = NO;
    if (_bleShield.activePeripheral.state == CBPeripheralStateConnected) {
        [_bleShield write:d];
    }
}

- (void) bleReport {
    NSData *d;
    NSString *combined = [NSString stringWithFormat:@"cg%@x",@"0"];
    d = [combined dataUsingEncoding:NSUTF8StringEncoding];
    
    if (_bleShield.activePeripheral.state == CBPeripheralStateConnected) {
        [_bleShield write:d];
    }
}

- (void) bleDidConnect {
    [self.rotopanStatusView setState:Connected];
    _rotopan.deviceStatus = RotopanStatusReady;
    [NSTimer    scheduledTimerWithTimeInterval:1.0    target:self    selector:@selector(rotopanReset)    userInfo:nil repeats:NO];
}

- (void) bleDidDisconnect {
    [self setUIScanInit];
    
    [self.rotopanStatusView setState:Disconnected];
    _rotopan.deviceStatus = RotopanStatusDisconnected;
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

#pragma mark - Scan Process

- (void) moveToNextTilt {
    
    if (TILT_STEP == 0) {
        _rotopan.isAngleDone = YES;
        [self rotopanReport];
    }
    else {
        if (_rotopan.tiltRotopan == MAX_TILT) {
            
            if (_rotopan.deviceDirection == RotopanDirectionUp) {
                _rotopan.deviceDirection = RotopanDirectionDown;
                _rotopan.isAngleDone = YES;
            } else {
                _rotopan.tiltRotopan -= TILT_STEP;
                _rotopan.deviceTask = RotopanTaskTilt;
                self.viewModel.numberOfCapturedData = 0;
            }
            
            [self rotopanReport];
            
        } else if (_rotopan.tiltRotopan == MIN_TILT) {
            
            if (_rotopan.deviceDirection == RotopanDirectionDown) {
                _rotopan.deviceDirection = RotopanDirectionUp;
                _rotopan.isAngleDone = YES;
            } else {
                _rotopan.tiltRotopan += TILT_STEP;
                _rotopan.deviceTask = RotopanTaskTilt;
                self.viewModel.numberOfCapturedData = 0;
            }
            
            [self rotopanReport];
        } else {
            if (_rotopan.deviceDirection == RotopanDirectionUp) {
                _rotopan.tiltRotopan += TILT_STEP;
            } else {
                _rotopan.tiltRotopan -= TILT_STEP;
            }
            
            _rotopan.deviceTask = RotopanTaskTilt;
            self.viewModel.numberOfCapturedData = 0;
            
            
            [self rotopanReport];
        }
    }
}

- (void) scanNow {
    NSLog(@"scanning.. ");
    
    if (self.viewModel.numberOfCapturedData < kDataPerAngle) {

        if (self.viewModel.numberOfCapturedData > 0){
            sleep(0.5);
        }

        NSLog(@"waiting for depth sensor to scan enough data = %d of %d", self.viewModel.numberOfCapturedData, kDataPerAngle);
        self.viewModel.isWaitingForDepthSensor = YES;

        // simulate without camera
//        if (_appStatus.sensorStatus == AppStatus::SensorStatusNeedsUserToConnect){
//            NSLog(@"Snap Picture:: %d-%d-%d ::", _rotopan.panValue, _rotopan.panAbsoluteValue, _rotopan.tiltValue);
//            
//            self.viewModel.isWaitingForDepthSensor = NO;
//            self.viewModel.numberOfCapturedData++;
//            
//            [self bleReport];
//        }

    } else {

        if (!self.viewModel.isWaitingForDepthSensor) {
            // == three rotation sequence
            if (_rotopan.counter >= COUNTER){

                // TODO::stop
                [self reset_pan];

            } else {
                
                if (_rotopan.stepRotopan >= (_rotopan.counter+1)*360/ROTATE_STEP) {

                    _rotopan.counter++;
                    [self moveToNextTilt];

                } else {
                    _rotopan.stepRotopan++;
                    self.viewModel.numberOfCapturedData = 0;
                    [self bleRotateTo:(_rotopan.stepRotopan)*ROTATE_STEP];
                }

            }


        } else {
            
            // wait a bit and re-run current task
            [NSTimer scheduledTimerWithTimeInterval:(float)1 target:self selector:@selector(bleReport) userInfo:nil repeats:NO];
            
        }
        
    }
}

- (void) updateUIafterDone {
//    [self.viewModel willDismiss];
//    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void) scanIsDone {
    
    [NSTimer scheduledTimerWithTimeInterval:(float)1.5
                                     target:self
                                   selector:@selector(enterFinalizingState)
                                   userInfo:nil
                                    repeats:NO];
}

#pragma mark - Multihander

- (void) bleDidReceiveData:(unsigned char *)data length:(int)length {
    
    NSData *d = [NSData dataWithBytes:data length:length];
    NSString *s = [[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];
    NSLog(@"%@", s);
    
    float _panStep, _panAbsolute, _tiltAbsolute;
    
    // k<_pan_steps>;<_abs_pan>;<_abs_tilt>
    NSArray *s_ = [s componentsSeparatedByString: @";"];
    _panStep =      [[[s_ objectAtIndex: 0] substringFromIndex:1] intValue];
    _panAbsolute =  [[s_ objectAtIndex: 1] intValue];
    _tiltAbsolute = [[s_ objectAtIndex: 2] intValue];
    
    _rotopan.tiltValue = _tiltAbsolute;
    _rotopan.panValue =  _panStep;
    _rotopan.panAbsoluteValue = _panAbsolute;
    
    // if Calibrating then check step.
    switch (_rotopan.deviceStatus) {
            
        case RotopanStatusCalibrating :
            switch (_rotopan.deviceTask){
                case RotopanTaskNone:
                    [self reset_pan];
                    _rotopan.deviceTask = RotopanTaskRotate;
                    break;
                    
                case RotopanTaskRotate:
                    [self bleTiltTo:0];
                    _rotopan.deviceTask = RotopanTaskTilt;
                    break;
                    
                default:
                    _rotopan.deviceTask = RotopanTaskNone;
                    _rotopan.deviceStatus = RotopanStatusReady;
                    
                    [self setUIScanReady];
                    break;
            }
            break;
            
        case RotopanStatusScanning:
            if (!_rotopan.isTaskConfirmed) {
                
                NSLog(@"check for bleUpdate with delay 0.25s");
                // WAITING BEFORE TAKING VALUE
                
                _rotopan.isTaskConfirmed = YES;
                [NSTimer scheduledTimerWithTimeInterval:(float)SCAN_DELAY target:self selector:@selector(bleReport) userInfo:nil repeats:NO];
                
            } else {
                
                switch (_rotopan.deviceTask){
                        
                        // initialize scanning for Rotopan
                    case RotopanTaskNone:
                        _rotopan.deviceTask = RotopanTaskRotate;
                        [self bleReport];
                        break;
                        
                    case RotopanTaskRotate:
                        if (_rotopan.counter<=COUNTER-1){
                            // WAITING BEFORE MOVE TO NEXT SCAN
                            
                            [NSTimer scheduledTimerWithTimeInterval:(float)0.05 target:self selector:@selector(scanNow) userInfo:nil repeats:NO];
                        } else {
                            _rotopan.isTaskConfirmed = YES;
                            [self scanIsDone];
                        }
                        break;
                        
                    case RotopanTaskTilt:
                        _rotopan.deviceTask = RotopanTaskRotate;
                        [self bleTiltTo:_rotopan.tiltRotopan];
                        break;
                        
                    default:
                        _rotopan.isTaskConfirmed = YES;
                        _rotopan.deviceTask = RotopanTaskNone;
                        break;
                }
                
            }
            break;
            
        default:
            
            // set back to initial
            _rotopan.deviceStatus = RotopanStatusReady;
            
            break;
    }
    
}

@end