//
//  RSAnnotationViewModel.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 23/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#ifndef RSAnnotationViewModel_h
#define RSAnnotationViewModel_h

#import <Structure/Structure.h>
#import "RVMViewModel.h"
#import "RVMViewModel+Edits.h"

@class RSAnnotation, RSPointViewModel;

@interface RSAnnotationViewModel : RVMViewModel {
    
}

@property (nonatomic, readonly) RSAnnotation *model;

@property (nonatomic, strong) NSString *annotationId;
@property (nonatomic, strong) NSString *annotationName;
@property (nonatomic) BOOL isSelected;
@property (nonatomic, strong) NSNumber *isTopBottomSplitted;

- (instancetype)initWithModel:(id)model;
- (RSPointViewModel *)pointAtIndex:(NSInteger)index;
- (NSMutableArray *)getLines;

- (void)setFirstLineEndPointTo:(CGPoint)location;
- (RSPointViewModel *)closestPointToLocation:(GLKVector2)vectLocation InRadius:(float)pointSize isEdgeOnly:(BOOL)onlyEdge;

- (double) topBottomLength;
- (void) setTopBottomDistance:(double) distance;

@end


#endif /* RSAnnotationViewModel_h */
