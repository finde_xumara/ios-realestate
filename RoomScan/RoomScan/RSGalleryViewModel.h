//
//  RVMViewModel+RSGalleryViewModel.h
//  RoomScan
//
//  Created by Finde Xumara on 06/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RVMViewModel.h"
#import "RVMViewModel+Edits.h"

@class RSEnvironmentViewModel;

@interface RSGalleryViewModel : RVMViewModel

-(instancetype)initWithModel:(id)model;

@property (nonatomic, readonly) RACSignal *updatedContentSignal;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *model;

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView;
- (NSInteger) collectionView: (UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
- (NSString *)titleAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *)subtitleAtIndexPath:(NSIndexPath *)indexPath;

- (RSEnvironmentViewModel *)environmentModelForIndexPath:(NSIndexPath *)indexPath;

- (void) addObject:(NSString *)environmentName;
- (void) deleteObjectAtIndexPath:(NSIndexPath *)indexPath;


@end
