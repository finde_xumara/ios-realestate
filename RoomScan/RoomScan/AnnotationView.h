//
//  LineView.h
//  RoomScan
//
//  Created by Finde Xumara on 15/05/16.
//  Copyright © 2016 Occipital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Pointcloud.h"

// View Mode
typedef enum
{
    DEFAULT_MODE,
    ADD_MARKER_MODE,
    EDIT_MARKER_MODE,
    ADD_ANCHOR_MODE,
    EDIT_ANCHOR_MODE
} ViewMode;

@class RSEnvironmentViewModel, RSAnnotationViewModel, RSPointViewModel;
@interface AnnotationView : UIView
@property (nonatomic) NSMutableArray *markers;
@property (nonatomic, strong) RSEnvironmentViewModel *envViewModel;

@property (nonatomic) float base_scale;

@property (nonatomic) float scale;
@property (nonatomic) float height;
@property (nonatomic) float width;
@property (nonatomic) CGPoint centerAnnotation;
@property (nonatomic) ViewMode viewMode;
@property (nonatomic) NSString *selectedScanID;

- (NSDictionary *) getSettings;

// update scale
- (void) prepareToScale;
- (void) updateScaleTo:(float) scale;
- (void) finalizeScaling;

- (void) updateCenterTo: (CGPoint) location;
@end