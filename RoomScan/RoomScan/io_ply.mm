//
//  io_ply.m
//  RoomScan
//
//  Created by Finde Xumara on 10/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "io_ply.h"
#import "CCHBinaryDataReader.h"

@implementation ply_element

-(void) setX:(float)X
           Y:(float)Y
           Z:(float)Z
           R:(unsigned char)R
           G:(unsigned char)G
           B:(unsigned char)B {
    
    _x = X;
    _y = Y;
    _z = Z;
    _r = R;
    _g = G;
    _b = B;
}

@end

@implementation io_ply

- (bool) read_ply:(NSString *)fileAtPath
       withTarget:(NSMutableArray *)arrayData {
   
    NSData *binData = [NSData dataWithContentsOfFile:fileAtPath];
    
    // file is not exists? return NO.
    if (binData == nil){
        return NO;
    }

    int line_index = 0;
    CCHBinaryDataReader *reader = [[CCHBinaryDataReader alloc] initWithData:binData options:LITTLE_ENDIAN];
    
    int max_read = 512;
    int n_points = 0;
    while ([reader canReadNumberOfBytes:max_read]) {
        NSString *tryLine = [reader readStringWithNumberOfBytes:max_read encoding:NSUTF8StringEncoding];
        NSArray *vals = [tryLine componentsSeparatedByString:@"\n"];
        NSString * firstLine = @"";
        
        if ([vals count]>1){
            firstLine = [vals objectAtIndex:0];
            NSArray *testFirstLine = [firstLine componentsSeparatedByString:@"element vertex "];
            
            if ([testFirstLine count] == 2) {
                n_points = [[[[testFirstLine objectAtIndex:1] componentsSeparatedByString:@"\n"] objectAtIndex:0] intValue];
            }
            
            line_index+=[firstLine length] + 1;
            [reader setNumberOfBytesRead: line_index];
        }
        
//        NSLog(@"[%d] %d - %@ = %@", max_read, n_points, firstLine, vals);
        
        if (tryLine == nil) {
            while ([reader readStringWithNumberOfBytes:max_read encoding:NSUTF8StringEncoding] == nil){
                max_read--;
                [reader setNumberOfBytesRead: line_index];
            }
            
            [reader setNumberOfBytesRead: line_index];
        }
        
        // TODO: validate ply file
        
        if (max_read <= 1) {
            break;
        }
    }
    
    [reader setNumberOfBytesRead: line_index];
    
    for (int i = 0; i<n_points; i++) {
        float x = [reader readFloat];
        float y = [reader readFloat];
        float z = [reader readFloat];
        
        unsigned char r = [reader readChar];
        unsigned char g = [reader readChar];
        unsigned char b = [reader readChar];
        
        ply_element * p = [[ply_element alloc] init];
        [p setX:x Y:y Z:z R:r G:g B:b];
        
        [arrayData addObject: p];
    }
    
    return YES;
}

- (bool) write_ply:(NSString *)fileAtPath
         withInput:(NSArray *)arrayData {
    
    NSMutableString *string;
    string = [[NSMutableString alloc] init];
    
    // create file
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    // create header
    [string appendFormat: @"ply\n"];
    [string appendFormat: @"format binary_little_endian 1.0\n"];
    [string appendFormat: @"comment 3DUniversum generated\n"];
    [string appendFormat: @"element vertex %d\n", (int) [arrayData count]];
    [string appendFormat: @"property float x\n"];
    [string appendFormat: @"property float y\n"];
    [string appendFormat: @"property float z\n"];
    [string appendFormat: @"property uchar red\n"];
    [string appendFormat: @"property uchar green\n"];
    [string appendFormat: @"property uchar blue\n"];
    [string appendFormat: @"end_header\n"];
    [[string dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:YES];
    
    NSFileHandle * outputFileHandle  = [NSFileHandle fileHandleForWritingAtPath: fileAtPath];

    for (ply_element * elem in arrayData) {
        [outputFileHandle seekToEndOfFile];
        NSMutableData * data = [[NSMutableData alloc] init];

        NSSwappedFloat x = NSSwapHostFloatToLittle(elem.x);
        NSSwappedFloat y = NSSwapHostFloatToLittle(elem.y);
        NSSwappedFloat z = NSSwapHostFloatToLittle(elem.z);
        
        [data appendData:[NSData dataWithBytes:&x length:sizeof(NSSwappedFloat)]];
        [data appendData:[NSData dataWithBytes:&y length:sizeof(NSSwappedFloat)]];
        [data appendData:[NSData dataWithBytes:&z length:sizeof(NSSwappedFloat)]];
        
        unsigned char r = elem.r;
        unsigned char g = elem.g;
        unsigned char b = elem.b;
        
        [data appendData:[NSData dataWithBytes:&r length:sizeof(unsigned char)]];
        [data appendData:[NSData dataWithBytes:&g length:sizeof(unsigned char)]];
        [data appendData:[NSData dataWithBytes:&b length:sizeof(unsigned char)]];
        
        [outputFileHandle writeData:data];
    }
    
    // close file
    [outputFileHandle closeFile];
    
    return TRUE;
}

@end
