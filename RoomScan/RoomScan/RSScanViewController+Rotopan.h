//
//  RSScanViewController+Rotopan.h
//  RoomScan
//
//  Created by Finde Xumara on 14/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RSScanViewController.h"

@interface RSScanViewController (Rotopan) {
}

- (void) setupRotopan;
//- (void) readRSSITimer:(NSTimer *)timer;
//- (void) connectionTimer:(NSTimer *)timer;

- (IBAction) BLEShieldScan:(id)sender;

//- (void) bleDidReceiveData:(unsigned char *)data length:(int)length;
//- (void) bleDidDisconnect;
//- (void) bleRotateTo: (int) degrees;
//- (void) bleTiltTo: (int) degrees;

- (void) rotopanReport;
- (void) rotopanReset;
- (void) rotopanStartScan;
- (void) rotopanStopScan;

- (void) scanNow;
//- (void) bleDidConnect;

@end
