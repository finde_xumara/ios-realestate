//
//  RSEnvironment.m
//  RoomScan
//
//  Created by Finde Xumara on 06/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RSEnvironment.h"
#import "FileHelper.h"

@implementation RSEnvironment

@dynamic environmentId;
@dynamic serverId;
@dynamic environmentName;
@dynamic scans;
@dynamic annotations;
@dynamic createdAt;
@dynamic modifiedAt;

-(void) prepareSetting {
    
    // create folder if not exist
    NSURL * envPath = [[[FileHelper applicationDocumentsDirectory] URLByAppendingPathComponent:rootEnvironment] URLByAppendingPathComponent:self.environmentId];
    
    [[NSFileManager defaultManager] createDirectoryAtPath: [envPath path] withIntermediateDirectories: YES attributes:nil error: nil];
    
    //TODO create basic files
}

-(void) prepareForDeletion {
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtURL:[self getPath] error:&error];
    
    [super prepareForDeletion];
}

- (NSURL *) getPath {
    return[[[FileHelper applicationDocumentsDirectory] URLByAppendingPathComponent:rootEnvironment] URLByAppendingPathComponent:self.environmentId];
}

- (void) saveToContext:(NSManagedObjectContext *)context {
    [context insertObject:self];
    
    NSError *error = nil;
    if ([context save:&error] == NO) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    [self prepareSetting];
}

@end
