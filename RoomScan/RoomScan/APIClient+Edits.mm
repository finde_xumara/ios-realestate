//
//  APIClient+Edits.m
//  ServerCommunication
//
//  Created by Daniel Hallin on 18/05/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "APIClient+Edits.h"
#import <AFNetworking/AFURLRequestSerialization.h>

@implementation APIClient (Edits)



- (NSURLSessionDataTask *)POST:(NSString *)URLString
                 URIparameters:(id)parameters
     constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                      progress:(nullable void (^)(NSProgress * _Nonnull))uploadProgress
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *queryString = AFQueryStringFromParameters(parameters);
    BOOL urlHasQuery = !![NSURL URLWithString: URLString].query;
    URLString = [URLString stringByAppendingFormat: urlHasQuery ? @"&%@" : @"?%@", queryString];
    
    return [super POST: URLString parameters: nil constructingBodyWithBlock: block
              progress: uploadProgress
               success: success
               failure: failure];
}

@end
