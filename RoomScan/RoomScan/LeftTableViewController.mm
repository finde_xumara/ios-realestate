//
//  LeftTableViewController.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 17/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//
#import "RSEnvironmentViewModel.h"
#import "RSEnvironmentViewController.h"
#import "LeftTableViewController.h"

@interface LeftTableViewController ()
    @property (nonatomic) int selectedIndex;
@end

@implementation LeftTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    _selectedCategory = @"Scans";
    
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    _selectedIndex = -1;
    [self.tableView reloadData];
}

-(IBAction)switchedListCategory:(UISegmentedControl *)segment {
    _selectedCategory = [segment titleForSegmentAtIndex:segment.selectedSegmentIndex];
    
    if ([_selectedCategory isEqualToString: @"Annotations"]) {
        [((RSEnvironmentViewController *) self.parentViewController.parentViewController) setAnnotationView:YES];
    } else {
        [((RSEnvironmentViewController *) self.parentViewController.parentViewController) setAnnotationView:NO];
    }
    
    [self.tableView reloadData];
}


#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(void) configureCell:(UITableViewCell *)cell atIndexPath: (NSIndexPath *) indexPath {
    
    if ([_selectedCategory isEqualToString: @"Scans"]) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Show all scans";
        }
        else {
            cell.textLabel.text = [self.viewModel scanTitleAtIndexPath:indexPath.row -1];
        }
//    } else if ([_selectedCategory isEqualToString: @"Annotations"]) {
    } else {
        cell.textLabel.text = [self.viewModel annotationTitleAtIndexPath:indexPath.row];
        
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([_selectedCategory isEqualToString: @"Scans"]) {
        return [_viewModel numberOfScans] + 1;
//    } else if ([_selectedCategory isEqualToString: @"Annotations"]) {
    } else {
        return [_viewModel numberOfAnnotations];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ScanCell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([_selectedCategory isEqualToString: @"Scans"]) {
    
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_selectedCategory isEqualToString: @"Scans"]) {
        
        UITableViewCell *prevCell = [tableView.visibleCells objectAtIndex:_selectedIndex + 1];
        [prevCell setAccessoryType:UITableViewCellAccessoryNone];
        prevCell.contentView.backgroundColor = [UIColor colorWithHexString:@"FFFFFF"];
        prevCell.backgroundColor = [UIColor colorWithHexString:@"FFFFFF"];
        
        if (_selectedIndex != indexPath.row - 1) {
            
            _selectedIndex = indexPath.row - 1;
            [((RSEnvironmentViewController *) self.parentViewController.parentViewController) selectScanAtIndex:indexPath.row - 1];
            UITableViewCell *cell = [tableView.visibleCells objectAtIndex:indexPath.row];
            
            if (_selectedIndex >= 0) {
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                cell.contentView.backgroundColor = [UIColor colorWithHexString:@"FFDD76"];
                cell.backgroundColor = [UIColor colorWithHexString:@"FFDD76"];
            } else {
                cell.contentView.backgroundColor = [UIColor colorWithHexString:@"FFFFFF"];
                cell.backgroundColor = [UIColor colorWithHexString:@"FFFFFF"];
            }
        } else if (indexPath.row != 0) {
            
            [self performSegueWithIdentifier:@"editAnchors" sender:nil];
            
        }
    }
    
    //    // load the pointcloud
    //    RSScanViewModel * comm = [self.viewModel scanAtIndex:indexPath.row];
    //    [self.pointcloudViewController selectScanAtIndex:indexPath.row];
    
    //    // load the annotation
    //    if (tableView == _scanView) {
    //
    //        // set the selected pointcloud
    //        if (indexPath.row >= _pointclouds.count) {
    //            _selectedPointcloud = nil;
    //            _annotationView.selectedScanID = nil;
    //
    //            // if there is anchor points, compute the transformation
    //            std::vector<GLKVector2> midPoints;
    //            std::vector<GLKVector2> firstPoints;
    //
    //            std::vector<RSLine*> anchors;
    //
    //            // list the midpoints
    //            for (Pointcloud *pc in _pointclouds) {
    //                RSLine *anchor = [_annotationView getAnchorsOfPointcloud:pc];
    //
    //                if (anchor == nil) {
    //                    continue;
    //                }
    //
    //                NSMutableArray *anchorPoints = [anchor.controlPoints mutableCopy];
    //                RSPoint *vA = anchorPoints[0];
    //                RSPoint *vB = anchorPoints[1];
    //                GLKVector2 mid = GLKVector2Subtract(vA.position, vB.position);
    //                mid = GLKVector2Normalize(mid);
    //
    //                midPoints.push_back(mid);
    //                firstPoints.push_back(vA.position);
    //                anchors.push_back(anchor);
    //            }
    //
    //            if (anchors.size() == _pointclouds.count) {
    //                // calculate the transformation
    //                int counter = 0;
    //                for (Pointcloud *pc in _pointclouds) {
    //                    if (counter == 0) {
    //                        counter++; continue;
    //                    }
    //
    //                    float rotation = ANGLEV(midPoints[counter], midPoints[0]);
    //
    //                    if (CLOCK(midPoints[counter], midPoints[0])) {
    //                        rotation = -rotation;
    //                    }
    //
    //                    float sinR = sin(GLKMathDegreesToRadians(-rotation));
    //                    float cosR = cos(GLKMathDegreesToRadians(-rotation));
    //
    //                    // rotate the point to the first scan
    //
    //
    //                    GLKVector2 vAfterR = GLKVector2Make((cosR * firstPoints[counter].x) - (sinR * firstPoints[counter].y),
    //                                                        (sinR * firstPoints[counter].x) + (cosR * firstPoints[counter].y));
    //
    //                    GLKVector2 vAfterRt = GLKVector2Subtract(firstPoints[0], vAfterR);
    //
    //
    //                    // update the pointcloud refinement
    //                    pc.refinement.rotation = rotation;
    //                    pc.refinement.translation = GLKVector2Make(vAfterRt.x, vAfterRt.y);
    //
    //                    counter++;
    //                }
    //            }
    //
    //        } else {
    //            _selectedPointcloud = [_pointclouds objectAtIndex:indexPath.row];
    //            _annotationView.selectedScanID = _selectedPointcloud.scanID;
    //        }
    //        [_annotationView setNeedsDisplay];
    //        [_markerView reloadData];
    //
    //    } else if (tableView == _markerView){
    //        
    //        if (_selectedPointcloud == nil) {
    //            _markerType = (MarkerType) indexPath.row;
    //        } else {
    //            _markerType = MarkerType::ANCHOR;
    //        }
    //        
    //        _viewMode = ADD_MARKER_MODE;
    //        [self updateUIComponent];
    //    }
    
    
}

// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([_selectedCategory isEqualToString: @"Scans"] && indexPath.row == 0) {
        return NO;
    }
    
    // Return YES if you want the specified item to be editable.
    return YES;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *editButton = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit"
                                                                    handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Title" message:@"Message" preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        [alertController addAction:[UIAlertAction actionWithTitle:@"Button 1" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                                            [self loadGooglrDrive];
                                        }]];
                                        
                                        [alertController addAction:[UIAlertAction actionWithTitle:@"Button 2" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                                            [self loadDropBox];
                                        }]];
                                        
                                        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                                            [self closeAlertview];
                                        }]];
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^ {
                                            [self presentViewController:alertController animated:YES completion:nil];
                                        });

//                                        [alert release];
                                    }];
    
    editButton.backgroundColor = [UIColor grayColor]; //arbitrary color
    
    
    UITableViewRowAction *deleteButon = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete"
                                                                     handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                     {
                                         [self deleteButtonPressedAtIndexPath:indexPath];
                                     }];
    deleteButon.backgroundColor = [UIColor redColor]; //arbitrary color
    
    return @[deleteButon, editButton]; //array with all the buttons you want. 1,2,3, etc...
}

- (void) deleteButtonPressedAtIndexPath:(NSIndexPath *) indexPath {
    if ([_selectedCategory isEqualToString: @"Scans"]) {
        
        [self.viewModel removeScanAtIndex:indexPath.row - 1];
        
        
    } else {
        [self.viewModel removeAnnotationAtIndex:indexPath.row];
        
    }
    
    [(RSEnvironmentViewController *)[self.parentViewController parentViewController] refreshDrawing];
    [self.tableView reloadData];
}

- (void) reloadData:(NSInteger) index {
    
    self.segmentControl.selectedSegmentIndex = index;
    self.selectedCategory = [self.segmentControl titleForSegmentAtIndex:index];
    
    [self.tableView reloadData];
    [self.segmentControl setNeedsDisplay];
}

@end
