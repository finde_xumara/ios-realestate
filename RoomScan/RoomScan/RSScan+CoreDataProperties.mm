//
//  RSScan+CoreDataProperties.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 01/09/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RSScan+CoreDataProperties.h"

@implementation RSScan (CoreDataProperties)

@dynamic createdAt;
@dynamic modifiedAt;
@dynamic scanId;
@dynamic scanName;
@dynamic serverId;
@dynamic m00;
@dynamic m01;
@dynamic m02;
@dynamic m03;
@dynamic m10;
@dynamic m11;
@dynamic m12;
@dynamic m13;
@dynamic m20;
@dynamic m21;
@dynamic m22;
@dynamic m30;
@dynamic m23;
@dynamic m31;
@dynamic m32;
@dynamic m33;
@dynamic environmentObj;

@end
