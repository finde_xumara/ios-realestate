//
//  ImageHelper.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 04/11/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#ifndef ImageHelper_h
#define ImageHelper_h

#import <Foundation/Foundation.h>
#import <Structure/Structure.h>

@interface ImageHelper : NSObject

+ (UIImage *) imageFromDepthFrame: (STDepthFrame*) depthFrame;
+ (UIImage *) imageFromColorFrame: (STColorFrame*) colorFrame;
+ (UIImage*) makeUIImage:(uint8_t *)inBaseAddress
              cBCrBuffer:(uint8_t*)cbCrBuffer
              bufferInfo:(CVPlanarPixelBufferInfo_YCbCrBiPlanar *)inBufferInfo
                   width:(size_t)inWidth
                  height:(size_t)inHeight
             bytesPerRow:(size_t)inBytesPerRow;

@end

#endif /* ImageHelper_h */
