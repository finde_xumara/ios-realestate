//
//  RVMViewModel+Edits.h
//  RoomScan
//
//  Created by Finde Xumara on 22/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RVMViewModel.h"

// Services
#import "APIClientManager.h"

@interface RVMViewModel (Edits)

-(instancetype)initWithAPIManager;

+(APIClientManager *) apiManager;
@end
