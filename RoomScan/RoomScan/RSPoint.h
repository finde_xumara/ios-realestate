//
//  RSPoint.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 22/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RSAnnotation;

NS_ASSUME_NONNULL_BEGIN

@interface RSPoint : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "RSPoint+CoreDataProperties.h"
