//
//  Shader.vsh
//  RoomScan
//
//  Created by Finde Xumara on 08/05/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

attribute vec4 position;
attribute vec3 color;
attribute vec3 normal;

varying highp vec4 colorVarying;

uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;
uniform highp float opacity;

void main()
{
    vec3 eyeNormal = normalize(normalMatrix * normal);
    vec3 lightPosition = vec3(0.0, 0.0, 1.0);
    vec4 diffuseColor = vec4(1.0);
    
    float nDotVP = max(0.0, dot(eyeNormal, normalize(lightPosition)));
    colorVarying = diffuseColor * nDotVP;
    
    
    colorVarying = vec4(color, opacity);
    
    gl_PointSize = 8.0;
    gl_Position = modelViewProjectionMatrix * position;
}
