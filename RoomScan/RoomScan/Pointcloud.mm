//
//  Pointcloud.m
//  RoomScan
//
//  Created by Finde Xumara on 17/05/16.
//  Copyright © 2016 Occipital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PointCloud.h"
#import "RSScanViewModel.h"

@implementation Refinement

-(id) init {
    self = [super init];
    if (self)
    {
        _rotation = 0;
        _translation = GLKVector2Make(0, 0);
        _transformMat = GLKMatrix4Identity;
    }
    return self;
}

-(GLKMatrix4) transformMatrix {
    
    GLKMatrix4 tMat = GLKMatrix4Identity;
    tMat = GLKMatrix4Translate(tMat, _translation.x, 0, _translation.y);
    tMat = GLKMatrix4Rotate(tMat, _rotation ,0, -1,0);
    
    return tMat;
}

@end

@implementation Pointcloud

-(id) init {
    self = [super init];
    if (self)
    {
        _refinement = [Refinement new];
    }
    return self;
}

@end