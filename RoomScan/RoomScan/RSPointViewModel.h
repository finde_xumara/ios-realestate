//
//  RSAnnotationViewModel.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 23/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#ifndef RSPointViewModel_h
#define RSPointViewModel_h

#import <Structure/Structure.h>
#import "RVMViewModel.h"
#import "RVMViewModel+Edits.h"

@class RSAnnotation, RSPoint;

@interface RSPointViewModel : RVMViewModel {
    
}

@property (nonatomic, readonly) RSPoint *model;

@property (nonatomic, strong) NSNumber *x;
@property (nonatomic, strong) NSNumber *y;
@property (nonatomic, strong) NSNumber *isEdge;

- (instancetype)initWithModel: (id)model;
- (void)setPositionWithCGPoint: (CGPoint)location;
- (void)setPositionWithVector: (GLKVector2)location;
- (float)hitSquareDistance: (GLKVector2)point;
- (GLKVector2) getVector;
@end


#endif /* RSPointViewModel_h */
