//
//  MathHelper.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 23/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RSPoint;

@interface MathHelper : NSObject

//+ (GLKVector2) convertToViewSpace:(GLKVector2)pointInView withOptions:(NSDictionary *) options;
+ (GLKVector2) convertRSPointToViewSpace:(RSPoint *)point withOptions:(NSDictionary *) options;

//+ (GLKVector2) convertTo3DSpace:(GLKVector2)pointInView withOptions:(NSDictionary *) options;

+ (GLKVector2) convertTo3DSpaceWithCGPoint:(CGPoint)pointInView withOptions:(NSDictionary *) options;

@end
