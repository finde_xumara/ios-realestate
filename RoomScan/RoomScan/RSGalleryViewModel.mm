//
//  RSGalleryViewModel.m
//  RoomScan
//
//  Created by Finde Xumara on 06/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RSGalleryViewModel.h"
#import "RSCoreDataStack.h"

// Models
#import "RSEnvironment.h"

// View Models
#import "RSEnvironmentViewModel.h"

@interface RSGalleryViewModel () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) RACSubject *updatedContentSignal;

-(RSEnvironment *)environmentAtIndexPath:(NSIndexPath *)indexPath;

@end

@implementation RSGalleryViewModel

-(instancetype)initWithModel:(id)model {
    self = [super initWithAPIManager];
    self.model = model;
    
    if (self == nil) return nil;
    
    self.updatedContentSignal = [[RACSubject subject] setNameWithFormat:@"RSGalleryViewModel updatedContentSignal"];
    
    @weakify(self)
    [self.didBecomeActiveSignal subscribeNext:^(id x) {
        @strongify(self);
        [self.fetchedResultsController performFetch:nil];
    }];
    
    return self;
}

- (void)addObject:(NSString *)environmentName {
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    
    RSEnvironment *newEnv = [NSEntityDescription insertNewObjectForEntityForName:@"RSEnvironment" inManagedObjectContext:[RSCoreDataStack defaultStack].managedObjectContext];
    
    newEnv.environmentName = environmentName;
    newEnv.environmentId = [[NSUUID UUID] UUIDString];
    newEnv.createdAt = [NSDate date];
    newEnv.modifiedAt = [NSDate date];
    
    [newEnv saveToContext: context];
}

- (void)deleteObjectAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *oldEnv = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    [context deleteObject:oldEnv];
    
    NSError *error = nil;
    if ([context save:&error] == NO) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

#pragma mark - Collection View

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
//    return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger) collectionView: (UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
//    return [[self.fetchedResultsController sections] count];
    return [[self.fetchedResultsController fetchedObjects] count];

    
//    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
//    return [sectionInfo numberOfObjects];
}

- (NSString *)titleAtIndexPath:(NSIndexPath *)indexPath {
    RSEnvironment *env = [self environmentAtIndexPath:indexPath];
    return env.environmentName;
}

- (NSString *)subtitleAtIndexPath:(NSIndexPath *)indexPath {
    RSEnvironment *env = [self environmentAtIndexPath:indexPath];
    return env.environmentId;
}

- (RSEnvironmentViewModel *)environmentModelForIndexPath:(NSIndexPath *)indexPath {
    RSEnvironment *model = [self environmentAtIndexPath:indexPath];
    RSEnvironmentViewModel *viewModel = [[RSEnvironmentViewModel alloc] initWithModel:model];
    
    return viewModel;
}

#pragma mark - Private Methods

-(RSEnvironment *)environmentAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath * correctedIP = [NSIndexPath indexPathForRow:0 inSection:indexPath.item];

    return [self.fetchedResultsController objectAtIndexPath:correctedIP];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"RSEnvironment" inManagedObjectContext:self.model];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"environmentName" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.model sectionNameKeyPath:@"environmentName" cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [(RACSubject *)self.updatedContentSignal sendNext:nil];
}



@end
