//
//  RSScanViewModel.m
//  RoomScan
//
//  Created by Finde Xumara on 14/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

// ViewController
#import "RSScanViewController.h"

// View Models
#import "RSScanViewModel.h"

// Models
#import "RSScan.h"
#import "RSEnvironment.h"

#import "Endian.h"
#import "SDVersion.h"

#import "Frame3D.h"
#import <opencv2/imgproc.hpp>
#import "Ply3dfHelper.h"
#import "ImageHelper.h"

#import "PCLHelper.h"
#import <pcl/point_cloud.h>
#import <pcl/io/ply_io.h>
#import <pcl/filters/voxel_grid.h>
#import <pcl/filters/passthrough.h>

namespace {
    NSString* stringWithMatrix (GLKMatrix4 pose) {
        NSMutableString *string = [[NSMutableString alloc] init];
        [string appendFormat: @"%f,%f,%f,%f,", pose.m00, pose.m01, pose.m02, pose.m03];
        [string appendFormat: @"%f,%f,%f,%f,", pose.m10, pose.m11, pose.m12, pose.m13];
        [string appendFormat: @"%f,%f,%f,%f,", pose.m20, pose.m21, pose.m22, pose.m23];
        [string appendFormat: @"%f,%f,%f,%f",  pose.m30, pose.m31, pose.m32, pose.m33];
        return string;
    }
    
//    float deltaRotationAngleBetweenPosesInDegrees (const GLKMatrix4& previousPose, const GLKMatrix4& newPose) {
//        
//        GLKMatrix4 deltaPose = GLKMatrix4Multiply(newPose,
//                                                  // Transpose is equivalent to inverse since we will only use the rotation part.
//                                                  GLKMatrix4Transpose(previousPose));
//        
//        GLKQuaternion deltaRotationAsQuaternion = GLKQuaternionMakeWithMatrix4(deltaPose);
//        
//        const float angleInDegree = GLKQuaternionAngle(deltaRotationAsQuaternion)*180.f/M_PI;
//        return angleInDegree;
//    }
//    
    cv::Mat cvMatFromUIImage(UIImage * image)
    {
        CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
        CGFloat cols = image.size.width;
        CGFloat rows = image.size.height;
        
        cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
        
        CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                        cols,                       // Width of bitmap
                                                        rows,                       // Height of bitmap
                                                        8,                          // Bits per component
                                                        cvMat.step[0],              // Bytes per row
                                                        colorSpace,                 // Colorspace
                                                        kCGImageAlphaNoneSkipLast |
                                                        kCGBitmapByteOrderDefault); // Bitmap info flags
        
        CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
        CGContextRelease(contextRef);
        
        return cvMat;
    }
}

@interface RSScanViewModel ()

@property (nonatomic, strong) RSScan *model;
@property (nonatomic, strong) NSString * depthDataPath;
@property (nonatomic, strong) NSString * keyframeDataPath;
@property (atomic, strong) NSFileHandle * cameraPositionHandle;

@property (atomic) unsigned int tagCounter;

@end

@implementation RSScanViewModel

-(instancetype)initWithModel:(id)model {
    self = [super initWithAPIManager];
    self.model = model;
    
    if (self == nil) return nil;
    
    RAC(self, scanId) = [RACObserve(self.model, scanId) map:^id(NSString *value) {
        return value;
    }];
    
    RAC(self, scanName) = [RACObserve(self.model, scanName) map:^id(NSString *value) {
        return value;
    }];
    
    RAC(self, serverId) = [RACObserve(self.model, serverId) map:^id(NSString *value) {
        return value;
    }];
    
    RAC(self, environmentId) = [RACObserve(self.model, environmentObj) map:^id(RSEnvironment *value) {
        return value.environmentId;
    }];
    
    RAC(self, environmentServerId) = [RACObserve(self.model, environmentObj) map:^id(RSEnvironment *value) {
        return value.serverId;
    }];
    
    RAC(self, environmentDir) = [RACObserve(self.model, environmentObj) map:^id(RSEnvironment *value) {
        return [[value getPath] path];;
    }];
    
    _numberOfCapturedData = 0;
    _progressValue = 0.0;
    _debugMessage = @"ready";
    _lastTimestamp = 0;
    
    _totalStep = ((MAX_ROTATION - MIN_ROTATION) / ROTATE_STEP + 1) * ((MAX_TILT - MIN_TILT) / TILT_STEP + 1);
    
    _depthDataPath = [[[self.model getPath] URLByAppendingPathComponent:kDepthDir] path];
    _keyframeDataPath = [[[self.model getPath] URLByAppendingPathComponent:kKeyframeDir] path];
    
    _cameraPositionHandle = [self cameraHandleAtDirectoryPath:[[self.model getPath] path]];
    
    return self;
}

-(void)willDismiss {
    self.model.modifiedAt = [NSDate date];
    [self.model.managedObjectContext save:nil];
}

-(void)cancel {
    [self.model.managedObjectContext deleteObject:self.model];
}

-(GLKVector2)getTranslation {
    GLKMatrix4 transformationMat = [self getTransformation];

    return GLKVector2Make(transformationMat.m30, transformationMat.m32);
}

-(void)setTranslation:(GLKVector2) translation {
    GLKMatrix4 transformationMat = [self getTransformation];
    
    transformationMat.m30 = translation.x;
    transformationMat.m32 = translation.y;
    
    [self setTransformation: transformationMat];
}

- (GLKMatrix4) getTransformation {
    return [self.model getTransformationMatrix];
}

- (void) setTransformation: (GLKMatrix4) transformationMat {
    [self.model setTransformationMatrix: transformationMat];
}

- (NSDictionary *) getAlignmentMeta {
    return @{@"transformationMatrix":[self.model getTransformationArray]};
}

// process continuos frame
-(void) processDepthFrame:(STDepthFrame *)depthFrame
               colorFrame:(STColorFrame *)colorFrame
            rotopanStatus:(Rotopan *) rotopan
                     pose:(GLKMatrix4) pose {
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        unsigned int tag = self.tagCounter++;
        
        // convert color data
        UIImage *rgbImage = [ImageHelper imageFromColorFrame:colorFrame];
        
        // convert camera data
        NSMutableString *stringPose = [[NSMutableString alloc] init];
        [stringPose appendFormat:@"%@,%d,%d,%d,%d,%d,%.2f,%.2f,%.2f\n", stringWithMatrix(pose), tag,
         rotopan.panValue, rotopan.tiltValue, rotopan.panAbsoluteValue, ZERO_TILT,
         GLKMathRadiansToDegrees(rotopan.motion.attitude.yaw),
         GLKMathRadiansToDegrees(rotopan.motion.attitude.pitch),
         GLKMathRadiansToDegrees(rotopan.motion.attitude.roll)];
        
        cv::Mat cameraMatrix(4,4,CV_32FC1);
        cameraMatrix.at<float>(0,0) = pose.m00;
        cameraMatrix.at<float>(0,1) = pose.m10;
        cameraMatrix.at<float>(0,2) = pose.m20;
        cameraMatrix.at<float>(0,3) = /*pose.m30;*/ GLKMathRadiansToDegrees(rotopan.motion.attitude.yaw);
        
        cameraMatrix.at<float>(1,0) = pose.m01;
        cameraMatrix.at<float>(1,1) = pose.m11;
        cameraMatrix.at<float>(1,2) = pose.m21;
        cameraMatrix.at<float>(1,3) = /*pose.m31;*/ GLKMathRadiansToDegrees(rotopan.motion.attitude.pitch);
        
        cameraMatrix.at<float>(2,0) = pose.m02;
        cameraMatrix.at<float>(2,1) = pose.m12;
        cameraMatrix.at<float>(2,2) = pose.m22;
        cameraMatrix.at<float>(2,3) = /*pose.m32;*/ GLKMathRadiansToDegrees(rotopan.motion.attitude.roll);
        
        cameraMatrix.at<float>(3,0) = /*pose.m03;*/ rotopan.panAbsoluteValue;
        cameraMatrix.at<float>(3,1) = /*pose.m13;*/ rotopan.tiltValue;
        cameraMatrix.at<float>(3,2) = /*pose.m23;*/ ZERO_TILT;
        cameraMatrix.at<float>(3,3) = pose.m33;
        
        cv::Mat rgbMat = cvMatFromUIImage(rgbImage);
        cv::Mat depthMat = cv::Mat(depthFrame.height, depthFrame.width, CV_32F, depthFrame.depthInMillimeters);
        depthMat.convertTo(depthMat, CV_16U);
        
        cv::cvtColor(rgbMat,rgbMat,cv::COLOR_BGRA2BGR);
        Frame3D scanData = Frame3D(rgbMat, depthMat, cameraMatrix, 0);
        NSString *filename = [NSString stringWithFormat:@"%d.3df", (int) tag];
        NSURL *filePath = [[_model getPath] URLByAppendingPathComponent:filename];
        
        scanData.save(std::string([[filePath path] UTF8String]));
        
        // Store camera pose data
        NSData *cameraPoseData = [stringPose dataUsingEncoding: NSUTF8StringEncoding];
        [self.cameraPositionHandle seekToEndOfFile];
        [self.cameraPositionHandle writeData: cameraPoseData];
        
        ////// transform to point cloud
        int _width = 640;
        int _height = 480;
        cv::Mat depth = cv::Mat::zeros(_height, _width, CV_16U);
        cv::Mat color = cv::Mat::zeros(_height, _width, CV_8UC3);
        
        float thresh = 3.0;
        float focal_length = 576;
        pcl::PointCloudPtr<pcl::PointXYZRGBNormal> frame_cloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
        
        // ensure the size
        cv::resize(rgbMat, color, color.size(), 0, 0, cv::INTER_CUBIC);
        cv::resize(depthMat, depth, depth.size(), 0, 0, cv::INTER_CUBIC);
        depth2Pointcloud(depth, color, frame_cloud, focal_length, thresh);
        filename = [NSString stringWithFormat:@"%d.ply", (int) tag];
        filePath = [[_model getPath] URLByAppendingPathComponent:filename];
        pcl::io::savePLYFileBinary(std::string([[filePath path] UTF8String]), *frame_cloud);
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            //TODO:: Run UI Updates
            _debugMessage = [NSString stringWithFormat:@"added frame: %d\n%@", tag+1, _debugMessage ];
            _progressValue = (tag+1) * 100 / _totalStep;
            NSLog(@"added frame: %d / %f",tag+1, _totalStep);
        });
        
    });
    
}

-(void) generatePly {
    [Ply3dfHelper createPLYFileFrom3DFDir:_environmentDir scanName:_scanName];
}


#pragma mark - the TODO
-(void) prepareScanning {
    
}
-(void) wipeLastObjectCache {
    
}
-(void) processCache {
    
}

-(void) updateServerId:(NSString *)serverId {
    self.model.serverId = serverId;
    [self.model.managedObjectContext save:nil];
}

- (NSFileHandle*) cameraHandleAtDirectoryPath: (NSString*) directoryPath
{
    NSString *path = [directoryPath stringByAppendingPathComponent: kCameraPositionFile];
    [[NSFileManager defaultManager] createFileAtPath: path contents: [[NSData alloc] init] attributes: nil];
    NSFileHandle* fileHandle = [NSFileHandle fileHandleForWritingAtPath: path];
    
    // Text
    NSString *header = @"# tag, pan, tilt, panAbsolute, middleTilt\n";
    [fileHandle seekToEndOfFile];
    [fileHandle writeData: [header dataUsingEncoding: NSUTF8StringEncoding]];
    
    return fileHandle;
}


@end


