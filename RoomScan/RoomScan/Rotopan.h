//
//  Rotopan.h
//  RoomScan
//
//  Created by Finde Xumara on 08/07/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//
#import <CoreMotion/CoreMotion.h>

#ifndef Rotopan_h
#define Rotopan_h

static const int kDataPerAngle = 1;
static const int kDelayForStabilizing = 1;

enum RotopanStatus {
    RotopanStatusDisconnected = 0,
    RotopanStatusReady,
    RotopanStatusCalibrating,
    RotopanStatusScanning
};

enum RotopanDirection {
    RotopanDirectionUp = 0,
    RotopanDirectionDown
};

enum RotopanTask {
    RotopanTaskNone = 0,
    RotopanTaskRotate,
    RotopanTaskTilt
};

@interface Rotopan : NSObject {
}

@property (nonatomic) int stepRotopan;
@property (nonatomic) int tiltRotopan;
@property (nonatomic) int counter;

@property (nonatomic) enum RotopanStatus deviceStatus;
@property (nonatomic) enum RotopanDirection deviceDirection;
@property (nonatomic) enum RotopanTask deviceTask;

@property (nonatomic) BOOL isAngleDone;
@property (nonatomic) BOOL isTaskConfirmed;

// Device data
@property (nonatomic, strong) CMDeviceMotion *motion;
@property (nonatomic) int tiltValue;
@property (nonatomic) int panValue;
@property (nonatomic) int panAbsoluteValue;
@property (nonatomic) int numberOfCapturedData;
@property (nonatomic) int waiting_unit;

- (void) reset;

@end


#endif /* Rotopan_h */
