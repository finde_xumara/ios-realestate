//
//  ScanMTL.h
//  RoomScan
//
//  Created by Finde Xumara on 22/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface ScanMTL : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong, readonly) NSString *serverId;
@property (nonatomic, strong, readonly) NSString *localId;
@property (nonatomic, strong, readonly) NSString *envId;
@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSDate *createdAt;
@property (nonatomic, strong, readonly) NSDate *modifiedAt;

@end