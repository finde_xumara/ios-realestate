//
//  APIClient.h
//  ServerCommunication
//
//  Created by Daniel Hallin on 04/04/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPSessionManager.h>


//static NSString * const kServerHost = @"iamai.nl";
//static NSInteger  const kServerPort = 33333;
//static NSString * const kServerPath = @"/v1/";

//static NSString * const kServerHost = @"localhost";
//static NSString * const kServerHost = @"10.250.99.30";

// local
#ifdef SENDSTREAM
//data collector
static NSString * const kServerHost = WIFI_CONT_IP;
static NSInteger  const kServerPort = WIFI_CONT_PORT;
static NSString * const kServerPath = @"/";
#else
static NSString * const kServerHost = WIFI_CONT_IP;
static NSInteger  const kServerPort = WIFI_CONT_PORT;
static NSString * const kServerPath = @"/v1/";

#endif


@interface APIClient : AFHTTPSessionManager

+ (instancetype) sharedClient;
- (instancetype) init;

@end
