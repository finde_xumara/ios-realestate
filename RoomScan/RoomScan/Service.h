//
//  Service.h
//  ServerCommunication
//
//  Created by Daniel Hallin on 21/04/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIClient.h"

@protocol ServiceDelegate;

typedef void(^ServiceCompletionBlock)(id result, NSError* error);
typedef void(^ServiceProgressBlock)(NSProgress* progress);

@interface Service : NSObject

@property (nonatomic, assign) id<ServiceDelegate> delegate;

- (instancetype) initWithDelegate: (id<ServiceDelegate>) delegate;

@end

@interface NetworkService : Service
{
    APIClient *_apiClient;
}
@end

@protocol ServiceDelegate <NSObject>
@optional

- (void) handleServiceProgress: (NSProgress*) progress;
- (void) handleServiceError: (NSError*) error;
- (void) handleServiceCompletion: (NSString*) message;

@end

