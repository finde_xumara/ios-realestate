//
//  MathHelper.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 23/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "MathHelper.h"
#import "RSPoint.h"

@implementation MathHelper

+ (GLKVector2) convertRSPointToViewSpace:(RSPoint *)point withOptions:(NSDictionary *) options {
    float width = [options[@"Width"] floatValue];
    float height = [options[@"Height"] floatValue];
    float scale = [options[@"Scale"] floatValue];
    CGPoint centerLocation = [options[@"Center"] CGPointValue];
    
    float scale_factor = height / scale;
    
    return GLKVector2Make(([point.x doubleValue] + centerLocation.x) * scale_factor + width * 0.5,
                          ([point.y doubleValue] + centerLocation.y) * scale_factor + height * 0.5);
}

// from pixel point to obj Space
+ (GLKVector2) convertTo3DSpace:(GLKVector2)pointInView withOptions:(NSDictionary *) options
{
    float width = [options[@"Width"] floatValue];
    float height = [options[@"Height"] floatValue];
    float scale = [options[@"Scale"] floatValue];
    CGPoint centerLocation = [options[@"Center"] CGPointValue];
    
    float scale_factor = scale / height;
    
    return GLKVector2Make((pointInView.x - width * 0.5) * scale_factor - centerLocation.x,
                          (pointInView.y - height * 0.5) * scale_factor - centerLocation.y);
}

+ (GLKVector2) convertTo3DSpaceWithCGPoint:(CGPoint)pointInView withOptions:(NSDictionary *) options
{
    GLKVector2 vectTap = GLKVector2Make(pointInView.x, pointInView.y);
    return [self convertTo3DSpace:vectTap withOptions:options];
}

@end
