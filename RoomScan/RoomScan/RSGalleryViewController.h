//
//  ViewController.h
//  RoomScan
//
//  Created by Finde Xumara on 06/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RSGalleryViewModel;

@interface RSGalleryViewController : UICollectionViewController <UICollectionViewDelegate>

@property (nonatomic, strong) RSGalleryViewModel *viewModel;
@property (nonatomic, strong) IBOutlet UICollectionView *galleryView;

@end
