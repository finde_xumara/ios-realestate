//
//  RSGalleryViewCell.h
//  RoomScan
//
//  Created by Finde Xumara on 07/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSGalleryViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@property (weak, nonatomic) IBOutlet UIView *frontCard;
@property (weak, nonatomic) IBOutlet UIView *backCard;
@property (weak, nonatomic) IBOutlet UIView *highlightLayer;

@property (strong, nonatomic) IBOutlet UIButton * uploadButton;

@property (nonatomic) BOOL isSelected;

-(BOOL) isFront;
-(void) flip;
@end

