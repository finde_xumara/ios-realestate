//
//  main.m
//  RoomScan
//
//  Created by Finde Xumara on 06/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
