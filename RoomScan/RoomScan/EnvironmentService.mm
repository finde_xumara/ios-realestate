//
//  ScanServices.m
//  ServerCommunication
//
//  Created by Daniel Hallin on 21/04/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "EnvironmentService.h"
//#import "UIDevice+Extensions.h"
#import "ZipService.h"

#import "RSEnvironmentViewModel.h"
#import "RSAnnotationViewModel.h"
#import "RSPointViewModel.h"
#import "RSScanViewModel.h"
#import "RSScan.h"

#import "EnvironmentMTL.h"
#import "ScanMTL.h"

@implementation EnvironmentService

- (BOOL) isLoggedIn
{
    return false;
//    return !!self.cookies;
}

- (NSArray*) cookies
{
    return [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL: [NSURL URLWithString:@"WIFI_CONT_IP"]];
}

- (RACSignal *) login:(RSEnvironmentViewModel *) viewModel {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSURLSessionDataTask *dataTask;
        
        //TODO check cookies
        if ([self isLoggedIn]) {
            [subscriber sendNext:viewModel];
            [subscriber sendCompleted];
            
        } else {
            
            dataTask = [_apiClient POST: @"users/login"
                             parameters: nil
              constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
                        {
                            
                            [formData appendPartWithFormData:[@"kubilay.ozisik@outlook.com" dataUsingEncoding:NSUTF8StringEncoding] name:@"email"];
                            [formData appendPartWithFormData:[@"1234" dataUsingEncoding:NSUTF8StringEncoding] name:@"password"];

                        }
                               progress: nil
                                success:^(NSURLSessionTask * _Nonnull operation, id  _Nonnull responseObject)
                        {                            
                            [subscriber sendNext:viewModel];
                            [subscriber sendCompleted];
                        }
                                failure:^(NSURLSessionTask * _Nullable operation, NSError * _Nonnull error)
                        {
                            [subscriber sendError:error];
               	         }];
            
            [dataTask resume];
        }
        
        return [RACDisposable disposableWithBlock:^{
            if(dataTask != nil) [dataTask cancel];
        }];
        
    }];
}


- (RACSignal *) fetchEnvironments {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSMutableDictionary *parameters = [NSMutableDictionary new];
        
        NSURLSessionDataTask *dataTask = [_apiClient GET: @"environments"
                                              parameters: parameters
                                                progress: nil
                                                 success:^(NSURLSessionTask * _Nonnull operation, id  _Nonnull responseObject)
                                          {
                                              
                                              NSLog(@"[GET] environments");
                                              NSDictionary *results = responseObject;
                                              
                                              [subscriber sendNext:results];
                                          }
                                                 failure:^(NSURLSessionTask * _Nullable operation, NSError * _Nonnull error)
                                          {
                                              [subscriber sendError:error];
                                          }];
        
        //        [subscriber sendCompleted];
        [dataTask resume];
        
        return [RACDisposable disposableWithBlock:^{
            [dataTask cancel];
        }];
    }];
}

- (RACSignal *) fetchEnvironment:(RSEnvironmentViewModel *) viewModel {
    return [[self fetchEnvironments] map:^(NSDictionary *json) {
        
        NSError *error = nil;
        
        for (NSDictionary *_env in json) {
            EnvironmentMTL *serverObj = [MTLJSONAdapter modelOfClass:EnvironmentMTL.class fromJSONDictionary:_env error: &error];
            
            if (!error) {
                if ([serverObj.serverId isEqualToString:viewModel.serverId]) {
                    [viewModel updateServerId:serverObj.serverId];
                    return viewModel;
                }
            }
        }
        
        return viewModel;
    }];
}

- (RACSignal *) createNewEnvironmentWithViewModel:(RSEnvironmentViewModel *) viewModel {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSMutableDictionary *parameters = [NSMutableDictionary new];
        
        NSMutableDictionary *meta = [NSMutableDictionary new];
        meta[@"annotations"] = [viewModel getAnnotationsMeta];
        meta[@"alignments"] = [viewModel getAlignmentsMeta];
        
        NSData *json = [NSJSONSerialization dataWithJSONObject:meta
                                                       options:0
                                                         error:nil];
        NSString *jsonMeta = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        
        NSURLSessionDataTask *dataTask;
        
        if (viewModel) parameters[@"name"]  = viewModel.environmentName;
        if (viewModel) parameters[@"local_id"]   = viewModel.environmentId;
        if (viewModel) parameters[@"meta"]  = jsonMeta;
        
        if (viewModel.serverId != nil && ![viewModel.serverId isEqualToString:@""]) {
            
            [subscriber sendNext:viewModel];
            [subscriber sendCompleted];
            
        } else {
            
            dataTask = [_apiClient POST: @"environments"
                             parameters: parameters
                               progress: nil
                                success:^(NSURLSessionTask * _Nonnull operation, id  _Nonnull responseObject)
                        {
                            NSLog(@"[POST] environments");
                            NSError *error = nil;
                            EnvironmentMTL *env = [MTLJSONAdapter modelOfClass:EnvironmentMTL.class fromJSONDictionary:responseObject error: &error];
                            if (!error) {
                                [viewModel updateServerId:env.serverId];
                                [subscriber sendNext:viewModel];
                                [subscriber sendCompleted];
                            } else {
                                [subscriber sendError:error];
                            }
                        }
                                failure:^(NSURLSessionTask * _Nullable operation, NSError * _Nonnull error)
                        {
                            [subscriber sendError:error];
                        }];
            
            [dataTask resume];
        }
        
        return [RACDisposable disposableWithBlock:^{
            if(dataTask != nil) [dataTask cancel];
        }];
        
    }];
}

- (RACSignal *) createNewScanWithViewModel:(RSScanViewModel *) viewModel {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSMutableDictionary *parameters = [NSMutableDictionary new];
        
        NSMutableDictionary *meta = [NSMutableDictionary new];
        meta[@"alignment"] = [viewModel getAlignmentMeta];
        NSData *json = [NSJSONSerialization dataWithJSONObject:meta
                                                       options:0
                                                         error:nil];
        NSString *jsonMeta = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        
        
        NSURLSessionDataTask *dataTask;
        
        if (viewModel) {
            parameters[@"name"]   = viewModel.scanName;
            parameters[@"local_id"]   = viewModel.scanId;
            parameters[@"env_id"]   = viewModel.environmentServerId;
            
//            parameters[@"user_id"]   = @"1";
            parameters[@"device_id"]   = viewModel.scanId;
            parameters[@"device_version"]   = @"random";
            parameters[@"meta"]  = jsonMeta;
        }
        
        if (viewModel.serverId != nil && ![viewModel.serverId isEqualToString:@""]) {
            
            [subscriber sendNext:viewModel];
            
        } else {
            
            dataTask = [_apiClient POST: @"scans"
                             parameters: parameters
                               progress: nil
                                success:^(NSURLSessionTask * _Nonnull operation, id  _Nonnull responseObject)
                        {
                            NSLog(@"[POST] environments");
                            NSError *error = nil;
                            EnvironmentMTL *env = [MTLJSONAdapter modelOfClass:ScanMTL.class fromJSONDictionary:responseObject error: &error];
                            if (!error) {
                                [viewModel updateServerId:env.serverId];
                                [subscriber sendNext:viewModel];
                                //                                [subscriber sendCompleted];
                            } else {
                                [subscriber sendError:error];
                            }
                        }
                                failure:^(NSURLSessionTask * _Nullable operation, NSError * _Nonnull error)
                        {
                            [subscriber sendError:error];
                        }];
            
            [dataTask resume];
        }
        
        return [RACDisposable disposableWithBlock:^{
            if(dataTask != nil) [dataTask cancel];
        }];
        
    }];
}

- (RACSignal *) uploadScanDataWithViewModel:(RSScanViewModel *) viewModel {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSMutableDictionary *parameters = [NSMutableDictionary new];
        NSURLSessionDataTask *dataTask;
        
        NSString *URLString = [NSString stringWithFormat: @"scans/%@/resource", viewModel.serverId];
        
        parameters[@"collection_name"] = @"scandata";
        parameters[@"unzip"]           = @YES;
        
        NSData *zipData = [ZipService zippedDataFromDirectory: [viewModel.model.getPath path]];
        
        if (viewModel.serverId != nil && ![viewModel.serverId isEqualToString:@""]) {
            
            dataTask = [_apiClient POST: URLString
                             parameters: parameters
              constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
                        {
                            
                            [formData appendPartWithFileData: zipData
                                                        name: @"scandata.zip"
                                                    fileName: @"scandata.zip"
                                                    mimeType: @"zip"];
                        }
                               progress:^(NSProgress * _Nonnull uploadProgress)
                        {
                            float progress = (float) uploadProgress.completedUnitCount / uploadProgress.totalUnitCount;
                            NSLog(@"%.2f", progress*100);
                            // TODO: update progress bar
                            
                            //                            [subscriber sendNext:[NSNumber numberWithFloat: progress]];
                        }
                                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                        {
                            // delete zip file
                            
                            [subscriber sendNext:[NSNumber numberWithFloat: 1]];
                            //                            [subscriber sendCompleted];
                        }
                                failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                        {
                            //                            [self.delegate handleServiceError: error];
                            [subscriber sendError:error];
                        }];
            
            [dataTask resume];
        } else {
            [subscriber sendNext:[NSNumber numberWithFloat: -1]];
            [subscriber sendCompleted];
        }
        
        return [RACDisposable disposableWithBlock:^{
            if(dataTask != nil) [dataTask cancel];
        }];
        
    }];
}

- (RACSignal *) uploadContinousDepth:(NSData *) depthData
                               Color:(NSData *) colorData
                                Pose:(NSString *) pose
                                 Tag:(int) tag
                             ModelID:(NSString *) modelID{
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSMutableDictionary *parameters = [NSMutableDictionary new];
        NSURLSessionDataTask *dataTask;
        
        parameters[@"modelID"] = modelID;
        
        NSString *URLString = [NSString stringWithFormat: @"storeFile"];
        
        dataTask = [_apiClient POST: URLString
                         parameters: parameters
          constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
                    {
                        
                        [formData appendPartWithFileData: depthData
                                                    name: @"depthFile"
                                                fileName: [NSString stringWithFormat:@"%d", tag]
                                                mimeType: @"application/octet-stream"];
                        
                        [formData appendPartWithFileData: colorData
                                                    name: @"colorFile"
                                                fileName: [NSString stringWithFormat:@"%d.jpg", tag]
                                                mimeType: @"image/jpeg"];
                        
                        [formData appendPartWithFileData: [pose dataUsingEncoding:NSUTF8StringEncoding]
                                                    name: @"poseFile"
                                                fileName: [NSString stringWithFormat:@"%d.txt", tag]
                                                mimeType: @"text/plain"];
                    }
                           progress:^(NSProgress * _Nonnull uploadProgress)
                    {
                        
                    }
                            success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                    {
                        [subscriber sendNext:[NSNumber numberWithFloat: 1]];
                    }
                            failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                    {
                        [subscriber sendError:error];
                    }];
        
        [dataTask resume];
        
        return [RACDisposable disposableWithBlock:^{
            if(dataTask != nil) [dataTask cancel];
        }];
        
    }];
}

- (RACSignal *) upload3DFrame:(NSData *) frame3d
                          Tag:(int) tag
//                         Pose:(NSString *) pose
                      ModelID:(NSString *) modelID {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSMutableDictionary *parameters = [NSMutableDictionary new];
        NSURLSessionDataTask *dataTask;
        
        parameters[@"modelID"] = modelID;
        
        NSString *URLString = [NSString stringWithFormat: @"storeFrame3D"];
        
        dataTask = [_apiClient POST: URLString
                         parameters: parameters
          constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
                    {
                        
                        [formData appendPartWithFileData: frame3d
                                                    name: @"frame3d"
                                                fileName: [NSString stringWithFormat:@"%d.3df", tag]
                                                mimeType: @"application/octet-stream"];
                        
//                        [formData appendPartWithFileData: [pose dataUsingEncoding:NSUTF8StringEncoding]
//                                                    name: @"poseFile"
//                                                fileName: [NSString stringWithFormat:@"%d.txt", tag]
//                                                mimeType: @"text/plain"];

                    }
                           progress:^(NSProgress * _Nonnull uploadProgress)
                    {
                        
                    }
                            success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                    {
                        [subscriber sendNext:[NSNumber numberWithFloat: 1]];
                    }
                            failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                    {
                        [subscriber sendError:error];
                    }];
        
        [dataTask resume];
        
        return [RACDisposable disposableWithBlock:^{
            if(dataTask != nil) [dataTask cancel];
        }];
        
    }];
}


@end
