//
//  FileUtils.h
//  RoomScan
//
//  Created by Finde Xumara on 14/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileHelper : NSObject {
}

// Returns the URL to the application's Documents directory.
+(NSURL *) applicationDocumentsDirectory;

+(BOOL) createDirectoryIfNotExist:(NSString *)newDirectoryName withInDirectory:(NSURL *) rootDir;

@end

