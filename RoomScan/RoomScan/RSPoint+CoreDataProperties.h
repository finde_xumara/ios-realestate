//
//  RSPoint+CoreDataProperties.h
//  ios-RoomScan
//
//  Created by Finde Xumara on 22/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RSPoint.h"

NS_ASSUME_NONNULL_BEGIN

@interface RSPoint (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *isEdge;
@property (nullable, nonatomic, retain) NSNumber *x;
@property (nullable, nonatomic, retain) NSNumber *y;
@property (nullable, nonatomic, retain) RSAnnotation *annotationObj;

@end

NS_ASSUME_NONNULL_END
