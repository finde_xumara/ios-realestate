//
//  ScanCell.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 18/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "ScanCell.h"

@implementation ScanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
