//
//  RSEditEnvironmentViewController.m
//  RoomScan
//
//  Created by Finde Xumara on 07/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

// View Controllers
#import "RSEnvironmentViewController.h"
#import "RSScanViewController.h"

// embed
#import "PCGLViewController.h"
#import "LeftTableViewController.h"

// View Models
#import "RSEnvironmentViewModel.h"

// Models
#import "RSEnvironment.h"
#import "RSScan.h"
#import "RSAnnotation.h"

#import "AnnotationView.h"

@interface RSEnvironmentViewController ()

@end

@implementation RSEnvironmentViewController

#pragma mark - UIViewController Overrides

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // ReactiveCocoa Bindings
    RAC(self, title) = RACObserve(self.viewModel, environmentName);
    
//    NSLog(@"%d", [[self.viewModel getAnnotationsMeta] count]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma mark - User Interaction

-(IBAction)backWasPressed:(id)sender {
    [self dismissSelf];
}

#pragma mark - Private Methods

-(void)dismissSelf {
    [self.viewModel willDismiss];

    // self save to file
    
    [self.navigationController popViewControllerAnimated: YES];
}

-(IBAction)automaticAlignmentButtonPressed:(id)sender {
    [self.pointcloudViewController automaticAlignment];
    [self.pointcloudViewController.annotationView setNeedsDisplay];
}

-(IBAction)addAnchorButtonPressed:(id)sender {
    [self.pointcloudViewController setViewMode: ViewMode::ADD_ANCHOR_MODE];
//    [self.pointcloudViewController.annotationView resetSelection];
    [self.pointcloudViewController.annotationView setNeedsDisplay];
}

-(IBAction)addMarkerButtonPressed:(id)sender {
    [self.pointcloudViewController setViewMode: ViewMode::ADD_MARKER_MODE];
//    [self.pointcloudViewController.annotationView resetSelection];
    [self.pointcloudViewController.annotationView setNeedsDisplay];
}

#pragma mark - Navigation Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"addNewScan"]) {
     
        // create scan, pass the scan to the new model 
        RSScanViewController *viewController = (RSScanViewController *)segue.destinationViewController;
//        RSScanViewController *viewController = (RSScanViewController *)[segue.destinationViewController topViewController];
        
        int numberOfScans = (int) [self.viewModel numberOfScans];
        [self.viewModel addScan:[NSString stringWithFormat:@"%04d", numberOfScans+1]];
        
        RSScanViewModel * comm = [self.viewModel scanAtIndex:numberOfScans];
        viewController.viewModel = comm;
        
    } else if ([segue.identifier isEqualToString:@"embedPointcloud"]) {
        
        // Bind the PCGL
        self.pointcloudViewController = segue.destinationViewController;
        self.pointcloudViewController.viewModel = self.viewModel;
    
    } else if ([segue.identifier isEqualToString:@"embedLeftTable"]) {

        self.selectionTableViewController = (LeftTableViewController *) [segue.destinationViewController topViewController];
        self.selectionTableViewController.viewModel = self.viewModel;
        
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return (self.editing == NO);
}

-(void)selectScanAtIndex:(int) index {
    [self.pointcloudViewController selectScanAtIndex:index];
}

- (void) reloadData:(NSInteger) index {
    [self.selectionTableViewController reloadData:index];
}

- (void) refreshDrawing {
    [self.pointcloudViewController.annotationView setNeedsDisplay];
}

- (void) setAnnotationView:(BOOL) isShouldShow {
    [self.pointcloudViewController.annotationView setHidden:!isShouldShow];
}

@end
