//
//  APIClientManager.h
//  RoomScan
//
//  Created by Finde Xumara on 22/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZipService.h"

#import "EnvironmentService.h"

@interface APIClientManager : NSObject

@property (strong, nonatomic) EnvironmentService * environmentService;

@end
