//
//  APIClient+Edits.h
//  ServerCommunication
//
//  Created by Daniel Hallin on 18/05/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "APIClient.h"

@interface APIClient (Edits)

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                 URIparameters:(id)parameters
     constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                      progress:(void (^)(NSProgress *))uploadProgress
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

@end
