//
//  RSAnnotationViewModel.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 23/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RSAnnotationViewModel.h"
#import "RSPointViewModel.h"

#import "RSAnnotation.h"
#import "RSPoint.h"

@interface RSAnnotationViewModel ()
    @property (nonatomic, strong) RSAnnotation *model;
    @property (nonatomic, strong) NSMutableArray *lines;
    @property (nonatomic) BOOL isNew;
@end

@implementation RSAnnotationViewModel

- (instancetype)initWithModel:(id)model {
    self = [super initWithAPIManager];
    
    self.model = model;
    self.isSelected = NO;
    
    if (self == nil) return nil;
    
    RAC(self, annotationName) = [RACObserve(self.model, annotationName) map:^id(NSString *value) {
        return value;
    }];
    
    RAC(self, annotationId) = [RACObserve(self.model, annotationId) map:^id(NSString *value) {
        return value;
    }];

    RAC(self, isTopBottomSplitted) = [RACObserve(self.model, isTopBottomSplitted) map:^id(NSNumber *value) {
        return value;
    }];
    
    self.isNew = YES;
    self.lines = [self getLines];
    
    return self;
}

- (RSPointViewModel *) pointAtIndex: (NSInteger) index {
    RSPointViewModel *point = [[RSPointViewModel alloc] initWithModel:[self.model.points objectAtIndex:index]];
    return point;
}

- (NSMutableArray *) getLines {
    
    if (!self.isNew) {
        return self.lines;
    }
    
    NSMutableArray *lines = [[NSMutableArray alloc] init];
    
    int n_points = [self.isTopBottomSplitted boolValue] ? 2 : 1;
    for (int i=0; i<n_points; i++) {
        NSMutableArray *points = [[NSMutableArray alloc] init];
        
        for (int j=0; j<3; j++) {
            [points addObject:[self pointAtIndex:i*3+j]];
        }
        
        [lines addObject:points];
    }
    
    self.isNew = NO;
    return lines;
}

- (void) setFirstLineEndPointTo:(CGPoint) location {
    // bottom
    RSPointViewModel *endPoint = [self pointAtIndex:1];
    [endPoint setPositionWithCGPoint:location];
    
    self.isNew = YES;
}

- (RSPointViewModel *) closestPointToLocation:(GLKVector2) vectLocation InRadius:(float) pointSize isEdgeOnly:(BOOL)onlyEdge {
    float bestDistanceSq = INFINITY;
    RSPointViewModel *bestPoint = nil;
    
    for (NSMutableArray *line in self.lines) {
        for (RSPointViewModel *point in line) {
            
            if (onlyEdge and ![point.isEdge boolValue]) {
                continue;
            }

            // exclude edge only or not
            float distanceSq = [point hitSquareDistance:vectLocation];
            if (distanceSq < bestDistanceSq) {
                bestPoint = point;
                bestDistanceSq = distanceSq;
            }
            
        }
    }
    
    return (bestDistanceSq < pointSize * pointSize) ? bestPoint : nil;
}

- (double) topBottomLength {
    if ([self.model.isTopBottomSplitted boolValue]) {
        RSPointViewModel *bottomPoint = self.lines[0][0];
        RSPointViewModel *topPoint = self.lines[1][0];
        
        return GLKVector2Distance([bottomPoint getVector],[topPoint getVector]);
    } else {
        return 0;
    }
    
}

- (void) setTopBottomDistance:(double) distance {
    
    if (fabs(distance) > 0.15 and [self.lines count] == 1) {
        self.model.isTopBottomSplitted = [NSNumber numberWithBool:YES];
        self.isNew = YES;
        self.lines = [self getLines];
    } else if (fabs(distance) <= 0.15 and [self.lines count] > 1){
        self.model.isTopBottomSplitted = [NSNumber numberWithBool:NO];
        self.isNew = YES;
        self.lines = [self getLines];
    }
    
    if ([self.model.isTopBottomSplitted boolValue]) {
        // calculate the top part from
        RSPointViewModel *bottomStartPoint = self.lines[0][0];
        RSPointViewModel *bottomEndPoint = self.lines[0][1];
        
        GLKVector2 bottomVector = GLKVector2Make([bottomStartPoint.x doubleValue] - [bottomEndPoint.x doubleValue],
                                                 [bottomStartPoint.y doubleValue] - [bottomEndPoint.y doubleValue]);
        GLKVector2 distanceVector = GLKVector2Normalize(PERP1(bottomVector));
        distanceVector = GLKVector2MultiplyScalar(distanceVector, distance);
        
        for (int i=0, limit=[self.lines[1] count]; i<limit; i++) {
            RSPointViewModel * pointT = self.lines[0][i];
            RSPointViewModel * pointB = self.lines[1][i];
            [pointB setPositionWithVector:GLKVector2Add([pointT getVector], distanceVector)];
        }
    }
}

@end
