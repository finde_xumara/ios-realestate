//
//  RSGalleryViewCell.m
//  RoomScan
//
//  Created by Finde Xumara on 07/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RSGalleryViewCell.h"

#import "RSEnvironmentViewModel.h"
#import "EnvironmentMTL.h"

@interface RSGalleryViewCell ()
    @property BOOL showFront;
@end

@implementation RSGalleryViewCell

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    _showFront = TRUE;
    _isSelected = FALSE;
    
    self.spinner.hidden = TRUE;
    self.highlightLayer.hidden = TRUE;
    self.frontCard.hidden = FALSE;
    self.backCard.hidden = TRUE;
    
    return self;
}


- (BOOL) isFront {
    return _showFront;
}

- (void) flip {
    
    _showFront = !_showFront;
    
    self.frontCard.hidden = !self.frontCard.hidden;
    self.backCard.hidden = !self.backCard.hidden;
}

@end
