//
//  RSEditEnvironmentViewModel.m
//  RoomScan
//
//  Created by Finde Xumara on 07/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

// View Models
#import "RSEnvironmentViewModel.h"
#import "RSScanViewModel.h"
#import "RSAnnotationViewModel.h"
#import "RSPointViewModel.h"

// Models
#import "RSEnvironment.h"
#import "RSScan.h"
#import "RSAnnotation.h"
#import "RSPoint.h"

@interface RSEnvironmentViewModel ()

@property (nonatomic, strong) RSEnvironment *model;
@property (nonatomic, strong) NSMutableArray *markers;

@end

@implementation RSEnvironmentViewModel

-(instancetype)initWithModel:(id)model {
    self = [super initWithAPIManager];
    self.model = model;
    
    if (self == nil) return nil;
    
    RAC(self, environmentName) = [RACObserve(self.model, environmentName) map:^id(NSString *value) {
        return value;
    }];
    
    RAC(self, environmentId) = [RACObserve(self.model, environmentId) map:^id(NSString *value) {
        return value;
    }];
    
    RAC(self, serverId) = [RACObserve(self.model, serverId) map:^id(NSString *value) {
        return value;
    }];
    
    //    RACChannelTo(self, environmentName) = RACChannelTo(self.model, environmentName);
    [self setupMarkers];
    
    return self;
}

#pragma mark - Public Methods
-(void)cancel {
    if (self.inserting) {
        [self.model.managedObjectContext deleteObject:self.model];
    }
}

-(void)willDismiss {
    self.model.modifiedAt = [NSDate date];
    [self.model.managedObjectContext save:nil];
}


-(NSURL *) environmentPath {
    return [self.model getPath];
}

-(void) updateServerId:(NSString *)serverId {
    self.model.serverId = serverId;
    self.model.modifiedAt = [NSDate date];
    [self.model.managedObjectContext save:nil];
}

-(RACSignal *) uploadSignal {
    return [RVMViewModel.apiManager.environmentService fetchEnvironment:(RSEnvironmentViewModel *) self];
}


#pragma mark - Scans
-(NSInteger)numberOfScans {
    return self.model.scans.count;
}

-(RSScanViewModel *) scanAtIndex: (NSInteger) index{
    
    RSScanViewModel *viewModel = [[RSScanViewModel alloc] initWithModel:[self.model.scans objectAtIndex:index]];
    
    return viewModel;
}

-(RSScan *) scanAtIndexPath: (NSInteger) index {
    return [self.model.scans objectAtIndex:index];
}

-(void)addScan: (NSString *) scanName {
    RSScan *scan = [NSEntityDescription insertNewObjectForEntityForName:@"RSScan" inManagedObjectContext:self.model.managedObjectContext];
    
    scan.scanName = scanName; // TODO should check the last scan and increase by 1
    scan.scanId = [[NSUUID UUID] UUIDString];
    scan.environmentObj = self.model;
    scan.createdAt = [NSDate date];
    scan.modifiedAt = [NSDate date];
    
    NSMutableOrderedSet *scanMutableOrderedSet = [self.model.scans mutableCopy];
    [scanMutableOrderedSet addObject:scan];
    
    self.model.scans = [scanMutableOrderedSet copy];
    
    [scan prepareSetting];
}

-(void)removeScanAtIndex:(NSInteger)index {
    NSMutableOrderedSet *scanMutableOrderedSet = [self.model.scans mutableCopy];
    
    NSAssert(index < scanMutableOrderedSet.count && index >= 0, @"Index must be within bounds of mutable set.");
    
    RSScanViewModel *viewModel = [self scanAtIndex:index];
    [viewModel cancel];
    
    [scanMutableOrderedSet removeObjectAtIndex:index];
    self.model.scans = [scanMutableOrderedSet copy];
    [self.model.managedObjectContext save:nil];
}

-(NSString *) scanTitleAtIndexPath: (NSInteger) index {
    return [[self scanAtIndexPath:index] scanName];
}


#pragma mark - Annotations
-(void) setupMarkers {
    if([self.markers count]){
        [self.markers removeAllObjects];
    }
    
    self.markers = [[NSMutableArray alloc] init];
    
    for (int i=0, limit=[self numberOfAnnotations]; i<limit; i++) {
        RSAnnotationViewModel *marker = [[RSAnnotationViewModel alloc] initWithModel:[self.model.annotations objectAtIndex:i]];
        [self.markers addObject:marker];
    }
}

-(NSInteger)numberOfAnnotations {
    return self.model.annotations.count;
}

-(void)addAnnotationWithOptions:(NSDictionary *)options {
    
    int newName = 1;
    
    if( [self numberOfAnnotations] > 0) {
        // get last annotation
        RSAnnotation *annotation = [self.model.annotations objectAtIndex:[self numberOfAnnotations]-1];
    
        //get number, >> increment 1
        newName = [[annotation annotationName] intValue] + 1;
    }
    
    NSString * annotationName = [NSString stringWithFormat:@"%04d", newName];
    
    RSAnnotation *annotation = [NSEntityDescription insertNewObjectForEntityForName:@"RSAnnotation" inManagedObjectContext:self.model.managedObjectContext];
    
    annotation.annotationName = annotationName;
    annotation.annotationId = [[NSUUID UUID] UUIDString];
    annotation.environmentObj = self.model;
    annotation.createdAt = [NSDate date];
    annotation.modifiedAt = [NSDate date];
    annotation.isTopBottomSplitted = [NSNumber numberWithBool:NO];
    [annotation setupPointsWithOptions:options];
    
    NSMutableOrderedSet *annotationMutableOrderedSet = [self.model.annotations mutableCopy];
    [annotationMutableOrderedSet addObject:annotation];
    
    self.model.annotations = [annotationMutableOrderedSet copy];
    
    [self.markers addObject:[[RSAnnotationViewModel alloc] initWithModel:annotation]];
}

-(RSAnnotationViewModel *) lastCreatedAnnotation {
    return [self.markers objectAtIndex:[self numberOfAnnotations]-1];
}

-(RSAnnotationViewModel *) annotationAtIndex: (NSInteger) index{
    return [self.markers objectAtIndex:index];
}

-(NSString *) annotationTitleAtIndexPath: (NSInteger) index {
    return [[self annotationAtIndex:index] annotationName];
}

-(void)removeAnnotationAtIndex:(NSInteger)index {
    NSMutableOrderedSet *annotationMutableOrderedSet = [self.model.annotations mutableCopy];
    
    NSAssert(index < annotationMutableOrderedSet.count && index >= 0, @"Index must be within bounds of mutable set.");
    [annotationMutableOrderedSet removeObjectAtIndex:index];
    
    self.model.annotations = [annotationMutableOrderedSet copy];
    
    [self.markers removeObjectAtIndex:index];
}

- (NSMutableArray *) getAnnotationsMeta {
    NSMutableArray *annotations = [NSMutableArray new];
    for (int i=0, limit_i=[self numberOfAnnotations]; i<limit_i; i++) {
        
        RSAnnotationViewModel *annotation = [self annotationAtIndex:i];
        
        NSMutableArray *lines = [annotation getLines];
        
        NSMutableArray *pointsOneAnnotation = [NSMutableArray new];
        
        for (int l=0, limit_l=[lines count]; l<limit_l; l++) {
            // generate string from RSAnnotationViewModel
            
            for (RSPointViewModel *point in lines[l]){
                [pointsOneAnnotation addObject:point.x];
                [pointsOneAnnotation addObject:point.y];
            }
            
            [annotations addObject:pointsOneAnnotation];
        }
    }
    return annotations;
}

- (NSMutableArray *) getAlignmentsMeta {
    NSMutableArray *alignments = [NSMutableArray new];
    for (int i=0, limit_i=[self numberOfScans]; i<limit_i; i++) {
        
        RSScanViewModel *scanModel = [self scanAtIndex:i];
        [alignments addObject:[scanModel getAlignmentMeta]];
    }
    return alignments;
}

@end
