//
//  3dfPly.c
//  ios-RoomScan
//
//  Created by Finde Xumara on 04/11/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "Ply3dfHelper.h"
#import <pcl/point_cloud.h>
#import <pcl/io/ply_io.h>
#import <pcl/filters/voxel_grid.h>
#import <pcl/filters/passthrough.h>
#import "PCLHelper.h"
#import "CommonMath.h"
#import <vector>
#import "Frame3D.h"
#import <opencv2/imgproc.hpp>

@implementation Ply3dfHelper
+ (NSArray *) getFrame3DFiles:(NSString *)dir scanName:(NSString *)scan_name {
    NSArray * dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/scans/%@", dir, scan_name]]
                                                          includingPropertiesForKeys:@[]
                                                                             options:NSDirectoryEnumerationSkipsHiddenFiles
                                                                               error:nil];
    NSArray * frame3DFiles = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"pathExtension='3df'"]];
    
    NSMutableArray * temp = [NSMutableArray arrayWithArray:frame3DFiles];
    
    [temp sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
     NSString *f1 = [(NSURL *)obj1 absoluteString];
     NSString *f2 = [(NSURL *)obj2 absoluteString];
     
     return [f1 localizedStandardCompare:f2];
     }];
    
    return [temp copy];
}

+ (void) createPLYFileFrom3DFDir:(NSString *)dir scanName:(NSString *)scan_name {
    
    NSArray * frame3DFiles = [self getFrame3DFiles:dir scanName:scan_name];
    
    pcl::VoxelGrid<pcl::PointXYZRGBNormal> vg;
    
    float leafSize = 0.01f;
    float thresh = 3.0;
    
    float focal_length = 576;
    
    int _width = 640;
    int _height = 480;
    float gb_cor = 0;
    
    if ([frame3DFiles count] > 0) {
        pcl::PointCloudPtr<pcl::PointXYZRGBNormal> cloud_final(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
        
        CFTimeInterval _generateSingleScanTime = CACurrentMediaTime();
        
        int STEP_PER_TILT = (MAX_ROTATION/ROTATE_STEP)+1;
        for (int i = 0; i < STEP_PER_TILT-1; i++)
        {
            pcl::PointCloudPtr<pcl::PointXYZRGBNormal> cloud_tilt(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
            std::vector<std::vector<float>> imu_3;
            std::vector<std::vector<float>> poses_3;
            std::vector<float> poses_imu_3;
            std::vector<pcl::PointCloudPtr<pcl::PointXYZRGBNormal>> clouds_3;
            
            for (int l=0; l<3; l++) {
                int frame_index = i + STEP_PER_TILT * l;
                std::string strPath = std::string([[frame3DFiles[frame_index] path] UTF8String]);
                
                pcl::PointCloudPtr<pcl::PointXYZRGBNormal> frame_cloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
                
                // do something with object
                Frame3D frame(strPath);
                
                NSString *plyPath = [[frame3DFiles[frame_index] path] stringByReplacingOccurrencesOfString:@"3df"
                                                                                                withString:@"ply"];
                if (![[NSFileManager defaultManager] fileExistsAtPath:plyPath]){
                    
                    std::string plyPath_string = std::string([plyPath UTF8String]);
                    
                    // load ply
                    pcl::io::loadPLYFile(plyPath_string, *frame_cloud);
                    
                } else {
                    
                    cv::Mat depth = cv::Mat::zeros(_height, _width, CV_16U);
                    cv::Mat color = cv::Mat::zeros(_height, _width, CV_8UC3);
                    
                    // ensure the size
                    cv::resize(frame.rgb_image_, color, color.size(), 0, 0, cv::INTER_CUBIC);
                    cv::resize(frame.depth_image_, depth, depth.size(), 0, 0, cv::INTER_CUBIC);
                    
                    depth2Pointcloud(depth, color, frame_cloud, focal_length, thresh);
                }
                
                // load frame_cloud
                
                clouds_3.push_back(frame_cloud);
                
                float pan = frame.transformation_.at<float>(3, 0);
                float tilt = frame.transformation_.at<float>(3, 1);
                float middle = frame.transformation_.at<float>(3, 2);
                
                // roll and pitch is swapped because the ipad is horizontal
                float yaw = frame.transformation_.at<float>(0, 3);
                float roll = frame.transformation_.at<float>(1, 3);
                float pitch = frame.transformation_.at<float>(2, 3);
                
                std::vector<float> device;
                device.push_back(tilt);
                device.push_back(pan);
                device.push_back(middle);
                
                poses_3.push_back(device);
                
                
                std::vector<float> imu;
                imu.push_back(yaw);
                imu.push_back(pitch);
                imu.push_back(roll);
                
                imu_3.push_back(imu);
                
                if (i == 0){
                    gb_cor = (poses_3[0][1] * 360 / 16383);
                }
                
//                poses_imu_3.push_back(roll);
            }
            
            //            median(poses_imu_3, rot_z);
            //            poses_imu_3.clear();
            
            for (int l=0; l<3; l++) {
                float pan = poses_3[l][1];
                float rot_z = imu_3[l][2];
                float rot_x = (imu_3[l][1] - 90) * M_PI / 180;
                float rot_y = -(pan * 360 / 16383 - gb_cor)*M_PI / 180;
                
                float xoffset, yoffset, zoffset;
                findTranslation(rot_x, rot_y, xoffset, yoffset, zoffset);
                
                Eigen::Affine3f R;
                composeRotation(rot_x, rot_y, (M_PI / 180)*(rot_z + 1), R);
                
                Eigen::Affine3f tform;
                tform = R;
                tform(0, 3) = -zoffset;
                tform(1, 3) = -yoffset;
                tform(2, 3) = xoffset;
                pcl::transformPointCloudWithNormals(*clouds_3[l], *clouds_3[l], tform);
                
                *cloud_tilt += *clouds_3[l];
            }
            
            //TODO::clear
            
            // voxel filter
            pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud_tilt_filt(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
            vg.setLeafSize(leafSize, leafSize, leafSize);
            vg.setInputCloud(cloud_tilt);
            vg.filter(*cloud_tilt_filt);
            *cloud_final += *cloud_tilt_filt;
        }
        
        vg.setLeafSize(leafSize, leafSize, leafSize);
        vg.setInputCloud(cloud_final);
        vg.filter(*cloud_final);
        
        NSLog(@">> Generate single scan ply: %g", CACurrentMediaTime() - _generateSingleScanTime);
        pcl::io::savePLYFileBinary(std::string([[NSString stringWithFormat:@"%@/scans/%@.ply", dir, scan_name] UTF8String]), *cloud_final);
        
        // remove ceiling
        pcl::PassThrough<pcl::PointXYZRGBNormal> pass_through;
        pass_through.setInputCloud (cloud_final);
        pass_through.setFilterFieldName ("y");
        pass_through.setFilterLimits (-10.0, 0.50); // from 50cm above camera to max 10m from camera to the ground
        pass_through.filter (*cloud_final);
        
        // filter the point to get the top view points only
        float leafSizeViz = 0.05f;
        vg.setLeafSize(leafSizeViz, leafSizeViz, leafSizeViz);
        vg.setInputCloud(cloud_final);
        vg.filter(*cloud_final);
        filterTopView(cloud_final, leafSizeViz);
        
        pcl::io::savePLYFileBinary(std::string([[NSString stringWithFormat:@"%@/scans/%@-viz.ply", dir, scan_name] UTF8String]), *cloud_final);
    }
}
@end
