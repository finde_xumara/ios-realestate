//
//  LineView.m
//  RoomScan
//
//  Created by Finde Xumara on 15/05/16.
//  Copyright © 2016 Occipital. All rights reserved.
//

#import "AnnotationView.h"

#import "RSEnvironmentViewModel.h"
#import "RSAnnotationViewModel.h"
#import "RSPointViewModel.h"

#import "RSAnnotation.h"
#import "RSPoint.h"

@implementation AnnotationView

- (NSDictionary *) getSettings {
    return  @{@"Width":@(_width),
              @"Height":@(_height),
              @"Scale":@(_scale),
              @"Center":[NSValue valueWithCGPoint:_centerAnnotation]};
}

-(instancetype) init {
    self = [super init];
    
    _centerAnnotation = VTP(VEC0);
    
    return self;
}


#pragma mark - Update Center
- (void) updateCenterTo: (CGPoint) location {
    _centerAnnotation = location;
    [self setNeedsDisplay];
}

#pragma mark - Update Scale

-(void) prepareToScale {
}

- (void) updateScaleTo: (float) newScale {
    _scale = newScale;
}

- (void) finalizeScaling {
    
}

#pragma mark - Drawing
- (void) drawLineBetweenPoint:(RSAnnotationViewModel *) marker {
    
    NSMutableArray *lines = [marker getLines];
    
    GLKVector2 nextPoint_position;

    UIBezierPath *aPath = [UIBezierPath bezierPath];
    
    for (int i=0, limit=[lines count]; i<limit; i++) {
        NSMutableArray *points = lines[i];
        
        GLKVector2 startPoint_position = [MathHelper convertRSPointToViewSpace:points[0] withOptions:[self getSettings]];
        GLKVector2 endPoint_position = [MathHelper convertRSPointToViewSpace:points[1] withOptions:[self getSettings]];
        GLKVector2 arcPoint_position = [MathHelper convertRSPointToViewSpace:points[2] withOptions:[self getSettings]];
        
        if (i>0) {
            startPoint_position = [MathHelper convertRSPointToViewSpace:points[1] withOptions:[self getSettings]];
            endPoint_position = [MathHelper convertRSPointToViewSpace:points[0] withOptions:[self getSettings]];
            arcPoint_position = [MathHelper convertRSPointToViewSpace:points[2] withOptions:[self getSettings]];
        }
        
        if (i==0)
            [aPath moveToPoint:VTP(startPoint_position)];
        [aPath addQuadCurveToPoint:VTP(endPoint_position) controlPoint:VTP(arcPoint_position)];
        
        if (limit > 1) {
            if (i==0) {
                NSMutableArray *nextLinePoints = lines[1];
                nextPoint_position = [MathHelper convertRSPointToViewSpace:nextLinePoints[1] withOptions:[self getSettings]];
            } else if (i==1) {
                NSMutableArray *nextLinePoints = lines[0];
                nextPoint_position = [MathHelper convertRSPointToViewSpace:nextLinePoints[0] withOptions:[self getSettings]];
            }
            
            [aPath addLineToPoint:VTP(nextPoint_position)];
        }
    }
    
    if (marker.isSelected) {
        [[[UIColor markerSelected] colorWithAlphaComponent:0.5] setStroke];
        [[[UIColor markerSelected] colorWithAlphaComponent:0.2] setFill];
    } else {
        [[[UIColor markerDefault] colorWithAlphaComponent:0.5] setStroke];
        [[[UIColor markerDefault] colorWithAlphaComponent:0.2] setFill];
    }

    
    aPath.lineWidth = 10.0;
    [aPath stroke];
    
    if ([lines count] > 1){
        [aPath closePath];
        [aPath fill];
    }
}

- (void) drawInteractionPoint:(RSAnnotationViewModel *) marker {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    NSMutableArray *lines = [marker getLines];
    
    float radius = 13;
    
    for (int i=0, limit = [lines count]; i<limit; i++){
        NSMutableArray *points = lines[i];
        
        for(RSPoint * point in points) {
            if (!marker.isSelected and ![point.isEdge boolValue]) {
                continue;
            }
            
            if (![point.isEdge boolValue] && i>0)
            {
                continue;
            }
            
            GLKVector2 pointPixelPosition = [MathHelper convertRSPointToViewSpace:point withOptions:[self getSettings]];
            
            CGRect circleRect = CGRectMake(pointPixelPosition.x - radius,
                                           pointPixelPosition.y - radius,
                                           radius * 2, radius * 2);
            
            CGContextBeginPath(ctx);
            CGContextAddEllipseInRect(ctx, circleRect);
            
            if ([point.isEdge boolValue]) {
                CGContextSetStrokeColorWithColor(ctx, [[UIColor pointDefaultStroke] CGColor]);
            } else {
                CGContextSetStrokeColorWithColor(ctx, [[UIColor yellowColor] CGColor]);
            }
            
            CGContextSetFillColorWithColor(ctx, [[UIColor pointDefaultFill] CGColor]);
            CGContextSetLineWidth(ctx, 5.0);
            CGContextDrawPath(ctx, kCGPathFillStroke);
        }
    }
}

- (void) drawTextForLine:(RSAnnotationViewModel *) marker {
//    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetTextDrawingMode(ctx, kCGTextFill);
    NSMutableArray *lines = [marker getLines];

    // put text "T" / "B" fo each point
    for (int i=0, limit = [lines count]; i<limit; i++) {
        NSMutableArray *points = lines[i];
        
        for (RSPoint * point in points) {
            GLKVector2 pointPixelPosition = [MathHelper convertRSPointToViewSpace:point withOptions:[self getSettings]];
            
            if (![point.isEdge boolValue]) {
                continue;
            }
            
            NSString *markerLabel = @"";
            
            if (![marker.isTopBottomSplitted boolValue]) {
                markerLabel = @"A";
            } else {
                if (i == 0) {
                    markerLabel = @"B";
                } else if (i == 1) {
                    markerLabel = @"T";
                }
            }
            
            [markerLabel drawAtPoint:CGPointMake(pointPixelPosition.x - 5, pointPixelPosition.y - 8)
                      withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica"  size:14], NSForegroundColorAttributeName: [UIColor textDefault]}];
        }
    }
    
    //    NSString *length = [NSString stringWithFormat:@"__%@__ :: %f", [self convertMarkerToString:line.markerType], line.length];
    //    [length drawAtPoint:CGPointMake(endPoint_position.x + 20, endPoint_position.y)
    //         withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica"  size:17], NSForegroundColorAttributeName: [UIColor greenColor]}];
}


#pragma mark - Inherit from UIView
- (void)drawRect:(CGRect)rect {
    
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
    for (int i=0, limit=[self.envViewModel numberOfAnnotations]; i<limit; i++) {
        RSAnnotationViewModel *marker = [self.envViewModel annotationAtIndex:i];
        
        [self drawLineBetweenPoint:marker];
        [self drawInteractionPoint:marker];
        [self drawTextForLine:marker];
        
    }

}

@end