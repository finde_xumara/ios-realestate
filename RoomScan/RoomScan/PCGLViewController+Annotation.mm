//
//  PCGLViewController+Annotation.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 19/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "RSEnvironmentViewController.h"
#import "RSEnvironmentViewModel.h"
#import "RSAnnotationViewModel.h"
#import "RSPointViewModel.h"

#import "PCGLViewController.h"
#import "PCGLViewController+Annotation.h"

#import "RSCoreDataStack.h"

@implementation PCGLViewController (Annotation)

RSPointViewModel * selectedPoint;
RSAnnotationViewModel * selectedAnnotation;
UIPinchGestureRecognizer *topBottomSplitter;

float initDistance;

-(void) setupAnnotationView {

    // add gesture to the glkView
    UITapGestureRecognizer *annnotationSelector = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectAnnotationGestureRecognizer:)];
    [self.view addGestureRecognizer:annnotationSelector];
    
    topBottomSplitter = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(splitTopBottomGestureRecognizer:)];
    [self.view addGestureRecognizer:topBottomSplitter];
    topBottomSplitter.enabled = NO;
}

-(void)selectAnnotationGestureRecognizer:(UITapGestureRecognizer *) sender {
    CGPoint location = [sender locationInView:self.view];
    
    // todo , find point that close to this tap location
    selectedAnnotation.isSelected = NO;
    [self selectClosestPoint:location];
    
    if (selectedPoint) {
        selectedAnnotation.isSelected = YES;
        topBottomSplitter.enabled = YES;
        [self setViewMode:ViewMode::EDIT_MARKER_MODE];
    } else {
        topBottomSplitter.enabled = NO;
        [self setViewMode:ViewMode::DEFAULT_MODE];
    }
    
    [self.annotationView setNeedsDisplay];
}


-(void)splitTopBottomGestureRecognizer:(UIPinchGestureRecognizer *) sender {
    
    if (selectedAnnotation) {
        
        switch (((UIGestureRecognizer *) sender).state) {
            case UIGestureRecognizerStateBegan: {
                // store initial state of the marker
                initDistance = [selectedAnnotation topBottomLength];
            } break;
                
            case UIGestureRecognizerStateChanged: {
                
                // calculate the final distance
                initDistance += sender.velocity * 0.25;
                
                // update the annotation
                [selectedAnnotation setTopBottomDistance:initDistance];
                [self.annotationView setNeedsDisplay];
                
            } break;
                
            case UIGestureRecognizerStateEnded: {
                
                // normalize the marker state
                initDistance = 0;
                
            } break;
                
            default:
                break;
        }
    }
    
}

-(void) handleEditMarkerMode:(CGPoint) location sender:(id) sender {
    
    if (selectedAnnotation) {
        switch (((UIGestureRecognizer *) sender).state) {
            case UIGestureRecognizerStateBegan: {
                RSPointViewModel *closestPointLine = [self findClosestPoint:location ofAnnotation:selectedAnnotation isEdgeOnly:NO];
                
                if (closestPointLine) {
                    selectedPoint = closestPointLine;
                } else {
                    ((UIGestureRecognizer *) sender).enabled = NO;
                    ((UIGestureRecognizer *) sender).enabled = YES;
                }
            } break;
                
            case UIGestureRecognizerStateChanged: {
                GLKVector2 vectLocation = [MathHelper convertTo3DSpaceWithCGPoint:location withOptions:[self.annotationView getSettings]];
                [selectedPoint setPositionWithVector:vectLocation];
                [self.annotationView setNeedsDisplay];
            } break;
                
            case UIGestureRecognizerStateEnded: {
//                [self clearSelection];
            } break;
                
            default:
                break;
        }
    }
}

-(void) handleAddMarkerMode:(CGPoint)location state:(UIGestureRecognizerState)state {
    
    
    if (state == UIGestureRecognizerStateBegan) {
        [(RSEnvironmentViewController *) self.parentViewController reloadData: 1];
        [self addAnnotationWithLocation:location];

    } else if (state == UIGestureRecognizerStateChanged) {        
        [self updateLastLineEndTo:location];
        
        
    } else if (state == UIGestureRecognizerStateEnded) {
        
        // todo add annotations as object
        //        self.viewMode = DEFAULT_MODE;
        //                _viewMode = EDIT_MODE;
        
        // set selected to "Annotations"
        [(RSEnvironmentViewController *) self.parentViewController reloadData: 1];
        
        [self updateUIComponent];
        
    }
    
}

#pragma mark - private methods

-(void) addAnnotationWithLocation:(CGPoint) location {
    GLKVector2 vec2Location = [MathHelper convertTo3DSpaceWithCGPoint:location withOptions:[self.annotationView getSettings]];
    [self.viewModel addAnnotationWithOptions: @{@"from":[NSValue valueWithCGPoint:VTP(vec2Location)],
                                                @"to":[NSValue valueWithCGPoint:VTP(vec2Location)],
                                                @"type":@"WALL"}];
}

-(void) updateLastLineEndTo:(CGPoint) location {
    // get last marker, set end to
    RSAnnotationViewModel *annotation = [self.viewModel lastCreatedAnnotation];
    GLKVector2 vec2Location = [MathHelper convertTo3DSpaceWithCGPoint:location withOptions:[self.annotationView getSettings]];
    [annotation setFirstLineEndPointTo:VTP(vec2Location)];
    
    [self.annotationView setNeedsDisplay];
}

-(RSPointViewModel *) findClosestPoint:(CGPoint) tapLocation ofAnnotation:(RSAnnotationViewModel *)annotation {
    return [self findClosestPoint:tapLocation ofAnnotation:annotation isEdgeOnly:YES];
}

-(RSPointViewModel *) findClosestPoint:(CGPoint) tapLocation ofAnnotation:(RSAnnotationViewModel *)annotation isEdgeOnly:(BOOL)onlyEdge {
    GLKVector2 vectLocation = [MathHelper convertTo3DSpaceWithCGPoint:tapLocation withOptions:[self.annotationView getSettings]];
    RSPointViewModel *closestPointLine = [annotation closestPointToLocation:vectLocation InRadius:0.5 isEdgeOnly:onlyEdge];
    return closestPointLine;
}

- (void) clearSelection {
    selectedPoint = nil;
    selectedAnnotation = nil;
}

-(void) selectClosestPoint:(CGPoint) tapLocation {
    float minDistance = INFINITY;
    [self clearSelection];
    
    // for each markers
    for (int i=0, limit=[self.viewModel numberOfAnnotations]; i<limit; i++) {
        RSAnnotationViewModel *marker = [self.viewModel annotationAtIndex:i];
        RSPointViewModel *closestPointLine = [self findClosestPoint:tapLocation ofAnnotation:marker];
        
        GLKVector2 vectLocation = [MathHelper convertTo3DSpaceWithCGPoint:tapLocation withOptions:[self.annotationView getSettings]];
        float current_distance = [closestPointLine hitSquareDistance:vectLocation];
        
        if (closestPointLine && minDistance > current_distance) {
            minDistance = current_distance;
            selectedPoint = closestPointLine;
            selectedAnnotation = marker;
        }
    }
    
}

@end