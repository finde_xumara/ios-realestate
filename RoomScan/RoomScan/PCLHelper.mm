    //
//  PCLHelper.m
//  ios-RoomScan
//
//  Created by Finde Xumara on 17/08/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import "PCLHelper.h"
#import <iostream>
#import <map>
#import <string>
#import <pcl/point_cloud.h>
#import <pcl/ModelCoefficients.h>
#import <pcl/io/ply_io.h>
#import <pcl/features/integral_image_normal.h>
#import <pcl/filters/project_inliers.h>
#import <pcl/filters/statistical_outlier_removal.h>
#import <pcl/filters/voxel_grid.h>
#import <pcl/filters/passthrough.h>

#import "Frame3D.h"
#import <pcl/io/obj_io.h>
#import <pcl/common/transforms.h>
#import <pcl/common/common.h>
#import <pcl/common/io.h>
#import <boost/thread/thread.hpp>
#import <pcl/range_image/range_image.h>
#import <pcl/io/pcd_io.h>
#import <pcl/kdtree/kdtree_flann.h>
#import <pcl/filters/filter_indices.h>
#import <pcl/filters/passthrough.h>
#import <pcl/filters/voxel_grid.h>
#import <pcl/features/normal_3d.h>
#import <pcl/features/fpfh.h>
#import <pcl/registration/transformation_estimation_svd.h>
//#import <pcl/correspondence.h>
//#import <pcl/registration/correspondence_estimation.h>
//#import <pcl/registration/correspondence_rejection_sample_consensus.h>
#import <pcl/features/range_image_border_extractor.h>
#import <pcl/keypoints/narf_keypoint.h>
#import <pcl/registration/gicp.h>

using namespace pcl;
using namespace cv;

@implementation PCLHelper

void depth2Pointcloud(cv::Mat& depth, cv::Mat& color, pcl::PointCloudPtr<pcl::PointXYZRGBNormal>& pointcloud, float focal_length, float thresh)
{
    cv::Mat color2;
    cv::resize(color, color2, depth.size());
    int height = depth.rows;
    int width = depth.cols;
    int halfheight = height / 2;
    int halfwidth = width / 2;
    thresh *= 1000.f;
    
    pcl::PointCloudPtr<pcl::PointXYZRGB> tempPointcloud(new pcl::PointCloud<pcl::PointXYZRGB>(depth.cols, depth.rows));
    
    float finv = 1.0 / focal_length;
    
    int heightCutOff = ceil(0.04 * height);
    int widthCutOff = ceil(0.08 * width);
    
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            ushort zval = depth.at<ushort>(i, j);
            pcl::PointXYZRGB p;
            cv::Vec3b bgr = color2.at<cv::Vec3b>(i, j);
            
            if (zval > 0 && zval < thresh &&
                j > widthCutOff && j < width - widthCutOff &&
                i > heightCutOff && i < height - heightCutOff
                ) {
                p.r = bgr.val[0];
                p.g = bgr.val[1];
                p.b = bgr.val[2];
                
                p.z = zval / 1000.f;
                p.x = (j - halfwidth) * p.z * finv;
                p.y = (i - halfheight) * p.z * finv;
            } else {
                p.x = 0;
                p.y = 0;
                p.z = 0;
                p.r = 0;
                p.g = 0;
                p.b = 0;
            }
            tempPointcloud->at(j, i) = p;
        }
    }
    
    // normal
    pcl::IntegralImageNormalEstimation<pcl::PointXYZRGB, pcl::PointNormal> ne;
    ne.setNormalEstimationMethod(ne.AVERAGE_3D_GRADIENT);
    ne.setMaxDepthChangeFactor(0.02f);
    ne.setNormalSmoothingSize(10.0f);
    
    pcl::PointCloudPtr<pcl::PointNormal> tmp(new pcl::PointCloud<pcl::PointNormal>(depth.cols, depth.rows));
    ne.setInputCloud(tempPointcloud);
    ne.compute(*tmp);
    pcl::copyPointCloud(*tempPointcloud, *tmp);
    pointcloud->points.clear();
    
    // 4 times subsampling
    int subsampling = 8;
    for (int i = 0; i < height; i+=subsampling)
    {
        for (int j = 0; j < width; j+=subsampling)
        {
            pcl::PointXYZRGB _p = tempPointcloud->at(j, i);
            pcl::PointNormal _pn = tmp->at(j,i);
            pcl::PointXYZRGBNormal p;
            
            p.x = _p.x;
            p.y = _p.y;
            p.z = _p.z;
            p.r = _p.r;
            p.g = _p.g;
            p.b = _p.b;
            p.normal_x = _pn.normal_x;
            p.normal_y = _pn.normal_y;
            p.normal_z = _pn.normal_z;
            
            if ((p.x != 0 || p.y != 0 || p.z != 0) && pcl::isFinite(p))
            {
                pointcloud->points.push_back(p);
            }
        }
    }
    pointcloud->width = pointcloud->points.size();
    pointcloud->height = 1;
}

void filterTopView (pcl::PointCloudPtr<pcl::PointXYZRGBNormal> &pointcloud,
                    float leafSize/*=0.01f*/)
{
    // create catalog to store xyz,rgb,normals
    std::map<std::string, pcl::PointXYZRGBNormal> pointCatalog;
    
    // foreach point in pointcloud->points
    for(pcl::PointXYZRGBNormal const &point: pointcloud->points) {
        int x = ceil(point.x / leafSize);
        int z = ceil(point.z / leafSize);
        std::string encodedLocation = std::to_string(x)+ "_" + std::to_string(z);
        
        if(pointCatalog.find(encodedLocation) != pointCatalog.end()) {
            if (pointCatalog[encodedLocation].y < point.y) {
                pointCatalog[encodedLocation] = point;
            }
        }
        else {
            pointCatalog.insert(std::make_pair(encodedLocation , point));
        }
    }
    
    pointcloud->clear();
    typedef std::map<std::string, pcl::PointXYZRGBNormal>::const_iterator MapIterator;
    for (MapIterator iter = pointCatalog.begin(); iter != pointCatalog.end(); iter++)
    {
        pointcloud->points.push_back(iter->second);
        
    }
}

///// new
bool isCandidate(pcl::PointXYZ& p1, pcl::PointXYZ& p2)
{
    return abs(p1.y - p2.y) < 0.05;
    //return true;
}

float FPFHSigDistance(pcl::FPFHSignature33& f0, pcl::FPFHSignature33& f1)
{
    int size = f0.descriptorSize();
    float dist;
    for (int i = 0; i < size; i++)
    {
        dist += (f0.histogram[i] - f1.histogram[i])*(f0.histogram[i] - f1.histogram[i]);
    }
    dist = sqrt(dist);
    return dist;
}

void getIndex(pcl::PointCloudPtr<pcl::PointXYZ> &points,
              pcl::PointCloudPtr<pcl::PointXYZ> &keypoints, std::vector<int> &index)
{
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
    kdtree.setInputCloud(points);
    int K = 1;
    index.resize(keypoints->points.size());
    std::vector<int> pointIdxNKNSearch(K);
    std::vector<float> pointNKNSquaredDistance(K);
    for (size_t i = 0; i<keypoints->points.size(); i++){
        kdtree.nearestKSearch(keypoints->points[i], K, pointIdxNKNSearch, pointNKNSquaredDistance);
        index[i] = pointIdxNKNSearch[0];
    }
}

void find_feature_correspondences(pcl::PointCloudPtr<pcl::PointXYZ>& keypoints0,
                                  pcl::PointCloudPtr<pcl::FPFHSignature33> &source_descriptors,
                                  pcl::PointCloudPtr<pcl::PointXYZ>& keypoints1,
                                  pcl::PointCloudPtr<pcl::FPFHSignature33> &target_descriptors,
                                  std::vector<int> &correspondences_out, std::vector<float> &correspondence_scores_out)
{
    // Resize the output vector
    //correspondences_out.resize(source_descriptors->size());
    //correspondence_scores_out.resize(source_descriptors->size());
    for (int i = 0; i < source_descriptors->points.size(); i++)
    {
        float score = 99999999;
        float index = -1;
        pcl::PointXYZ p0 = keypoints0->points[i];
        pcl::FPFHSignature33 f0 = source_descriptors->points[i];
        for (int j = 0; j < target_descriptors->points.size(); j++)
        {
            pcl::PointXYZ p1 = keypoints1->points[j];
            pcl::FPFHSignature33 f1 = target_descriptors->points[j];
            if (isCandidate(p0, p1))
            {
                float tmp_score = FPFHSigDistance(f0, f1);
                if (tmp_score < score)
                {
                    score = tmp_score;
                    index = j;
                }
            }
        }
        
        correspondences_out.push_back(index);
        correspondence_scores_out.push_back(score);
        
    }
    
}

void findTranslation(float tilt, float pan, float& xoffset, float& yoffset, float& zoffset)
{
    xoffset = 0.04*cos(tilt + 45 * M_PI / 180)*cos(pan) - 0.04*cos(90 * M_PI / 180 - pan);
    zoffset = 0.04*cos(tilt + 45 * M_PI / 180)*sin(pan) + 0.04*sin(90 * M_PI / 180 - pan);
    yoffset = 0.02*sin(tilt);
    //yoffset =  -0.01*sin(tilt);
}

void composeRotation(float x, float y, float z, Eigen::Affine3f& R)
{
    Eigen::Affine3f X = Eigen::Affine3f::Identity();
    Eigen::Affine3f Y = Eigen::Affine3f::Identity();
    Eigen::Affine3f Z = Eigen::Affine3f::Identity();
    
    X(1, 1) = cos(x);
    X(2, 1) = -sin(x);
    X(1, 2) = sin(x);
    X(2, 2) = cos(x);
    
    Y(0, 0) = cos(z);
    Y(1, 0) = -sin(z);
    Y(0, 1) = sin(z);
    Y(1, 1) = cos(z);
    
    Z(0, 0) = cos(y);
    Z(2, 0) = sin(y);
    Z(0, 2) = -sin(y);
    Z(2, 2) = cos(y);
    //R = Y  Z  X;
    R = Z * Y * X;
    //R = X  Y  Z;
}

void median(std::vector<float>& values, float& val_med)
{
    std::sort(values.begin(), values.end());
    int ind;
    if (values.size() / 2 == 0)
    {
        ind = values.size() / 2;
    }
    else
    {
        ind = values.size() / 2 + 1;
    }
    val_med = values[ind];
}

void rotationXAxis(float Q, Eigen::Affine3f& rotmatrix)
{
    rotmatrix(0, 0) = 1;
    rotmatrix(1, 0) = 0;
    rotmatrix(2, 0) = 0;
    rotmatrix(3, 0) = 0;
    
    rotmatrix(0, 1) = 0;
    rotmatrix(1, 1) = cos(Q);
    rotmatrix(2, 1) = -sin(Q);
    rotmatrix(3, 1) = 0;
    
    rotmatrix(0, 2) = 0;
    rotmatrix(1, 2) = sin(Q);
    rotmatrix(2, 2) = cos(Q);
    rotmatrix(3, 2) = 0;
    
    rotmatrix(0, 3) = 0;
    rotmatrix(1, 3) = 0;
    rotmatrix(2, 3) = 0;
    rotmatrix(3, 3) = 1;
    //[1 0 0 0; 0 cos(Q) - sin(Q) 0; 0 sin(Q) cos(Q) 0; 0 0 0 1]
}

void rotationYAxis(float Q, Eigen::Affine3f& rotmatrix)
{
    rotmatrix(0, 0) = cos(Q);
    rotmatrix(1, 0) = 0;
    rotmatrix(2, 0) = sin(Q);
    rotmatrix(3, 0) = 0;
    
    rotmatrix(0, 1) = 0;
    rotmatrix(1, 1) = 1;
    rotmatrix(2, 1) = 0;
    rotmatrix(3, 1) = 0;
    
    rotmatrix(0, 2) = -sin(Q);
    rotmatrix(1, 2) = 0;
    rotmatrix(2, 2) = cos(Q);
    rotmatrix(3, 2) = 0;
    
    rotmatrix(0, 3) = 0;
    rotmatrix(1, 3) = 0;
    rotmatrix(2, 3) = 0;
    rotmatrix(3, 3) = 1;
    
    //Y(1, 1) = cos(y);
    //Y(1, 3) = sin(y);
    //Y(3, 1) = -sin(y);
    //Y(3, 3) = cos(y);
    //[1 0 0 0; 0 cos(Q) - sin(Q) 0; 0 sin(Q) cos(Q) 0; 0 0 0 1]
}

void rotationZAxis(float Q, Eigen::Affine3f& rotmatrix)
{
    rotmatrix(0, 0) = cos(Q);
    rotmatrix(1, 0) = -sin(Q);
    rotmatrix(2, 0) = 0;
    rotmatrix(3, 0) = 0;
    
    rotmatrix(0, 1) = sin(Q);
    rotmatrix(1, 1) = cos(Q);
    rotmatrix(2, 1) = 0;
    rotmatrix(3, 1) = 0;
    
    rotmatrix(0, 2) = 0;
    rotmatrix(1, 2) = 0;
    rotmatrix(2, 2) = 1;
    rotmatrix(3, 2) = 0;
    
    rotmatrix(0, 3) = 0;
    rotmatrix(1, 3) = 0;
    rotmatrix(2, 3) = 0;
    rotmatrix(3, 3) = 1;
}

void keyPointsDetect(pcl::PointCloudPtr<pcl::PointXYZRGBNormal>& cloud0,
                     pcl::PointCloudPtr<pcl::PointXYZ>& keypoints,
                     float dist_threshold)
{
    float angular_resolution = 0.01f;
    float support_size = 0.2f;
    pcl::RangeImage::CoordinateFrame coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
    bool setUnseenToMaxRange = true;
    float noise_level = 0.0;
    float min_range = 0.0f;
    int border_size = 1;
    boost::shared_ptr<pcl::RangeImage> range_image_ptr0(new pcl::RangeImage);
    pcl::RangeImage& range_image0 = *range_image_ptr0;
    Eigen::Affine3f scene_sensor_pose(Eigen::Affine3f::Identity());
    pcl::PointCloud<pcl::PointWithViewpoint> far_ranges;
    range_image0.createFromPointCloud(*cloud0, angular_resolution,
                                      pcl::deg2rad(360.0f), pcl::deg2rad(180.0f),
                                      scene_sensor_pose, coordinate_frame,
                                      noise_level, min_range, border_size);
    range_image0.integrateFarRanges(far_ranges);
    if (setUnseenToMaxRange)
        range_image0.setUnseenToMaxRange();
    pcl::RangeImageBorderExtractor range_image_border_extractor;
    pcl::NarfKeypoint narf_keypoint_detector(&range_image_border_extractor);
    narf_keypoint_detector.getParameters().support_size = support_size;
    narf_keypoint_detector.getParameters ().add_points_on_straight_edges = false;
    narf_keypoint_detector.getParameters ().distance_for_additional_points = 0.5;
    narf_keypoint_detector.getParameters().calculate_sparse_interest_image = false;
    narf_keypoint_detector.setRangeImage(&range_image0);
    pcl::PointCloud<int> keypoint_indices0;
    narf_keypoint_detector.compute(keypoint_indices0);
    for (size_t i = 0; i<keypoint_indices0.points.size(); ++i)
    {
        pcl::PointXYZ p;
        p.getVector3fMap() = range_image0.points[keypoint_indices0.points[i]].getVector3fMap();
        float dist = sqrt(p.x*p.x + p.y * p.y + p.z * p.z);
        if (dist_threshold <= 0)
        {
            keypoints->points.push_back(p);
        }else
        {
            if (dist < dist_threshold)
            {
                keypoints->points.push_back(p);
            }
        }
    }
}



void pointCloudFilter(pcl::PointCloud<pcl::PointNormal>::Ptr& cloud0, pcl::PointCloud<pcl::PointNormal>::Ptr& cloud1, float dist)
{
    for (int i = 0; i < cloud0->points.size(); i++)
    {
        pcl::PointNormal p = cloud0->points[i];
        float dist1 = sqrt(p.x*p.x + p.y * p.y + p.z * p.z);
        if (dist1 < dist)
        {
            cloud1->points.push_back(p);
        }
    }
    cloud1->width = cloud1->points.size();
    cloud1->height = 1;
}

void mat2Aff(Eigen::Matrix4f& pose, Eigen::Affine3f& pose2)
{
    NSLog(@"==================");
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++) {
            pose2(i, j) = pose(i, j);
            NSLog(@"%f", pose(i, j));
        }
}

int getRandomIndex(int n)
{
    return (static_cast<int> (n * (rand() / (RAND_MAX + 1.0))));
}

void selectSamples(const pcl::PointCloud<pcl::PointXYZ>::Ptr keypoints, int nr_samples, const std::vector<int> correspondences, std::vector<int>& sample_indices)
{
    sample_indices.clear();
    float min_sample_distance = -1;
    while (static_cast<int> (sample_indices.size()) < nr_samples)
    {
        int sample_index = getRandomIndex(static_cast<int> (keypoints->points.size()));
        bool valid_sample = true;
        if (correspondences[sample_index] == -1)
        {
            valid_sample = false;
        }
        for (size_t i = 0; i < sample_indices.size(); ++i)
        {
            
            float distance_between_samples = euclideanDistance(keypoints->points[sample_index], keypoints->points[sample_indices[i]]);
            
            if (sample_index == sample_indices[i] || distance_between_samples < min_sample_distance)
            {
                valid_sample = false;
                break;
            }
        }
        if (valid_sample)
        {
            sample_indices.push_back(sample_index);
        }
    }
}

int computeInliners(const pcl::PointCloud<pcl::PointXYZ> points1t, const pcl::PointCloud<pcl::PointXYZ>::Ptr points2, float threshold)
{
    int inliers = 0;
    pcl::KdTreeFLANN<pcl::PointXYZ>::Ptr tree_(new pcl::KdTreeFLANN<pcl::PointXYZ>);
    tree_->setInputCloud(points2);
    std::vector<int> nn_index(1);
    std::vector<float> nn_distance(1);
    for (int i = 0; i < static_cast<int> (points1t.points.size()); ++i)
    {
        tree_->nearestKSearch(points1t, i, 1, nn_index, nn_distance);
        if (nn_distance[0] < threshold)
            inliers++;
    }
    return inliers;
}

int computeInliners(const pcl::PointCloud<pcl::PointXYZ> points1t, const pcl::PointCloud<pcl::PointXYZ>::Ptr points2, float threshold, std::vector<int>& index1, std::vector<int>& index2)
{
    int inliers = 0;
    pcl::KdTreeFLANN<pcl::PointXYZ>::Ptr tree_(new pcl::KdTreeFLANN<pcl::PointXYZ>);
    tree_->setInputCloud(points2);
    std::vector<int> nn_index(1);
    std::vector<float> nn_distance(1);
    index1.clear();
    index2.clear();
    for (int i = 0; i < static_cast<int> (points1t.points.size()); ++i)
    {
        tree_->nearestKSearch(points1t, i, 1, nn_index, nn_distance);
        if (nn_distance[0] < threshold)
        {
            index1.push_back(i);
            index2.push_back(nn_index[0]);
            inliers++;
        }
    }
    return inliers;
}

void ransac(
            const pcl::PointCloudPtr<pcl::PointXYZ> keypoints1,
            const pcl::PointCloudPtr<pcl::PointXYZ> keypoints2,
            const std::vector<int> &correspondences,
            const std::vector<float> &correspondence_scores,std::vector<int>& sample_indices3, std::vector<int>& corresponding_indices3,Eigen::Matrix4f& transform)
{
    pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> transformation_estimation_;
    int nter = 5000;
    int nr_samples = 3;
    std::vector<int> sample_kindices(nr_samples);
    std::vector<int> sample_indices(nr_samples);
    std::vector<int> corresponding_indices(nr_samples);
    float corr_dist_threshold_ = 0.05;
    int max_inliers = 0;
    for (int i = 0; i < nter; i++)
    {
        Eigen::Matrix4f transformation_;
        selectSamples(keypoints1, nr_samples, correspondences, sample_kindices);
        for (int m = 0; m < sample_kindices.size(); m++)
        {
            sample_indices[m] = sample_kindices[m];
            corresponding_indices[m] = correspondences[sample_kindices[m]];
        }
        transformation_estimation_.estimateRigidTransformation(*keypoints1, sample_indices, *keypoints2, corresponding_indices, transformation_);
        pcl::PointCloud<pcl::PointXYZ> input_transformed;
        pcl::transformPointCloud(*keypoints1, input_transformed, transformation_);
        int inlier_number = computeInliners(input_transformed, keypoints2, static_cast<float> (corr_dist_threshold_));
        if (inlier_number > max_inliers)
        {
            computeInliners(input_transformed, keypoints2, corr_dist_threshold_, sample_indices3, corresponding_indices3);
            max_inliers = inlier_number;
            transform = transformation_;
        }
    }
}

bool isNan(pcl::FPFHSignature33& f)
{
    int size = f.descriptorSize();
    for (int i = 0; i < size; i++)
    {
        if (f.histogram[i] != f.histogram[i])
            return true;
    }
    return false;
}

//void testIt () {
//    NSLog(@"asdasa");
//}

void estimateCameraPose(
                        pcl::PointCloudPtr<pcl::PointXYZRGBNormal>& cloud0,
                        pcl::PointCloudPtr<pcl::PointXYZRGBNormal>& cloud1,
                        Eigen::Affine3f& relative_pose,
                        float leaf_size/*=0.05*/, float dist_threshold/*=5.0*/
)
{
    CFTimeInterval startTime = CACurrentMediaTime();
    {
        pcl::PointCloudPtr<pcl::PointXYZ> keypoints0(new pcl::PointCloud<pcl::PointXYZ>);
        keyPointsDetect(cloud0, keypoints0, dist_threshold);
        //keyPointsSave(keypoints0, "keypoints0.ply");
        
        pcl::PointCloudPtr<pcl::PointXYZ> keypoints1(new pcl::PointCloud<pcl::PointXYZ>);
        keyPointsDetect(cloud1, keypoints1, dist_threshold);
        //keyPointsSave(keypoints1, "keypoints1.ply");
        
        float radius = 8 * leaf_size;
        pcl::FPFHEstimation<pcl::PointXYZ, pcl::PointXYZRGBNormal, pcl::FPFHSignature33> fpfh;
        fpfh.setRadiusSearch(radius);
        fpfh.setSearchMethod(pcl::search::KdTree<pcl::PointXYZ>::Ptr(new pcl::search::KdTree<pcl::PointXYZ>));
        
        pcl::PointCloudPtr<pcl::PointXYZ> cloud0_xyz(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::copyPointCloud(*cloud0, *cloud0_xyz);
        pcl::PointCloudPtr<pcl::PointXYZRGBNormal> cloud0_normal(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
        pcl::copyPointCloud(*cloud0, *cloud0_normal);
        fpfh.setSearchSurface(cloud0_xyz);
        fpfh.setInputNormals(cloud0_normal);
        fpfh.setInputCloud (keypoints0);
        pcl::PointCloudPtr<pcl::FPFHSignature33> features0(new pcl::PointCloud<pcl::FPFHSignature33>);
        fpfh.compute (*features0);
        pcl::PointCloud<pcl::PointXYZ>::Ptr keypointsc0(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::FPFHSignature33>::Ptr featuresc0(new pcl::PointCloud<pcl::FPFHSignature33>);
        for (int i = 0; i < keypoints0->points.size(); i++)
        {
            pcl::FPFHSignature33 f = features0->points[i];
            if (!isNan(f))
            {
                keypointsc0->points.push_back(keypoints0->points[i]);
                featuresc0->points.push_back(f);
            }
        }
        std::vector<int> indices0;
        getIndex(cloud0_xyz, keypointsc0, indices0);

        pcl::PointCloudPtr<pcl::PointXYZ> cloud1_xyz(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::copyPointCloud(*cloud1, *cloud1_xyz);
        pcl::PointCloudPtr<pcl::PointXYZRGBNormal> cloud1_normal(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
        pcl::copyPointCloud(*cloud1, *cloud1_normal);
        fpfh.setSearchSurface(cloud1_xyz);
        fpfh.setInputNormals(cloud1_normal);
        fpfh.setInputCloud (keypoints1);
        pcl::PointCloud<pcl::FPFHSignature33>::Ptr features1(new pcl::PointCloud<pcl::FPFHSignature33>);
        fpfh.compute (*features1);
        pcl::PointCloud<pcl::PointXYZ>::Ptr keypointsc1(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::FPFHSignature33>::Ptr featuresc1(new pcl::PointCloud<pcl::FPFHSignature33>);
        for (int i = 0; i < keypoints1->points.size(); i++)
        {
            pcl::FPFHSignature33 f = features1->points[i];
            if (!isNan(f))
            {
                keypointsc1->points.push_back(keypoints1->points[i]);
                featuresc1->points.push_back(f);
            }
        }
        std::vector<int> indices1;
        getIndex(cloud1_xyz, keypointsc1, indices1);
        
        std::vector<int> correspondences;
        std::vector<float> correspondences_scores;
        std::cout << "key points 0: " << keypointsc0->points.size() << std::endl;
        std::cout << "key points 1: " << keypointsc1->points.size() << std::endl;

        //std::vector<int> sample_indices2;
        //std::vector<int> corresponding_indices2;

        std::vector<int> sample_indices3;
        std::vector<int> corresponding_indices3;
        find_feature_correspondences(keypointsc1, featuresc1, keypointsc0, featuresc0, correspondences, correspondences_scores);
        
        std::cout << "correspondences: " << correspondences.size() << std::endl;

        //correspondences_out
        
        Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
        pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> transformation_estimation_;
        Eigen::Matrix4f transform2;
        pcl::PointCloudPtr<pcl::PointXYZRGBNormal> transformed_cloud3(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
        CFTimeInterval _ransacTime = CACurrentMediaTime();
        {
            ransac(keypointsc1, keypointsc0, correspondences, correspondences_scores, sample_indices3, corresponding_indices3, transform);
            transformation_estimation_.estimateRigidTransformation(*keypointsc1, sample_indices3, *keypointsc0, corresponding_indices3, transform2);
            pcl::transformPointCloudWithNormals(*cloud1, *transformed_cloud3, transform2);
        }
        NSLog(@">> Ransac Runtime: %g s", CACurrentMediaTime() - _ransacTime);

        mat2Aff(transform2, relative_pose);
//        float leaf_size_gicp = 0.15;
//        pcl::VoxelGrid<pcl::PointXYZRGBNormal> vg;
//        vg.setLeafSize(leaf_size_gicp, leaf_size_gicp, leaf_size_gicp);
//        vg.setInputCloud(cloud0);
//        pcl::PointCloudPtr<pcl::PointXYZRGBNormal> cloud0f(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
//        vg.filter(*cloud0f);
//        
//        vg.setInputCloud(transformed_cloud3);
//        pcl::PointCloudPtr<pcl::PointXYZRGBNormal> transformed_cloud3f(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
//        vg.filter(*transformed_cloud3f);
//        
//        std::cout << "Pointcloud 0 points: " << cloud0f->points.size() << std::endl;
//        std::cout << "Pointcloud 1 points: " << transformed_cloud3f->points.size() << std::endl;
        
        CFTimeInterval _gicpTime = CACurrentMediaTime();
        {
//            pcl::GeneralizedIterativeClosestPoint<pcl::PointXYZRGBNormal,pcl::PointXYZRGBNormal> gicp;
//            gicp.setInputTarget(cloud0f);
//            gicp.setInputSource(transformed_cloud3f);
//            pcl::PointCloudPtr<pcl::PointXYZRGBNormal> tmp(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
//            gicp.align(*tmp);
//            Eigen::Matrix4f relative_pose_icp = gicp.getFinalTransformation();
//            Eigen::Matrix4f relative_pose_final = relative_pose_icp * transform2;
//            mat2Aff(relative_pose_final, relative_pose);
        }
        NSLog(@">> GICP Runtime: %g s", CACurrentMediaTime() - _gicpTime);
    }
    NSLog(@">> Total Runtime: %g s", CACurrentMediaTime() - startTime);
}


@end
