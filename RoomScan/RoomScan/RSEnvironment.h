//
//  RSEnvironment.h
//  RoomScan
//
//  Created by Finde Xumara on 06/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RSScan;

@interface RSEnvironment : NSManagedObject

@property (nonatomic, retain) NSString * environmentId;
@property (nonatomic, retain) NSString * environmentName;
@property (nonatomic, retain) NSString * serverId;
@property (nonatomic, retain) NSOrderedSet * scans;
@property (nonatomic, retain) NSOrderedSet * annotations;
@property (nonatomic, retain) NSDate *createdAt;
@property (nonatomic, retain) NSDate *modifiedAt;

@end

@interface RSEnvironment (CoreDataGeneratedAccessors)

- (void) prepareSetting;
- (NSURL *) getPath;
- (void) saveToContext: (NSManagedObjectContext *) context;

@end
