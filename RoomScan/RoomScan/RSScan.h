//
//  RSScan.h
//  RoomScan
//
//  Created by Finde Xumara on 06/06/16.
//  Copyright © 2016 3DUniversum. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RSEnvironment;

NS_ASSUME_NONNULL_BEGIN

@interface RSScan : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
-(void) prepareSetting;
-(NSURL *) getPath;
-(NSString *) getDir; // get NSString path without scans folder

- (void) setTransformationMatrix: (GLKMatrix4) transformationMat;
- (GLKMatrix4) getTransformationMatrix;
- (NSMutableArray *) getTransformationArray;

@end

NS_ASSUME_NONNULL_END

#import "RSScan+CoreDataProperties.h"
